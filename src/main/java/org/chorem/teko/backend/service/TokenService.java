package org.chorem.teko.backend.service;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.StringUtils;
import org.chorem.teko.backend.TekoConfig;
import org.chorem.teko.backend.dao.ProjectDao;
import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.User;
import org.chorem.teko.backend.entity.Visibility;
import org.chorem.teko.backend.exceptions.TekoException;
import org.chorem.teko.backend.exceptions.UnauthorizedException;
import org.nuiton.spgeed.SqlSession;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by lebras on 27/01/17.
 */

@ApplicationScoped
public class TokenService extends TekoAbstractService {

    @Inject
    TekoConfig tekoConfig;

    public String createToken(User user) throws Exception {
        try {
            Algorithm algorithm = Algorithm.HMAC256(tekoConfig.webSecurityKey());
            String token = JWT.create()
                .withClaim("userId", user.getId().toString())
                .withClaim("firstName", user.getFirstName())
                .withClaim("lastName", user.getLastName())
                .sign(algorithm);
            return token;
        } catch (JWTCreationException exception) {
            throw new TekoException("Invalid Signing configuration / Couldn't convert Claims");
        }
    }

    public UUID checkToken(String jwtToken) throws UnauthorizedException {

        if (jwtToken == null) {
            throw new UnauthorizedException("Seems no user connected");
        }

        try {
            String token = StringUtils.replace(jwtToken, "Bearer ", "");
            Algorithm algorithm = Algorithm.HMAC256(tekoConfig.webSecurityKey());
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            String userId = jwt.getClaim("userId").asString();
            return UUID.fromString(userId);

        } catch (JWTVerificationException e) {
            // Error during Payload verification
            throw new UnauthorizedException("Invalid authorization", e);
        }
    }

    @Deprecated
    public boolean isUserAllowedToSeeProject(String token, UUID projectId) throws Exception {

        UUID userId = this.checkToken(token);
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        boolean userAuthorizedOnProject = dao.isUserAuthorizedOnProject(userId, projectId);
        return userAuthorizedOnProject;
    }

    public boolean isUserAllowedToSeeProject(String token, UUID projectId, SqlSession session) throws Exception {

        UUID userId = null;
        if (StringUtils.isNotBlank(token)) {
            userId = this.checkToken(token);
        }
        ProjectDao dao = session.getDao(ProjectDao.class);
        boolean userAuthorizedOnProject = dao.isUserAuthorizedOnProject(userId, projectId);
        return userAuthorizedOnProject;
    }

    public boolean isUserAllowedOnProject(UUID userId, Project project) throws Exception {
        if (Visibility.PUBLIC.equals(project.getVisibility())) {
            return true;
        } else {
            try {
                if (userId.equals(project.getCreatorId())) {
                    return true;
                }
                if (project.getAuthorizedUsersId() != null) {
                    return Arrays.stream(project.getAuthorizedUsersId()).anyMatch(id -> Objects.equals(id, userId));
                }
            } catch (IllegalStateException e) {
                throw new Exception();
            }
        }
        return false;
    }
}
