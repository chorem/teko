package org.chorem.teko.backend.entity;

import org.nuiton.spgeed.mapper.JsonMapper;

/**
 * Need to be able to run dev mode with quarkus and have mapping in memory
 * @author ymartel (martel@codelutin.com)
 */
public class TekoJsonMapper extends JsonMapper {

    /**
     * Find a Class for the given elementTypeName as an Array.
     *
     * This method is meant to be overriden if Spgeed is used in a framework with a custom classpath management
     * (eg. Quarkus's dev mode). Don't make it private.
     *
     * @param elementTypeName the element to find in an array format. Example: {@code MyObject}
     * @return the array's Class. Example: {@code Class<MyObject[]>}
     * @throws ClassNotFoundException if the class cannot be found
     * @see Class#forName(String)
     */
    protected Class<?> findElementArrayClass(String elementTypeName) throws ClassNotFoundException {
        String arrayClassName = "[L" + elementTypeName + ";";
        Class<?> result = Class.forName(arrayClassName);
        return result;
    }
}
