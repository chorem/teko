package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.Criticality;
import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.StepCategory;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import javax.sql.DataSource;
import java.util.UUID;

/**
 * Created by couteau on 05/02/17.
 */
public class TestCaseDaoTest extends AbstractDaoTest {

    @Test
    public void createAndGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            initDB(session);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            UUID project01Id = UUID.randomUUID();
            project1.setId(project01Id);
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            String testCase01Label = "Test Case 1";
            testcase1.setLabel(testCase01Label);
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            testcase2.setId(UUID.randomUUID());
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase2);

            {
                TestCase testCase = testCaseDao.findById(testCase01Id);
                Assertions.assertNotNull(testCase);
                Assertions.assertEquals(testCase01Id, testCase.getId());
                Assertions.assertEquals(project01Id, testCase.getProjectId());
                Assertions.assertEquals(testCase01Label, testCase.getLabel());
            }

            {
                TestCase[] all = testCaseDao.findAllByProjectId(project1.getId(), "creationDate", true);
                Assertions.assertEquals(2, all.length);
            }

            {
                boolean isProject01Referenced = projectDao.isProjectReferencedByTestCase(project01Id);
                Assertions.assertTrue(isProject01Referenced);
            }

            {
                Project[] projects = projectDao.getProjects(null, "date", true, null);
                Assertions.assertEquals(2, projects[0].getNbTestCases());
            }

            /*{
                Project p3 = projectDao.getProject(project1.getId());
                Assertions.assertEquals(project1.getName(), p3.getName());
                Assertions.assertEquals(project1.getId(), p3.getId());
            }*/
        }
    }

    @Test
    public void addRemoveAndGetTag() throws Exception {

        try (SqlSession session = createSqlSession()) {
            initDB(session);
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            project1.setId(UUID.randomUUID());
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            testcase1.setLabel("Test Case 1");
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            UUID testCase02Id = UUID.randomUUID();
            testcase2.setId(testCase02Id);
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase2);

            //add three tag
            testCaseDao.addTag(testCase01Id, "tag 1");
            testCaseDao.addTag(testCase01Id, "tag 1");
            testCaseDao.addTag(testCase02Id, "tag 1");
            testCaseDao.addTag(testCase02Id, "tag 2");

            {
                String[] tag = testCaseDao.getTestCaseTags(testCase01Id).getTags();
                Assertions.assertEquals(1, tag.length);
                Assertions.assertEquals("tag 1", tag[0]);
                tag = testCaseDao.getTestCaseTags(testCase02Id).getTags();
                Assertions.assertEquals(2, tag.length);
                Object[] tags = projectDao.getProjectTags(project1.getId());
                Assertions.assertEquals(2, tags.length);
                TestCase[] testCase = testCaseDao. getTestCaseByTag("tag 1", project1.getId());
                Assertions.assertEquals(2, testCase.length);
            }

            //remove two tag
            testCaseDao.removeTag(testCase01Id, "tag 1");
            testCaseDao.removeTag(testCase02Id, "tag 1");

            {
                String[] tag = testCaseDao.getTestCaseTags(testCase01Id).getTags();
                Assertions.assertEquals(0, tag.length);
                tag = testCaseDao.getTestCaseTags(testCase02Id).getTags();
                Assertions.assertEquals(1, tag.length);
                Object[] tags = projectDao.getProjectTags(project1.getId());
                Assertions.assertEquals(1, tags.length);
            }
        }
    }

    @Test
    public void getWithFilter() throws Exception {

        try (SqlSession session = createSqlSession()) {
            initDB(session);
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            project1.setId(UUID.randomUUID());
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            testcase1.setLabel("Test Case 1");
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testcase1.setCriticality(Criticality.MEDIUM);
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            UUID testCase02Id = UUID.randomUUID();
            testcase2.setId(testCase02Id);
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testcase2.setCriticality(Criticality.HIGH);
            testCaseDao.createTestCase(testcase2);

            //create testCase 3
            TestCase testcase3 = new TestCase();
            UUID testCase03Id = UUID.randomUUID();
            testcase3.setId(testCase03Id);
            testcase3.setLabel("Test Case 3");
            testcase3.setRef("1");
            testcase3.setStatus(Status.OBSOLETE);
            testcase3.setProjectId(project1.getId());
            testcase3.setCriticality(Criticality.HIGH);
            testCaseDao.createTestCase(testcase3);

            {
                TestCase[] testCases = testCaseDao.findAllWithFilter(project1.getId(),"%Case%", null, null, true, "date");
                Assertions.assertEquals(3, testCases.length);
                testCases = testCaseDao.findAllWithFilter(project1.getId(),null, "HIGH", null, true, "date");
                Assertions.assertEquals(2, testCases.length);
                testCases = testCaseDao.findAllWithFilter(project1.getId(),null, null, "AVAILABLE", true, "date");
                Assertions.assertEquals(2, testCases.length);
                testCases = testCaseDao.findAllWithFilter(project1.getId(),"%Case%", "HIGH", "AVAILABLE", true, "date");
                Assertions.assertEquals(1, testCases.length);
                Assertions.assertEquals(testCase02Id, testCases[0].getId());
            }

        }
    }

    @Test
    public void getByTestStep() throws Exception {

        try (SqlSession session = createSqlSession()) {
            initDB(session);
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            project1.setId(UUID.randomUUID());
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            testcase1.setLabel("Test Case 1");
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            UUID testCase02Id = UUID.randomUUID();
            testcase2.setId(testCase02Id);
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase2);

            //create one TestStep
            TestStep testStep1 = new TestStep();
            testStep1.setLabel("Step 01");
            UUID testStep01Id = UUID.randomUUID();
            testStep1.setId(testStep01Id);
            testStep1.setCategory(StepCategory.PREREQUISITE);
            testStepDao.createTestStep(testStep1);
            testCaseDao.addStep(testCase01Id, testStep01Id);

            //create an other TestStep
            TestStep testStep2 = new TestStep();
            testStep2.setLabel("Step two");
            UUID testStep02Id = UUID.randomUUID();
            testStep2.setId(testStep02Id);
            testStep2.setCategory(StepCategory.ACTION);
            testStepDao.createTestStep(testStep2);
            testCaseDao.addStep(testCase01Id, testStep02Id);
            testCaseDao.addStep(testCase02Id, testStep02Id);

            //create an other TestStep
            TestStep testStep3 = new TestStep();
            testStep3.setLabel("Step three");
            UUID testStep03Id = UUID.randomUUID();
            testStep3.setId(testStep03Id);
            testStep3.setCategory(StepCategory.ACTION);
            testStepDao.createTestStep(testStep3);

            {
                TestCase[] testCases = testCaseDao.getAllForTestStep(testStep01Id);
                Assertions.assertEquals(1, testCases.length);
                Assertions.assertEquals(testCase01Id, testCases[0].getId());
                testCases = testCaseDao.getAllForTestStep(testStep02Id);
                Assertions.assertEquals(2, testCases.length);
                boolean stepReferenced = testCaseDao.isTestStepReferenced(testStep01Id);
                Assertions.assertTrue(stepReferenced);
                stepReferenced = testCaseDao.isTestStepReferenced(testStep02Id);
                Assertions.assertTrue(stepReferenced);
                stepReferenced = testCaseDao.isTestStepReferenced(testStep03Id);
                Assertions.assertFalse(stepReferenced);
            }
        }
    }

    @Test
    public void testFindAllByIds() throws Exception {

        try (SqlSession session = createSqlSession()) {
            initDB(session);
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            project1.setId(UUID.randomUUID());
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            testcase1.setLabel("Test Case 1");
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            UUID testCase02Id = UUID.randomUUID();
            testcase2.setId(testCase02Id);
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase2);

            //create testCase 3
            TestCase testcase3 = new TestCase();
            UUID testCase03Id = UUID.randomUUID();
            testcase3.setId(testCase03Id);
            testcase3.setLabel("Test Case 3");
            testcase3.setRef("42");
            testcase3.setStatus(Status.OBSOLETE);
            testcase3.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase3);

            //create one TestStep
            TestStep testStep1 = new TestStep();
            testStep1.setLabel("Step 01");
            UUID testStep01Id = UUID.randomUUID();
            testStep1.setId(testStep01Id);
            testStep1.setCategory(StepCategory.PREREQUISITE);
            testStepDao.createTestStep(testStep1);
            testCaseDao.addStep(testCase01Id, testStep01Id);

            //create an other TestStep
            TestStep testStep2 = new TestStep();
            testStep2.setLabel("Step two");
            UUID testStep02Id = UUID.randomUUID();
            testStep2.setId(testStep02Id);
            testStep2.setCategory(StepCategory.ACTION);
            testStepDao.createTestStep(testStep2);
            testCaseDao.addStep(testCase01Id, testStep02Id);
            testCaseDao.addStep(testCase02Id, testStep02Id);

            //create an other TestStep
            TestStep testStep3 = new TestStep();
            testStep3.setLabel("Step three");
            UUID testStep03Id = UUID.randomUUID();
            testStep3.setId(testStep03Id);
            testStep3.setCategory(StepCategory.ACTION);
            testStepDao.createTestStep(testStep3);

            {
                TestCase[] testCases = testCaseDao.findAllByIds(new UUID[]{testCase03Id});
                Assertions.assertEquals(1, testCases.length);
                Assertions.assertEquals(testCase03Id, testCases[0].getId());

                testCases = testCaseDao.findAllByIds(new UUID[]{testCase01Id, testCase03Id});
                Assertions.assertEquals(2, testCases.length);

                testCases = testCaseDao.findAllByIds(new UUID[]{testCase01Id, testCase03Id, testCase02Id});
                Assertions.assertEquals(3, testCases.length);
            }
        }
    }
    @Test
    public void addRemoveAndGetCondition() throws Exception {

        try (SqlSession session = createSqlSession()) {
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            initDB(session);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            UUID project01Id = UUID.randomUUID();
            project1.setId(project01Id);
            projectDao.createProject(project1);

            //create testCase 1
            TestCase testcase1 = new TestCase();
            String testCase01Label = "Test Case 1";
            testcase1.setLabel(testCase01Label);
            UUID testCase01Id = UUID.randomUUID();
            testcase1.setId(testCase01Id);
            testcase1.setRef("1");
            testcase1.setStatus(Status.AVAILABLE);
            testcase1.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase1);

            //create testCase 2
            TestCase testcase2 = new TestCase();
            testcase2.setLabel("Test Case 2");
            testcase2.setId(UUID.randomUUID());
            testcase2.setRef("1");
            testcase2.setStatus(Status.AVAILABLE);
            testcase2.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase2);

            //create testCase 3
            TestCase testcase3 = new TestCase();
            testcase3.setLabel("Test Case 3");
            testcase3.setId(UUID.randomUUID());
            testcase3.setRef("1");
            testcase3.setStatus(Status.AVAILABLE);
            testcase3.setProjectId(project1.getId());
            testCaseDao.createTestCase(testcase3);

            //add conditions
            testCaseDao.addCondition(testcase1.getId(), testcase1.getId());
            testCaseDao.addCondition(testcase1.getId(), testcase2.getId());
            testCaseDao.addCondition(testcase1.getId(), testcase3.getId());

            {
                TestCase[] conditionsTest = testCaseDao.getConditions(testcase1.getId());
                Assertions.assertEquals(2, conditionsTest.length );
                Assertions.assertEquals(testcase2.getId(), conditionsTest[0].getId());
                TestCase[] testCondition = testCaseDao.getDependOf(testcase2.getId());
                Assertions.assertEquals(1, testCondition.length);
                Assertions.assertEquals(testcase1.getId(), testCondition[0].getId());

            }

            //remove condition
            testCaseDao.removeCondition(testcase1.getId(), testcase2.getId());

            {
                TestCase[] conditionsTest = testCaseDao.getConditions(testcase1.getId());
                Assertions.assertEquals(1, conditionsTest.length );
                Assertions.assertEquals(testcase3.getId(), conditionsTest[0].getId());
            }

        }
    }

}
