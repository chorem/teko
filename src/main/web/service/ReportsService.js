let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class ReportsService extends FetchService {

    getProjectTestCasesPdfReportsUrl(projectId, locale) {
        return window.BACKEND_URL + "/reports/testCase/" + projectId + "/pdf/" + locale;
    }

    getProjectTestCasesOdtReportsUrl(projectId, locale) {
        return window.BACKEND_URL + "/reports/testCase/" + projectId + "/odt/" + locale;
    }

    getCampaignExecutionPdfReportsUrl(executionId, locale) {
        return window.BACKEND_URL + "/reports/campaign/execution/" + executionId + "/pdf/" + locale;
    }

    getCampaignExecutionOdtReportsUrl(executionId, locale) {
        return window.BACKEND_URL + "/reports/campaign/execution/" + executionId + "/odt/" + locale;
    }
}

module.exports = singleton(ReportsService);