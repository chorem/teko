let projectsService = require("../../service/ProjectService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<ProjectForm>
  <div class="sheet">
    <div class="sheet-header">
      {translate('project.create')}
    </div>

    <div class="sheet-content">
      <form ref="project" onsubmit="{create}">
        <ul class="form-content">
          <li>
            <label for="name">{translate('project.name.label')}</label>
            <input type="text" name="name" placeholder="{translate('project.name.placeholder')}" required autofocus>
          </li>
          <li>
            <label for="visibility">{translate('project.visibility.label')}</label>
            <select name="visibility">
              <option value="PUBLIC" selected>{translate('project.visibility.public')}</option>
              <option value="PRIVATE">{translate('project.visibility.private')}</option>
            </select>
          </li>
        </ul>

        <div class="form-buttons">
          <button onclick="{cancelAction}"
                  type="button"
                  class="button button--secondary">
            { translate('common.cancel') }
          </button>
          <button type="submit"
                 class="button button--main">
            {translate('common.create')}
          </button>
        </div>

        <div if="{error}" class="notification-error">
          <i class="icon icon-bullhorn pulsate-fwd"></i>
          {error}
        </div>
      </form>
    </div>
  </div>

  <script>
      this.translate = translate;
      this.error = "";

      document.title = "Teko - " + this.translate('project.new');

      this.create = (e) => {
          e.preventDefault();
          e.stopPropagation();
          eventBus.trigger('loading-start');

          let project = FormUtils.formToMap(this.refs.project);
          projectsService.createProject(project)
          .then(() => {
              eventBus.trigger('project-created');
              route('');
          })
          .catch((msg) => {
              eventBus.trigger('loading-end');
              this.error = translate('project.saveError');
              this.update();
          });
      };

      this.cancelAction = () => {
        route('/');
      };

    eventBus.on('locale-changed', () => {
      this.update();
    });
  </script>

</ProjectForm>