package org.chorem.teko.backend.dao;

import org.apache.commons.lang3.StringUtils;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.TestCampaign;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.query.Query;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public interface TestCampaignDao extends SpgeedDao {

    @Update(sql = "INSERT INTO testCampaign (id, projectId, label, testCaseIds, status, creationDate) "
                + "VALUES ${testCampaign | values("
                + "'id', 'projectId', 'label', 'testCaseIds', 'status::status', 'creationDate'"
                + ")}")
    public int create(TestCampaign testCampaign);

    @Update(sql = "UPDATE testCampaign SET "
                + " label = ${testCampaign.label} , "
                + " testCaseIds = ${testCampaign.testCaseIds} , "
                + " status = ${testCampaign.status}::status "
                + " WHERE id=${testCampaign.id}")
    public int update(TestCampaign testCampaign);

    @Select(sql = "SELECT id, projectId, label, testCaseIds, status::status, creationDate, CASE WHEN EXISTS("
                + " SELECT * FROM campaignExecution WHERE testCampaignId = id"
                + " ) THEN 'false' ELSE 'true' END AS alterable"
                + " FROM testCampaign "
                + " WHERE id=${id}")
    public TestCampaign findById(UUID id);

    @Select(sql = " SELECT id, projectId, label, testCaseIds, status::status, creationDate, CASE WHEN EXISTS("
                + " SELECT * FROM campaignExecution WHERE testCampaignId = id"
                + " ) THEN 'false' ELSE 'true' END AS alterable"
                + " FROM testCampaign "
                + " ORDER BY creationDate ASC")
    public TestCampaign[] findAll();

    @Select(sql = " SELECT id, projectId, label, testCaseIds, status::status, creationDate, CASE WHEN EXISTS("
            + " SELECT * FROM campaignExecution WHERE testCampaignId = id"
            + " ) THEN 'false' ELSE 'true' END AS alterable"
            + " FROM testCampaign "
            + " WHERE status = ${status}::status "
            + " ORDER BY creationDate ASC")
    public TestCampaign[] findAllByStatus(Status status);

    default TestCampaign[] findWithFilter(UUID projectId, String status, String keyWord, boolean order, String attribut) throws Exception {

        Map<String, Object> map = new HashMap();
        map.put("projectId", projectId);
        String sql = " SELECT tc.id, tc.projectId, tc.label, tc.testCaseIds, tc.status::status, tc.creationDate, CASE WHEN EXISTS("
                + " SELECT * FROM campaignexecution WHERE testCampaignId = tc.id"
                + " ) THEN 'false' ELSE 'true' END AS alterable"
                + " FROM testCampaign tc"
                + " WHERE tc.projectId= ${projectId} ";

        if (StringUtils.isNotBlank(status)) {
            map.put("status", status);
            sql += "AND status = ${status}::status ";
        }
        if(StringUtils.isNotBlank(keyWord)){
            map.put("keyWord", keyWord);
            sql += "AND UPPER(label) LIKE UPPER(${keyWord}) ";
        }

        sql += " ORDER BY ";
        if ("label".equals(attribut)) {
            sql += "label ";
        } else {
            sql += "creationDate ";
        }
        if (order) {
            sql += "ASC";
        } else {
            sql += "DESC";
        }

        Query query = new Query(getSession(), sql, map, TestCampaign[].class);
        return (TestCampaign[]) query.executeQuery();
    }

    @Update(sql = "DELETE FROM testCampaign WHERE id=${id}")
    public int delete(UUID id);

    @Update(sql = "UPDATE testCampaign SET "
                + " status = ${status}::status "
                + " WHERE id=${testCampaignId}")
    public int updateStatus(UUID testCampaignId, Status status);

}
