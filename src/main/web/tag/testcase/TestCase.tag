let testCaseService = require("../../service/TestCaseService");
let route = require("riot-route");
let translate = require("counterpart");
let eventBus = require("../../utils/TekoMagicEventBus");

<TestCase class="sheet main-content {loading : loading}">
  <div class="sheet-header">
    {testCase.label}
    <div if="{login && testCase.alterable}" class="sheet-header-button align-right">
      <a href="/testcase/{testCase.id}/edit"
         class="button"
         title="{translate('common.update')}">
        <span class="icon icon-edit"></span> {translate('common.update')}
      </a>
    </div>
    <div if="{login && testCase.alterable}" class="sheet-header-button">
      <button onclick="{deleteConfirmation(testCase)}">
        <i class="icon icon-delete"></i>
        {translate('common.delete')}
      </button>
    </div>
  </div>

  <div if="{error}" class="error-notification">{error}</div>

  <div class="sheet-content">
    <TestTags  opts = {testCaseId=opts.testCaseId;}/>

    <!-- FIXME à placer dans un composant -->
    <div class="sheet-content--part" style="border-bottom: 0;">
      <label>{translate('testcase.condition.label')}</label>
      <ul class="tags-inline" >
          <li each="{test in testCasesConditions}">
              <span class="board-list-item-label">{test.label}</span>
              <a href="" onclick="{removeCondition(test.id)}" title="{translate('common.delete')}" if="{login && testCase.alterable}">
                  <span class="icon icon-delete"></span>
              </a>
          </li>
      </ul>
    </div>
    <div class="sheet-content--part">
      <select onchange="{addCondition}" disabled="{!login || !testCase.alterable}">
        <option value="" selected>{translate('testcase.condition.choose')} &nbsp;&nbsp;&nbsp;&#9662;</option>
        <option each="{testCase in testCasesPossibles}"
          value="{testCase.id}"
          id="{testCase.id}"
          name="testCase.label">
          <label for="{testCase.id}">{testCase.label}</label>
        </option>
      </select>
    </div>

    <div class="form-content" style="margin: 0;">
      <TestSteps testCaseId="{opts.testCaseId}" alterable="{testCase.alterable}"/>
    </div>
  </div>

    <script>
      this.translate = translate;
      this.loading = false;
      this.testCase = {};
      this.allTestCases = [];
      this.testCasesPossibles = [];
      this.testCasesConditions = [];
      this.login = localStorage.getItem('accessToken');

      this.getAllTest = () => {
          this.allTestCases = [];
          testCaseService.getTestCases(this.testCase.projectId)
              .then((testCases)=>{
                this.allTestCases = testCases;
                this.getConditions();
                this.update();
          });
      }

      this.getConditions = () => {
          this.testCasesConditions = [];
          testCaseService.getCondition(this.testCase.id, false)
              .then((conditions)=>{
              if(conditions){
                  this.testCasesConditions = conditions;
              }
              this.getPossibles();
              this.update();
          });
      }

      this.getPossibles = () => {
          this.testCasesPossibles = [];
          testCaseService.getCondition(this.testCase.id, true)
              .then((conditions)=>{
              testCaseService.getDependOf(this.testCase.id, true)
                  .then((dependOf)=>{
                  dependOf.forEach(function(son){
                      conditions.push(son);
                  });
                  conditions.push(this.testCase);
                  this.allTestCases.forEach((test) => {
                      var index = 0;
                      while (index < conditions.length && conditions[index].id != test.id){
                          index++;
                      }
                      if (index == conditions.length){
                          this.testCasesPossibles.push(test)
                      }
                  });
              this.update();
              });
          });
      }

      this.addCondition = (e) => {
          let testCaseCondition = e.target.value;
          if(testCaseCondition != ""){
              testCaseService.addCondition(this.testCase.id, testCaseCondition)
              .then(()=>{
                  this.getAllTest();
              });
          }
      }

      this.removeCondition = (condition) => {
          return (e) => {
              testCaseService.removeCondition(this.testCase.id, condition)
              .then(()=>{
                  this.getAllTest();
              });
          };
      }

      this.deleteConfirmation = (testCase) => {
        return (e) => {
          message = translate('project.confirmDeleteLabel');
          this.confirmObsolesceModal(message, testCase.label).then((confirm) => {
            if (confirm === "OBSOLESCE") {
              this.obsolesce(testCase);
            } else if (confirm === "DELETE") {
              this.delete(testCase.id);
            }
          });
        }
      };

      this.delete = (id) => {
          testCaseService.deleteTestCase(id)
          .then(() => {
              eventBus.trigger('testcase-deleted', id);
          })
          .catch((msg) => {
              this.error = msg || translate('testcase.deletionError');
              this.update();
          });
      };

      eventBus.on('addtesttag', (tag) => {
          testCaseService.addTestTag(testCaseId, tag)
              .then(() => {
                this.update();
                eventBus.trigger('tagfolder');
            }).catch((msg) => {
                console.log("Unable to create tag");
            });
      });

      eventBus.on('removetesttag', (tag) => {
          testCaseService.removeTestTag(testCaseId, tag)
              .then(() => {
              this.update();
              eventBus.trigger('tagfolder');
          });
      });

      eventBus.on('navigation-testcase-changed', (testCaseId, alterable) => {
        testCaseService.getTestCase(testCaseId)
        .then((testCase) => {
          this.testCase = testCase;
          this.getAllTest();
          this.update();
          eventBus.trigger('loading-end');
          this.loading = false;
        });
      });

      eventBus.on('loading-start', () => {
        this.loading = true;
      });

      eventBus.on('locale-changed', () => {
        this.getAllTest();
      });
    </script>

</TestCase>