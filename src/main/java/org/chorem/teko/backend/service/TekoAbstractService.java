package org.chorem.teko.backend.service;

import io.agroal.api.AgroalDataSource;
import org.chorem.teko.backend.entity.TekoJsonMapper;
import org.nuiton.spgeed.SqlSession;

import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Transactional
public abstract class TekoAbstractService {

    @Inject
    protected AgroalDataSource ds;

    protected SqlSession createSqlSession() {
        SqlSession sqlSession = new SqlSession(ds);
        sqlSession.setDefaultMapper(TekoJsonMapper.class);
        return sqlSession;
    }

}
