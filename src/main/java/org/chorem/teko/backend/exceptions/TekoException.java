package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;
import java.io.Serializable;

public class TekoException extends Exception implements Serializable {


    private static final long serialVersionUID = 8900860045504715698L;

    public TekoException() {
        super();
    }
    public TekoException(String msg)   {
        super(msg);
    }
    public TekoException(String msg, Exception e)  {
        super(msg, e);
    }

    public Response.Status getHttpStatus() {
        return Response.Status.INTERNAL_SERVER_ERROR;
    }
}
