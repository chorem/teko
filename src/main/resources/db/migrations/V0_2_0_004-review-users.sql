-- Table users
ALTER TABLE users DROP COLUMN salt;

-- still admin, but format change
UPDATE users SET password = '$2a$12$0Iya2bgwCPGtRPLVcTaiOOMsVtRPujRg/Kksh0rUxLdgcDVOr/SsO' WHERE id = '3cd769d1-3d62-4708-9026-2331a2c3f608';
