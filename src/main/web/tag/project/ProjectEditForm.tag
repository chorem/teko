let projectsService = require("../../service/ProjectService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');
let route = require("riot-route");

<ProjectEditForm>
  <div class="sheet">
    <div class="sheet-header">
      { translate('project.update')}
    </div>
    <div if="{error}" class="error-notification">{error}</div>
    <div class="sheet-content">
      <form ref="project" onsubmit="{edit}">
        <ul class="form-content">
          <li>
            <label for="name">{translate('project.name.label')}</label>
            <input type="text" name="name" placeholder="{translate('project.name.placeholder')}" required>
            <input type="hidden" name="id">
            <input type="hidden" name="creatorId">
          </li>
            <li>
                <label for="visibility">visibility</label>
                <select name="visibility">
                    <option value="PUBLIC" selected>Public</option>
                    <option value="PRIVATE">Private</option>
                </select>
            </li>
        </ul>

        <div class="form-buttons">
          <button onclick="{cancelAction}" class="button button--secondary">
            { translate('common.cancel')}
          </button>
          <input type="submit"
                 value="{translate('common.update')}"
                 class=" button button--main" />
        </div>
      </form>
    </div>
  </div>

  <script>
      this.translate = translate;
      this.error = "";

      projectsService.getProject(this.opts.projectId)
      .then((project) => {
          FormUtils.fillForm(this.refs.project, project);
      });

      this.edit = (e) => {
          e.preventDefault();
          e.stopPropagation();
          eventBus.trigger('loading-start');

          let project = FormUtils.formToMap(this.refs.project);
          projectsService.updateProject(this.opts.projectId, project)
          .then(() => {
              eventBus.trigger('project-updated');
              route('/project/'+this.opts.projectId);
          })
          .catch((msg) => {
              eventBus.trigger('loading-end');
              this.error = msg.message || translate('project.saveError');
              this.update();
          });
      };

      this.cancelAction = () => {
        route('/settings/'+this.opts.projectId);
      };

      eventBus.on('locale-changed', () => {
        this.update();
      });
  </script>

</ProjectEditForm>