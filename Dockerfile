# snapshot docker file, create docker from current git state
# use with:
#  docker build -f Dockerfile -t docker.chorem.com/teko .
#  docker run -p 80:80
FROM httpd:2.4

RUN apt-get update && apt-get -y install --no-install-recommends openjdk-11-jre && apt-get clean\
    && a2enmod rewrite

RUN echo '\
<Location /teko-backend/>\n\
    ProxyPass "http://localhost:8084/"\n\
    ProxyPassReverse "http://localhost:8084/"\n\
</Location>\n\
\n\
<Directory "/var/www/html/">\n\
    RewriteEngine On\n\
    RewriteCond /var/www/html/%{REQUEST_URI} -f\n\
    RewriteRule ^(.+) $1 [L]\n\
\n\
    RewriteRule "^[^.]*$"  "/index.html" [L]\n\
</Directory>\n\
\n\
' > /etc/apache2/conf-enabled/teko.conf

# Prepare apache2 server
COPY ./target/dist /var/www/html

# Prepare backend
RUN mkdir /usr/local/teko
COPY ./target/teko-backend-*-runner.jar /usr/local/teko/teko-backend.jar


EXPOSE 80

CMD ["java -jar", "/usr/local/teko/teko-backend.jar"]
CMD (service apache2 start)
