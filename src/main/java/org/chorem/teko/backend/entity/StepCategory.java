package org.chorem.teko.backend.entity;

/**
 * Created by martel on 04/07/17.
 **/
public enum StepCategory {

    PREREQUISITE,
    ACTION,
    RESULT,
    ;
}
