package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.StepCategory;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestStep;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestStepDaoTest extends AbstractDaoTest {

    protected static final UUID PROJECT_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_CASE_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_TWO_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_THREE = UUID.randomUUID();

    protected void fillDatabase(SqlSession session) {

        ProjectDao projectDao = session.getDao(ProjectDao.class);
        TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
        TestStepDao testStepDao = session.getDao(TestStepDao.class);
        initDB(session);

        //create project 1
        Project project1 = new Project();
        project1.setName("Project 1");
        project1.setId(PROJECT_ONE_UUID);
        project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
        projectDao.createProject(project1);

        //create testCase 1
        TestCase testCase1 = new TestCase();
        testCase1.setLabel("Test Case 1");
        testCase1.setId(TEST_CASE_ONE_UUID);
        testCase1.setRef("1");
        testCase1.setStatus(Status.AVAILABLE);
        testCase1.setProjectId(project1.getId());
        testCaseDao.createTestCase(testCase1);

        //create one TestStep, contained by testCase1
        TestStep testStep1 = new TestStep();
        testStep1.setLabel("Step 01");
        testStep1.setId(TEST_STEP_ONE_UUID);
        testStep1.setCategory(StepCategory.PREREQUISITE);
        testStepDao.createTestStep(testStep1);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_ONE_UUID);

        //create an other TestStep, contained by testCase1
        TestStep testStep2 = new TestStep();
        testStep2.setLabel("Step two");
        testStep2.setId(TEST_STEP_TWO_UUID);
        testStep2.setCategory(StepCategory.ACTION);
        testStepDao.createTestStep(testStep2);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_TWO_UUID);

        //create a third TestStep, contained by testCase1
        TestStep testStep3 = new TestStep();
        testStep3.setLabel("Step three");
        testStep3.setId(TEST_STEP_THREE);
        testStep3.setCategory(StepCategory.RESULT);
        testStepDao.createTestStep(testStep3);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_THREE);

    }

    @Test
    public void createAndGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            TestStepDao testStepDao = session.getDao(TestStepDao.class);
            fillDatabase(session);

            {
                TestStep[] testSteps = testStepDao.getAllForTestCase(TEST_CASE_ONE_UUID);
                Assertions.assertEquals(3, testSteps.length);
            }
        }
    }

    @Test
    public void detele() throws Exception {

        try (SqlSession session = createSqlSession()) {
            TestStepDao testStepDao = session.getDao(TestStepDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            fillDatabase(session);

            {
                testStepDao.removeTestStepOccurrences(TEST_STEP_ONE_UUID);
                testStepDao.deleteTestStep(TEST_STEP_ONE_UUID);
            }

            {
                TestCase testCase = testCaseDao.findById(TEST_CASE_ONE_UUID);
                Assertions.assertNotNull(testCase);
                Assertions.assertEquals(2, testCase.getSteps().length );
            }

            {
                TestStep[] testSteps = testStepDao.getAllForTestCase(TEST_CASE_ONE_UUID);
                Assertions.assertEquals(2, testSteps.length);
            }

        }
    }

    @Test
    public void deteleOrhan() throws Exception {

        try (SqlSession session = createSqlSession()) {
            TestStepDao testStepDao = session.getDao(TestStepDao.class);
            fillDatabase(session);
            UUID testStep4Id = UUID.randomUUID();

            {
                TestStep testStep4 = new TestStep();
                testStep4.setLabel("Step four");
                testStep4.setId(testStep4Id);
                testStep4.setCategory(StepCategory.RESULT);
                testStepDao.createTestStep(testStep4);
            }

            {
                TestStep testStep4 = testStepDao.getTestStep(testStep4Id);
                Assertions.assertNotNull(testStep4);
            }

            {
                // Only testStep4 is orphan ...
                testStepDao.deleteOrphanTestStep();
            }

            {
                TestStep testStep4 = testStepDao.getTestStep(testStep4Id);
                Assertions.assertNull(testStep4);
            }

        }
    }
}
