let testCaseService = require("../../service/TestCaseService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<TestCaseEditForm>
  <div class="sheet">
    <div class="sheet-header">
      { translate("testcase.edit") }
    </div>

    <div if="{error}" class="error-notification">{error}</div>

    <div class="sheet-content">
      <form ref="testCase" method="post" onsubmit="{edit}" class="modal">
        <ul class="form-content">
          <li>
            <label for="label">{ translate("testcase.name.label")}</label>
            <input type="text" name="label" placeholder="{translate('testcase.name.placeholder')}" required>
          </li>

          <li>
            <label for="criticality">{ translate("testcase.criticality.label") }</label>
            <select name="criticality">
              <option value="LOW">{ translate("testcase.criticality.low") }</option>
              <option value="MEDIUM">{ translate("testcase.criticality.medium" ) }</option>
              <option value="HIGH">{ translate("testcase.criticality.high" ) }</option>
              <option value="CRITICAL">{ translate("testcase.criticality.critical" ) }</option>
            </select>
            <input type="hidden" name="status">
            <input type="hidden" name="id">
          </li>
        </ul>

        <div class="form-buttons">
          <button class="button--secondary"
                  type="button"
                  onclick="{cancelAction}">
            { translate('common.cancel') }
          </button>
          <button if="{!testCase.status || testCase.status == 'DRAFT'}"
                  class="button--main"
                  type="button"
                  onclick="{editDraft}">
            { translate('common.saveDraft')}
          </button>
          <input type="submit"
                 value="{translate('common.update')}"
                 class="button--main">
        </div>
      </form>
    </div>
  </div>

  <script>
      this.translate = translate;
      this.error = "";
      this.testCase = {};

      testCaseService.getTestCase(this.opts.testCaseId)
      .then((testCase) => {
          this.testCase = testCase;
          FormUtils.fillForm(this.refs.testCase, testCase);
          this.update();
      });

      this.editDraft = (e) => {
          e.preventDefault();
          e.stopPropagation();

          let testCase = FormUtils.formToMap(this.refs.testCase);
          testCaseService.updateTestCase(this.opts.testCaseId, testCase)
          .then(() => {
              eventBus.trigger('testcase-updated');
              route('/project/'+this.testCase.projectId);
          })
          .catch((msg) => {
              this.error = msg || translate('testcase.saveError');
              this.update();
          });
      };

      this.edit = (e) => {
          e.preventDefault();
          e.stopPropagation();

          let testCase = FormUtils.formToMap(this.refs.testCase);
          testCase['status'] = "AVAILABLE";
          testCaseService.updateTestCase(this.opts.testCaseId, testCase)
          .then(() => {
              eventBus.trigger('testcase-updated');
              route('/project/'+this.testCase.projectId);
          })
          .catch((msg) => {
              this.error = msg || translate('testcase.saveError');
              this.update();
          });
      };

      this.cancelAction = () => {
        route('/project/'+this.testCase.projectId);
      }

      eventBus.on('locale-changed', () => {
        this.update();
      });
  </script>

</TestCaseEditForm>