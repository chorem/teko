package org.chorem.teko.backend;

import org.chorem.teko.backend.exceptions.LoginExceptionHandler;
import org.chorem.teko.backend.exceptions.TekoExceptionHandler;
import org.chorem.teko.backend.service.ProjectService;
import org.chorem.teko.backend.service.ReportsService;
import org.chorem.teko.backend.service.TestCampaignService;
import org.chorem.teko.backend.service.TestCaseService;
import org.chorem.teko.backend.service.TestStepService;
import org.chorem.teko.backend.service.UserService;

import javax.naming.NamingException;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by couteau on 27/01/17.
 */
//@ApplicationPath("/api")
public class TekoApplication extends Application {

    HashSet<Object> singletons = new HashSet<>();

    public TekoApplication() {
        singletons.add(new ProjectService());
        singletons.add(new TestCaseService());
        singletons.add(new TestStepService());
        singletons.add(new TestCampaignService());
        singletons.add(new ReportsService());
        singletons.add(new UserService());
        singletons.add(new CORSResponseFilter());

        singletons.add(new LoginExceptionHandler());
        singletons.add(new TekoExceptionHandler());
    }

    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> set = new HashSet<>();
        return set;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}