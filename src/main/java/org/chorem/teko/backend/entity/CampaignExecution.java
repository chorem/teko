package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class CampaignExecution {

    protected UUID id;

    protected UUID testCampaignId;

    protected OffsetDateTime executionDate;

    protected UUID[] testCaseResultIds;

    protected List<TestCaseResult> testCaseResults;

    protected OffsetDateTime creationDate;

    public CampaignExecution() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTestCampaignId() {
        return testCampaignId;
    }

    public void setTestCampaignId(UUID testCampaignId) {
        this.testCampaignId = testCampaignId;
    }

    //XXX ymartel 20170803 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setTestcampaignid(UUID testCampaignId) {
        this.testCampaignId = testCampaignId;
    }

    public OffsetDateTime getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(OffsetDateTime executionDate) {
        this.executionDate = executionDate;
    }

    //XXX ymartel 20170803 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setExecutiondate(OffsetDateTime executionDate) {
        this.executionDate = executionDate;
    }

    public UUID[] getTestCaseResultIds() {
        return testCaseResultIds;
    }

    public void setTestCaseResultIds(UUID[] testCaseResultIds) {
        this.testCaseResultIds = testCaseResultIds;
    }

    //XXX ymartel 20170803 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setTestcaseresultids(UUID[] testCaseResultIds) {
        this.testCaseResultIds = testCaseResultIds;
    }

    public TestCaseResult[] getTestCaseResults() {
        if (this.testCaseResults == null) {
            return null;
        } else {
            TestCaseResult[] tcrs = new TestCaseResult[this.testCaseResults.size()];
            return this.testCaseResults.toArray(tcrs);
        }
    }

    public void setTestCaseResults(TestCaseResult[] testCaseResults) {
        this.testCaseResults = new ArrayList<>(Arrays.asList(testCaseResults));
    }

    //XXX ymartel 20170830 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setTestcaseresults(TestCaseResult[] testCaseResults) {
        if (testCaseResults == null) {
            this.testCaseResults = null;
        } else {
            this.testCaseResults = new ArrayList<>(Arrays.asList(testCaseResults));
        }
    }

    public void addTestCaseResult(TestCaseResult testCaseResult) {
        if (this.testCaseResults == null) {
            this.testCaseResults = new ArrayList<>();
        }
        this.testCaseResults.add(testCaseResult);
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
