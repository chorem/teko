let translate = require("counterpart");

<ObsolesceConfirmationModal>
  <div class="modal-fullscreen {opened: showConfirmationDialog}" if={showConfirmationDialog}>
    <div class="modal-fullscreen-content">
      <div class="modal-fullscreen-title">
        {message} <span if="{object}" class="text-colored-bold">{object}</span>
      </div>
      <button class="button--secondary autofocus" onclick="{cancelAction}">{ translate('common.cancelDelete') }</button>
      <button class="button--main" onclick="{obsolesceAction}">{ translate('common.obsolesce') }</button>
      <button class="button--main" onclick="{deleteAction}">{ translate('common.confirmDelete') }</button>
    </div>
  </div>

  <script>
    this.translate = translate;

    this.showConfirmationDialog = false;

    this.open = (message, object) => {
      this.message = message;
      this.object = object;
      this.showConfirmationDialog = true;

      this.modalPromise = new Promise((resolve, reject) => {
          this.modalResolve = resolve;
          this.modalReject = reject;
      });

      this.update();
      $(".autofocus").focus();
      return this.modalPromise;
    };

    this.obsolesceAction = () => {
      this.showConfirmationDialog = false;
      this.modalResolve("OBSOLESCE");
      this.update();
    };

    this.deleteAction = () => {
      this.showConfirmationDialog = false;
      this.modalResolve("DELETE");
      this.update();
    };

    this.cancelAction = () => {
      this.showConfirmationDialog = false;
    };

    riot.mixin({confirmObsolesceModal: this.open});
  </script>
</ObsolesceConfirmationModal>