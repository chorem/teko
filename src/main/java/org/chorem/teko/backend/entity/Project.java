package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Created by couteau on 27/01/17.
 */
public class Project {
    protected UUID id;
    protected String name;

    protected int nbTestCases;

    protected UUID creatorId;

    protected Visibility visibility = Visibility.PUBLIC; // By default, make it public

    protected UUID[] authorizedUsersId;

    protected OffsetDateTime creationDate;

    public Project() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNbTestCases() {
        return nbTestCases;
    }

    public void setNbTestCases(int nbTestCases) {
        this.nbTestCases = nbTestCases;
    }

    public void setnbtestcases(int nbTestCases) {
        this.nbTestCases = nbTestCases;
    }

    public UUID getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(UUID creatorId) {
        this.creatorId = creatorId;
    }

    //XXX mlebras 20180704 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setcreatorid(UUID creatorId) {
        this.creatorId = creatorId;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public UUID[] getAuthorizedUsersId() {
        return authorizedUsersId;
    }

    //XXX mlebras 20180718 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setauthorizedusersid(UUID[] authorizedUsersId) {
        this.authorizedUsersId = authorizedUsersId;
    }

    public void setAuthorizedUsersId(UUID[] authorizedUsersId) {
        this.authorizedUsersId = authorizedUsersId;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
