let testCasesService = require("../../service/TestCaseService");
let testCaseService = require("../../service/TestCaseService");
let route = require("riot-route");
let translate = require("counterpart");
let eventBus = require("../../utils/TekoMagicEventBus");

<TestCases>
  <div if="{this.opts.filtersShow}" class="list-filters">
    <select id="criticality" onChange="{filterTestCase}">
      <option value="">{translate('criticality.ALL')}</option>
      <option value="LOW">{translate('criticality.LOW')}</option>
      <option value="MEDIUM">{translate('criticality.MEDIUM')}</option>
      <option value="HIGH">{translate('criticality.HIGH')}</option>
      <option value="CRITICAL">{translate('criticality.CRITICAL')}</option>
    </select>
    <select id="status" onChange="{filterTestCase}">
        <option value="">{translate('status.ALL')}</option>
        <option value="DRAFT">{translate('status.DRAFT')}</option>
        <option value="AVAILABLE">{translate('status.AVAILABLE')}</option>
        <option value="CLOSED">{translate('status.CLOSED')}</option>
    </select>
    <input type="" id="keyWord" onchange="{filterTestCase}" size="10" placeholder="{ translate('testcase.filter') }" >
    <select id="attribut" onChange="{filterTestCase}">
      <option value="date" selected>Chronologique</option>
      <option value="label">Alphabétique</option>
    </select>
    <a onclick="{sortTestCases}" title="Inverser l'ordre">
      <i data-icon class="icon-arrow-up" if="{!orderBool}"></i>
      <i data-icon class="icon-arrow-down" if="{orderBool}"></i>
    </a>
  </div>

  <ul class="list-items" id="maincontent">
    <li if="{!testCases || !testCases.length}" class="notification-no-results">
      { translate("testcase.x_testcases.zero") }
    </li>
    <li each="{testCase in testCases}" class="list-item list-item--link">
      <a href=""
          onclick="{selectTestCase(testCase.id, testCase.alterable)}"
          class="{active : testCase.id == activeTestCase} a--without-decoration"
          title="{testCase.label}">
        <span class="{draft-item : testCase.status == 'DRAFT'}">
          <span if="{testCase.status == 'DRAFT'}">{ translate("common.draft") }</span>
          {testCase.label}
        </span>
        <span if="{testCase.tags}" class="list-item--extra-info">
          [<span each="{tag, index in testCase.tags}">
            {tag}<span if="{index != testCase.tags.length - 1}">,</span>
          </span>]
        </span>
      </a>
      <a if="{login && testCase.alterable && !this.opts.testCaseId}"
         href="/testcase/{testCase.id}/edit"
         class="item-action"
         title="{translate('common.update')}">
        <span class="icon icon-edit"></span>
      </a>
      <a if="{login && testCase.alterable && !this.opts.testCaseId}"
         href=""
         class="item-action"
         onclick="{deleteConfirmation(testCase)}"
         title="{translate('common.delete')}">
        <span class="icon icon-delete"></span>
      </a>
    </li>
  </ul>

  <script>
      this.translate = translate;
      this.testCases = [];
      this.activeTestCase = '';
      this.projectId = this.opts.projectId;
      this.orderBool = false;
      this.login = localStorage.getItem('accessToken');

      this.fetchTestCases = () => {
        eventBus.trigger('loading-start');
        testCasesService.getTestCases(this.projectId)
          .then((testCases) => {
              this.testCases = testCases;
              this.update();
              eventBus.trigger('loading-end');
        })
      };
      this.fetchTestCases();

      this.selectTestCase = (id, alterable) => {
        return (e) => {
            eventBus.trigger('loading-start');
            this.activeTestCase = id;
            eventBus.trigger('navigation-testcase-changed', id, alterable);
        };
      };

      this.sortTestCases = () => {
          this.orderBool = !this.orderBool;
          this.filterTestCase();
      }

      this.filterTestCase = () => {
          let order = this.orderBool;
          let attribut = document.getElementById("attribut").value;
          let status = document.getElementById("status").value;
          let keyWord = document.getElementById("keyWord").value;
          let criticality = document.getElementById("criticality").value;
          testCaseService.getTestCasesWithFilter(this.projectId, keyWord, status, criticality, order, attribut)
            .then((testCases) => {
                if (testCases) {
                  this.testCases = testCases;
                  eventBus.trigger('fiter-testcase-changed', testCases.length);
                } else {
                  this.testCases = [];
                  eventBus.trigger('fiter-testcase-changed', 0);
                }
                this.update();
            });
      };

      this.deleteConfirmation = (testCase) => {
        return (e) => {
          message = translate('project.confirmDeleteLabel');
          this.confirmObsolesceModal(message, testCase.label).then((confirm) => {
            if (confirm === "OBSOLESCE") {
              this.obsolesce(testCase);
            } else if (confirm === "DELETE") {
              this.delete(testCase.id);
            }
          });
        }
      };

      this.delete = (id) => {
          testCaseService.deleteTestCase(id)
          .then(() => {
              eventBus.trigger('testcase-deleted', id);
          })
          .catch((msg) => {
              this.error = msg || translate('testcase.deletionError');
              this.update();
          });
      };

      this.obsolesce = (testCase) => {
          testCase['status'] = "OBSOLETE";
          testCaseService.updateTestCase(testCase.id, testCase)
          .then(() => {
              this.fetchTestCases();
          });
      };

      eventBus.on('testcase-updated', () => {
          this.fetchTestCases();
      });

      eventBus.on('testcase-deleted', () => {
          this.fetchTestCases();
          this.testCaseId = null;
          this.update();
      });

      eventBus.on('navigation-testcase-changed', (id) => {
        this.activeTestCase = id;
        this.update();
      });

      eventBus.on('locale-changed', () => {
        this.update();
      });
  </script>

</TestCases>
