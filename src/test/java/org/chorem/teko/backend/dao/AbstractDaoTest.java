package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.StepCategory;
import org.chorem.teko.backend.entity.TekoJsonMapper;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestStep;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.nuiton.spgeed.SqlSession;
import org.postgresql.ds.PGSimpleDataSource;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public abstract class AbstractDaoTest {

    protected static final UUID PROJECT_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_CASE_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_CASE_TWO_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_ONE_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_TWO_UUID = UUID.randomUUID();
    protected static final UUID TEST_STEP_THREE = UUID.randomUUID();

    private PostgreSQLContainer pgContainer;

    @BeforeEach
    public void init() {
        pgContainer = new PostgreSQLContainer<>("postgres:12")
            .withDatabaseName("teko")
            .withUsername("teko")
            .withPassword("teko");

        pgContainer.start();
    }
    @AfterEach
    public void endTest() {
        pgContainer.close();
    }



    protected SqlSession createSqlSession() {
        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setUrl(pgContainer.getJdbcUrl());
        ds.setUser(pgContainer.getUsername());
        ds.setPassword(pgContainer.getPassword());

        SqlSession sqlSession = new SqlSession(ds);
        sqlSession.setDefaultMapper(TekoJsonMapper.class);
        return sqlSession;
    }

    protected void initDB(SqlSession session) {
        DatabaseDao databaseDao = session.getDao(DatabaseDao.class);
        databaseDao.createDataBase();
        databaseDao.updateDatabaseWithUser();
        databaseDao.updateDatabaseWithProjectVisibility();
        databaseDao.updateDatabaseWithTestCaseResultLastUser();
    }

    protected void fillDatabase(SqlSession session) {
        initDB(session);

        ProjectDao projectDao = session.getDao(ProjectDao.class);
        TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
        TestStepDao testStepDao = session.getDao(TestStepDao.class);
        UserDao userDao = session.getDao(UserDao.class);

        //create project 1
        Project project1 = new Project();
        project1.setName("Project 1");
        project1.setId(PROJECT_ONE_UUID);
        project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
        projectDao.createProject(project1);

        //create testCase 1
        TestCase testCase1 = new TestCase();
        testCase1.setLabel("Test Case 1");
        testCase1.setId(TEST_CASE_ONE_UUID);
        testCase1.setRef("1");
        testCase1.setStatus(Status.AVAILABLE);
        testCase1.setProjectId(project1.getId());
        testCaseDao.createTestCase(testCase1);

        //create testCase 2
        TestCase testCase2 = new TestCase();
        testCase2.setLabel("Test Case 2");
        testCase2.setId(TEST_CASE_TWO_UUID);
        testCase2.setRef("2");
        testCase2.setStatus(Status.AVAILABLE);
        testCase2.setProjectId(project1.getId());
        testCaseDao.createTestCase(testCase2);

        //create one TestStep, contained by testCase1
        TestStep testStep1 = new TestStep();
        testStep1.setLabel("Step 01");
        testStep1.setId(TEST_STEP_ONE_UUID);
        testStep1.setCategory(StepCategory.PREREQUISITE);
        testStepDao.createTestStep(testStep1);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_ONE_UUID);

        //create an other TestStep, contained by testCase1
        TestStep testStep2 = new TestStep();
        testStep2.setLabel("Step two");
        testStep2.setId(TEST_STEP_TWO_UUID);
        testStep2.setCategory(StepCategory.ACTION);
        testStepDao.createTestStep(testStep2);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_TWO_UUID);

        //create a third TestStep, contained by testCase1
        TestStep testStep3 = new TestStep();
        testStep3.setLabel("Step three");
        testStep3.setId(TEST_STEP_THREE);
        testStep3.setCategory(StepCategory.RESULT);
        testStepDao.createTestStep(testStep3);
        testCaseDao.addStep(TEST_CASE_ONE_UUID, TEST_STEP_THREE);

    }
}
