class FormUtils {

    static formToMap(form) {
        let result = {};
        Array.prototype.forEach.call(form.elements, (e) => {
            if (e.name) {
                if (e.type === "checkbox") {
                    if (e.checked) {
                        // it is a multiple choices checkbox : use an array
                        if (e.getAttribute('multiplechoices') && e.getAttribute('multiplechoices') === "true") {
                            if (!result[e.name]) {
                                result[e.name] = [];
                                result[e.name].push(e.value);
                            } else if (Array.isArray(result[e.name])) {
                              result[e.name].push(e.value);

                            }
                        } else {
                            result[e.name] = e.value || true;
                        }
                    }
                } else if (e.type === "date") {
                    if (e.value !== "") {
                        result[e.name] = this.toISOTZString(e.value + "T12:00");
                    }
                } else if (e.type === "datetime-local") {
                    if (e.value !== "") {
                        result[e.name] = this.toISOTZString(e.value);
                    }
                } else if (e.type === "radio") {
                    Array.prototype.forEach.call(form.elements[e.name], function(r) {
                        if (r.checked) {
                            result[e.name] = r.value;
                        }
                    });
                } else if (e.tagName === "SELECT") {
                    if (e.multiple) {
                        let list = result[e.name] = [];
                        Array.prototype.forEach.call(e.selectedOptions, function(o) {
                            list.push(o.value);
                        });
                    } else {
                        result[e.name] = e.value;
                    }
                } else {
                    result[e.name] = e.value;
                }
            }
        });
        return result;
    }

    static fillForm(form, context, defaults = {}) {
        let formData = context._formData || context;
        Array.prototype.forEach.call(form.elements, (e) => {
            if (e.name) {
                let value =
                formData[e.name] || formData["_" + e.name] ||
                context[e.name] || context["_" + e.name] ||
                defaults[e.name] || "";

                if (e.type === "checkbox") {
                    if (Array.isArray(value)) {
                        e.checked = value.includes(e.value);
                    } else {
                        e.checked = !!value;
                    }
                } else if (e.type === "datetime-local") {
                    let d = new Date(value);
                    e.value = new Date(+d - d.getTimezoneOffset() * 60 * 1000).toISOString().replace("Z", "");
                } else if (e.type === "radio") {
                    Array.prototype.forEach.call(form.elements[e.name], function(r) {
                        if (r.value === value) {
                            r.checked = true;
                        }
                    });
                } else if (e.tagName === "SELECT") {
                    if (e.multiple) {
                        let values = new Set(Array.isArray(value) ? value : [value]);
                        Array.prototype.forEach.call(e.options, function(o) {
                            o.selected = values.has(o.value);
                        });
                    } else {
                        e.value = value;
                    }
                } else {
                    e.value = value;
                }
            }
        });
    }

    static toISOTZString(date) {
        let d = new Date(date);
        let tz = -d.getTimezoneOffset() * 60 * 1000;
        let result = new Date(+d - 2 * tz).toISOString();
        return result;
    }

}

module.exports = FormUtils;
