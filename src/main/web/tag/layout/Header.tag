require("./Menu.tag");
require("./HeaderContext.tag");

<header>

  <a href="#maincontent" id="skip-nav">Skip to main content (Press Enter)</a>
    <div class="header--left">
      <a href="/" class="header-logo a--without-decoration">
        <img alt="Logo Teko" src="/../img/logo-teko.svg"/>
      </a>

      <Menu/>
    </div>

    <headerContext/>

</header>