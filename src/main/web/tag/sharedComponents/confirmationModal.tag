let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require("counterpart");

<ConfirmationModal>
  <div class="modal-fullscreen {opened: showConfirmationDialog}" if={showConfirmationDialog}>
    <div class="modal-fullscreen-content">
      <div class="modal-fullscreen-title modal-fullscreen-title--with-icon">
        <i class="icon icon-bullhorn icon-size--huge pulsate-fwd"></i>
        <div>
          {message} <span if="{object}" class="text-colored-bold">{object}</span>
        </div>
        <a href="#" class="hidden-anchor autofocus"></a>
      </div>
      <button class="button--secondary" onclick="{cancelAction}">{ translate('common.cancelDelete') }</button>
      <button class="button--main" onclick="{confirmAction}">{ button || translate('common.confirmDelete') }</button>
    </div>
  </div>

  <script>
    this.translate = translate;

    this.showConfirmationDialog = false;

    this.open = (message, object, button) => {
      this.message = message;
      this.button = button;
      this.object = object;
      this.showConfirmationDialog = true;

      this.modalPromise = new Promise((resolve, reject) => {
          this.modalResolve = resolve;
          this.modalReject = reject;
      });

      this.update();
      $(".autofocus").focus();
      return this.modalPromise;
    };

    this.confirmAction = () => {
      this.showConfirmationDialog = false;
      this.update();
      this.modalResolve();
    };

    this.cancelAction = () => {
      this.showConfirmationDialog = false;
      this.update();
    };

    eventBus.on('esc-key-pressed', () => {
      this.cancelAction();
    });
    eventBus.on('enter-key-pressed', () => {
      this.confirmAction();
    });

    riot.mixin({confirmModal: this.open});
  </script>
</ConfirmationModal>