package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.CampaignExecution;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.TestCampaign;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestCaseResult;
import org.chorem.teko.backend.entity.Validity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class CampaignExecutionDaoTest extends AbstractDaoTest {

    @Test
    public void testCreate() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create a campaign
            TestCampaign campaign = new TestCampaign();
            UUID campaign01Id = UUID.randomUUID();
            campaign.setId(campaign01Id);
            String campaignName = "Version 0.1-alpha";
            campaign.setLabel(campaignName);
            campaign.setProjectId(PROJECT_ONE_UUID);
            campaign.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, TEST_CASE_TWO_UUID});
            campaign.setStatus(Status.AVAILABLE);

            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            int createResult = testCampaignDao.create(campaign);
            Assertions.assertEquals(1, createResult);

            // Create a CampaignExecution
            CampaignExecution campaignExecution = new CampaignExecution();
            UUID campaignExecutionId = UUID.randomUUID();
            campaignExecution.setId(campaignExecutionId);
            campaignExecution.setExecutionDate(OffsetDateTime.now(ZoneOffset.UTC));
            campaignExecution.setTestCampaignId(campaign01Id);

            CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
            createResult = campaignExecutionDao.create(campaignExecution);
            Assertions.assertEquals(1, createResult);

        }
    }

    @Test
    public void testGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create other test cases
            TestCase testCase02 = new TestCase();
            testCase02.setLabel("Test Case 02");
            UUID testCase02Id = UUID.randomUUID();
            testCase02.setId(testCase02Id);
            testCase02.setRef("1");
            testCase02.setStatus(Status.AVAILABLE);
            testCase02.setProjectId(PROJECT_ONE_UUID);
            TestCase testCase03 = new TestCase();
            testCase03.setLabel("Test Case 03");
            UUID testCase03Id = UUID.randomUUID();
            testCase03.setId(testCase03Id);
            testCase03.setRef("1");
            testCase03.setStatus(Status.AVAILABLE);
            testCase03.setProjectId(PROJECT_ONE_UUID);

            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            testCaseDao.createTestCase(testCase02);
            testCaseDao.createTestCase(testCase03);

            //create campaign
            TestCampaign campaign01 = new TestCampaign();
            UUID campaign01Id = UUID.randomUUID();
            campaign01.setId(campaign01Id);
            String campaign01Name = "Version 0.1-alpha";
            campaign01.setLabel(campaign01Name);
            campaign01.setProjectId(PROJECT_ONE_UUID);
            campaign01.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, testCase02Id});
            campaign01.setStatus(Status.AVAILABLE);

            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            testCampaignDao.create(campaign01);

            // Create a CampaignExecution
            CampaignExecution campaignExecution = new CampaignExecution();
            UUID campaignExecutionId = UUID.randomUUID();
            campaignExecution.setId(campaignExecutionId);
            OffsetDateTime executionDate = OffsetDateTime.now(ZoneOffset.UTC);
            campaignExecution.setExecutionDate(executionDate);
            campaignExecution.setTestCampaignId(campaign01Id);

            // some results
            TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
            TestCaseResult testCaseResult0101 = new TestCaseResult();
            UUID testCaseResult0101Id = UUID.randomUUID();
            testCaseResult0101.setId(testCaseResult0101Id);
            testCaseResult0101.setTestCaseId(TEST_CASE_ONE_UUID);
            testCaseResult0101.setValidity(Validity.OK);
            testCaseResult0101.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            testCaseResultDao.create(testCaseResult0101);
            TestCaseResult testCaseResult0102 = new TestCaseResult();
            UUID testCaseResult0102Id = UUID.randomUUID();
            testCaseResult0102.setId(testCaseResult0102Id);
            testCaseResult0102.setTestCaseId(testCase02Id);
            testCaseResult0102.setValidity(Validity.OK);
            testCaseResult0102.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            testCaseResultDao.create(testCaseResult0102);
            campaignExecution.setTestCaseResultIds(new UUID[]{testCaseResult0101Id, testCaseResult0102Id});

            CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
            campaignExecutionDao.create(campaignExecution);

            {
                CampaignExecution testCampaignExecution = campaignExecutionDao.findById(campaignExecutionId);
                Assertions.assertNotNull(testCampaignExecution);
                Assertions.assertEquals(campaignExecutionId, testCampaignExecution.getId());
                Assertions.assertEquals(campaign01Id, testCampaignExecution.getTestCampaignId());
                Assertions.assertTrue(executionDate.isEqual(testCampaignExecution.getExecutionDate()));
            }

            {
                CampaignExecution[] all = campaignExecutionDao.findAllByTestCampaign(campaign01Id);
                Assertions.assertEquals(1, all.length);
                Assertions.assertEquals(campaignExecutionId, all[0].getId());
                Assertions.assertEquals(2, all[0].getTestCaseResultIds().length);
                Assertions.assertEquals(2, all[0].getTestCaseResults().length);
                Assertions.assertEquals(testCaseResult0101Id, all[0].getTestCaseResults()[0].getId());
                Assertions.assertEquals(testCaseResult0102Id, all[0].getTestCaseResults()[1].getId());

            }

            // Create an other CampaignExecution
            CampaignExecution campaignExecution02 = new CampaignExecution();
            UUID campaignExecution02Id = UUID.randomUUID();
            campaignExecution02.setId(campaignExecution02Id);
            OffsetDateTime execution02Date = OffsetDateTime.now(ZoneOffset.UTC);
            campaignExecution02.setExecutionDate(execution02Date);
            campaignExecution02.setTestCampaignId(campaign01Id);

            // some results
            TestCaseResult testCaseResult0201 = new TestCaseResult();
            UUID testCaseResult0201Id = UUID.randomUUID();
            testCaseResult0201.setId(testCaseResult0201Id);
            testCaseResult0201.setTestCaseId(TEST_CASE_ONE_UUID);
            testCaseResult0201.setValidity(Validity.valueOf("OK"));
            testCaseResult0201.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            testCaseResultDao.create(testCaseResult0201);
            TestCaseResult testCaseResult0202 = new TestCaseResult();
            UUID testCaseResult0202Id = UUID.randomUUID();
            testCaseResult0202.setId(testCaseResult0202Id);
            testCaseResult0202.setTestCaseId(testCase02Id);
            testCaseResult0202.setValidity(Validity.valueOf("OK"));
            testCaseResult0202.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            testCaseResultDao.create(testCaseResult0202);
            campaignExecution02.setTestCaseResultIds(new UUID[]{testCaseResult0201Id, testCaseResult0202Id});

            campaignExecutionDao.create(campaignExecution02);

            {
                CampaignExecution[] all = campaignExecutionDao.findAll();
                Assertions.assertEquals(2, all.length);
            }

            {
                CampaignExecution[] all = campaignExecutionDao.findAllByTestCampaign(campaign01Id);
                Assertions.assertEquals(2, all.length);
            }

        }
    }
}
