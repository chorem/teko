package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.Project;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import java.util.UUID;

public class ProjectDaoTest extends AbstractDaoTest {

    @Test
    public void createAndGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            initDB(session);

            //create project 1
            Project project1 = new Project();
            project1.setName("Project 1");
            UUID project01Id = UUID.randomUUID();
            project1.setId(project01Id);
            project1.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project1);

            //create project 2
            Project project2 = new Project();
            project2.setName("Project 2");
            UUID project02Id = UUID.randomUUID();
            project2.setId(project02Id);
            project2.setCreatorId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));
            projectDao.createProject(project2);

            {
                Project[] all = projectDao.getProjects("","date", true, null);
                Assertions.assertEquals(2, all.length);
            }

            {
                Project p3 = projectDao.getProject(project1.getId());
                Assertions.assertEquals(project1.getName(), p3.getName());
                Assertions.assertEquals(project1.getId(), p3.getId());
            }

            {
                boolean isProject01Referenced = projectDao.isProjectReferencedByTestCase(project01Id);
                Assertions.assertFalse(isProject01Referenced);
            }

            {
                Project[] keyWord = projectDao.getProjects("%Project%","date", true, null);
                Assertions.assertEquals(2, keyWord.length);
                keyWord = projectDao.getProjects("%1%","date", true, null);
                Assertions.assertEquals(1, keyWord.length);
                Assertions.assertEquals(project1.getId(), keyWord[0].getId());
            }
        }
    }
}