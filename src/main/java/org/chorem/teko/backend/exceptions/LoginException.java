package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class LoginException extends TekoException {

    private static final long serialVersionUID = -8951273923098185152L;

    public LoginException(String msg) {
        super(msg);
    }

    @Override
    public Response.Status getHttpStatus() {
        return Response.Status.BAD_REQUEST; //TODO as no Response.Status.UNPROCESSABLE_ENTITY exists yet, be careful to intercept this...
    }
}
