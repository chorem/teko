let projectsService = require("../../service/ProjectService");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require("counterpart");
let jwt = require('jsonwebtoken');

<ProjectSettings>
  <div class="sheet">
    <div class="sheet-header">
      { translate("settings.label") }
    </div>

    <div class="sheet-content" if={project}>
      <span if="{user && user == project.creatorId}"
            class="header-context-title--buttons">
        <a href="/project/{project.id}/edit"
           title="{ translate('common.update') }"
           class="button button--secondary">
          <span class="icon icon-edit" /> { translate('common.update') }
        </a>
        <a if="{project.visibility == 'PRIVATE'}"
           href="/project/{project.id}/editUsers"
           title="{ translate('user.manage') }"
           class="button button--secondary">
          <span class="icon icon-lock" /> { translate('user.manage') }
        </a>
        <button
                onclick="{deleteConfirmation(project)}"
                title="{translate('common.delete') }"
                class="button button--secondary">
          <span class="icon icon-delete" /> {translate('common.delete') }
        </button>
      </span>

      <span if="{!user || user != project.creatorId}">
        { translate('common.security.unauthorizedAccess') }
      </span>
    </div>

    <div class="notification-error" if="{!project && !loading}">
      {error || translate("project.notFound")}
    </div>
  </div>

  <script>
      this.translate = translate;
      this.loading = true;

      if(localStorage.getItem('accessToken')){
          this.user = jwt.decode(localStorage.getItem('accessToken')).userId;
      }

      this.fecthProject = () => {
        projectsService.getProject(this.opts.projectId)
        .then((project) => {
            this.project = project;
            document.title = "Teko - " + project.name + " - " + this.translate('settings.label');
            this.update();
        })
        .catch((msg) => {
            this.error = msg
            this.project = null;
            this.update();
        });
      }
      this.fecthProject();

      this.deleteConfirmation = (project) => {
          return (e) => {
              message = translate('project.confirmDeleteLabel');
              this.confirmModal(message, project.name).then((confirm) => {
                  this.deleteProject(project.id);
              });
          }
      };

      this.deleteProject = (id) => {
          projectsService.deleteProject(id)
          .then(() => {
              route('/');
          })
          .catch((msg) => {
              this.error = msg || translate('project.deletionError');
              this.update();
          });
      };

      eventBus.on('locale-changed', () => {
        this.update();
      });
      eventBus.on('loading-end', () => {
        this.loading = false;
      });
  </script>

</ProjectSettings>
