package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class ForbiddenException extends TekoException {

    private static final long serialVersionUID = -8048781584470760721L;

    public ForbiddenException(String msg) {
        super(msg);
    }

    @Override
    public Response.Status getHttpStatus() {
        return Response.Status.FORBIDDEN;
    }

}
