/* eslint strict: 0 */
"use strict";

let webpack = require("webpack");
let CopyWebpackPlugin = require("copy-webpack-plugin");
let path = require('path');

module.exports = {
    entry: {
        site: "./src/main/web/index"
    },

    output: {
        filename: "teko.js",
        path: __dirname + "/target/dist/"
    },

    resolveLoader: {
        alias: {
            "riotjs-loader": require.resolve("./riotjs-loader"),
            'services': path.resolve(__dirname, './src/main/web/service'),
            'utils': path.resolve(__dirname, './src/main/web/utils'),
            'vues': path.resolve(__dirname, './src/main/web/tag')
        }
    },

    devServer: {
        historyApiFallback: {
            rewrites: [
                {from: /^\/pages\/(.*)/, to: function(context) {
                    return "/" + context.match[1];
                }},
                {from: /./, to: "/index.html"}
            ]
        }
    },

    plugins: [
        new webpack.ProvidePlugin({
            riot: "riot"
        }),

    new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
    }),

    new webpack.ProvidePlugin({
           bootstrap: "bootstrap.css",
    }),

        new CopyWebpackPlugin([
            {from: "src/main/web/conf.js"},
            {from: "src/main/web/index.html"},
            {from: "src/main/web/robots.txt"},
            {from: "src/main/resources/img", to: "img"},
            {from: "src/main/resources/fonts", to: "fonts"},
        ])
    ],

    module: {
        rules: [
            {
                test: /\.tag$/,
                exclude: /node_modules/,
                loader: "riotjs-loader",
                query: {type: "none"}
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.less/,
                loader: "style-loader!css-loader!less-loader"
            },
            {
              test: /\.(woff|woff2|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
              loader: 'url-loader'
            }
        ]
    }
};
