let testCaseService = require("../../service/TestCaseService");
let projectService = require("../../service/ProjectService");
let route = require("riot-route");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<TestTags>
    <div class="form-tags sheet-content--part" style="border-bottom: 0;">
      <label>{ translate("testtag.label")}</label>
      <ul class="tags-inline" if="{testTagsUsed.length}">
        <li each="{testTag in testTagsUsed}">
          <span class="board-list-item-label">{testTag}</span>
          <a href="" onclick="{removeTag(testTag)}" title="{translate('common.delete')}" if="{login && alterable}">
            <span class="icon icon-delete"></span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sheet-content--part">
      <select onchange="{addTag}" disabled="{!login || !alterable}">
          <option value="" selected>{ translate("testtag.choose")} &nbsp;&nbsp;&nbsp;&#9662;</option>
          <option each="{testTag in testTagsNotUsed}"
                  value="{testTag}"
                  id="{testTag}"
                  name="testTag">
              <label for="{testTag}">{testTag}</label>
          </option>
      </select>
      { translate("common.or")}
      <TestTagForm alterable="{alterable}"/>
    </div>
    <script>
        this.translate = translate;
        this.testTags = [];
        this.testTagsUsed = [];
        this.testTagsNotUsed = [];
        this.testCaseId = this.opts.testCaseId;
        this.alterable;
        this.login = localStorage.getItem('accessToken');

        this.getAlltags = () => {
            this.testTags = [];
            testCaseService.getTestCase(this.testCaseId)
            .then((testCase)=> {
                projectService.getProjectTags(testCase.projectId)
                .then((tags) => {
                    if(tags) {
                        this.testTags = tags;
                    }
                    this.tagsUsed();
                    this.update();
                })
            })
        };

        this.tagsUsed = () => {
            this.testTagsUsed = [];
            testCaseService.getTestCaseTags(this.testCaseId)
            .then((tags)=> {
                if (tags) {
                    this.testTagsUsed = tags;
                }
                this.tagsNotUsed();
                this.update();
            })
        };

        this.tagsNotUsed = () => {
            this.testTagsNotUsed = [];
            this.testTags.forEach((tag) => {
                var index = 0;
                while (index < this.testTagsUsed.length && this.testTagsUsed[index] != tag.tags) index++;
                if(index == this.testTagsUsed.length){
                    this.testTagsNotUsed.push(tag.tags);
                }
            });
            this.update();
        };

        this.addTag = (e) => {
            let tag = e.target.value;
            if (tag != "") {
                eventBus.trigger('addtesttag', tag);
            }
            this.update();
        };

        this.removeTag = (tag) => {
            return (e) => {
                eventBus.trigger('removetesttag', tag);
                this.update();
            };
        };

        eventBus.on('tagfolder', () => {
            this.tagsUsed();
            this.update();
        });

        eventBus.on('navigation-testcase-changed', (testCaseId, alterable) => {
            this.alterable = alterable;
            this.testCaseId = testCaseId;
            this.getAlltags();
            this.update();
        });

        eventBus.on('testTag-created', () => {
            this.getAlltags();
            this.update();
        });

        eventBus.on('locale-changed', () => {
            this.update();
        });
    </script>

</TestTags>