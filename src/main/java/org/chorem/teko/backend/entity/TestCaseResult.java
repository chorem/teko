package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestCaseResult {

    protected UUID id;

    protected UUID testCaseId;

    protected Validity validity;

    protected String comment;

    protected TestCase testCase;

    protected UUID lastUserId;

    protected OffsetDateTime creationDate;

    public TestCaseResult() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(UUID testCaseId) {
        this.testCaseId = testCaseId;
    }

    //XXX ymartel 20170803 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setTestcaseid(UUID testCaseId) {
        this.testCaseId = testCaseId;
    }

    public Validity getValidity() {
        return validity;
    }

    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public TestCase getTestCase() {
        return testCase;
    }

    public void setTestCase(TestCase testCase) {
        this.testCase = testCase;
    }

    //XXX ymartel 20170823 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setTestcase(TestCase testCase) {
        this.testCase = testCase;
    }

    public UUID getLastUserId() {
        return lastUserId;
    }

    public void setLastUserId(UUID lastUserId) {
        this.lastUserId = lastUserId;
    }

    //XXX ymartel 20181128 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setLastuserid(UUID lastUserId) {
        this.lastUserId = lastUserId;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
