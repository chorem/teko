package org.chorem.teko.backend.dto;

import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.TestCampaign;
import org.chorem.teko.backend.entity.TestCase;

import java.util.Collection;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestCampaignDto extends TestCampaign {

    protected Project project;

    protected Collection<TestCase> testCases;

    public TestCampaignDto(TestCampaign campaign) {
        this.setId(campaign.getId());
        this.setLabel(campaign.getLabel());
        this.setProjectId(campaign.getProjectId());
        this.setTestCaseIds(campaign.getTestCaseIds());
        this.setStatus(campaign.getStatus());
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<TestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(Collection<TestCase> testCases) {
        this.testCases = testCases;
    }
}
