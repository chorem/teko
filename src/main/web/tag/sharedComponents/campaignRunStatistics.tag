let translate = require("counterpart");
let chart = require("chart.js");

<campaignRunStatistics>
  <div class="statistics-chart-pie">
      <canvas ref="chart" width="100" height="100"></canvas>
  </div>

  <script>
    this.translate = translate;
    this.executionResults = null;
    this.chart = null;

    let getChartData = () => {
      let data = [this.opts.count, 0, 0];
      let testResults = Object.values(this.opts.results);
      let counts = {};

      for (let i = 0; i < testResults.length; i++) {
        let resultValue = testResults[i];
        counts[resultValue] = counts[resultValue] ? counts[resultValue] + 1 : 1;
      }
      if (counts['KO']) {
        data[1] = counts['KO'];
        data[0] = data[0] - counts['KO'];
      }
      if (counts['OK']) {
        data[2] = counts['OK'];
        data[0] = data[0] - counts['OK'];
      }

      return data;
    }

    let updateChart = () => {
      this.chart.data.datasets[0].data = getChartData();
      this.chart.update();
    };

    let initChart = () => {
      let ctx = this.refs.chart.getContext("2d");

      let chartDataSet = {
          labels: ["Non exécutés", "Non conformes", "Conformes"],
          datasets: [{
              label: 'Tests',
              data: [0,0,0],
              backgroundColor: [
                  '#b4b4b4',
                  '#eb3f4e',
                  '#b8f8c0'
              ],
              borderWidth: 0
          }]
      };

      let chartOptions = {
        cutoutPercentage: 50,
        legend : {
          display: false
        },
        tooltips : {
          bodyFontSize : 10
        }
      };

      this.chart = new Chart(ctx, {
          type: 'doughnut',
          data: chartDataSet,
          options: chartOptions
      });
    }

    this.on('mount', initChart);
    this.on('update', updateChart);

  </script>
</campaignRunStatistics>
