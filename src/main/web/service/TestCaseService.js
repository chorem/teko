let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class TestCaseService extends FetchService {

    getTestCases(projectId, order, attribut) {
        return this.get("/testCase/getAll/" +projectId + "?" + (order ? "order=" + order : "") + "&" + (attribut ? "attribut=" + attribut : ""));
    }

    createTestCase(projectId, testCase) {
        return this.post("/testCase/create/"+projectId, testCase);
    }

    getTestCase(id) {
        return this.get("/testCase/get/"+ id);
    }

    getTestCasesWithFilter(projectId, keyWord, status, criticality, order, attribut){
        return this.get("/project/"+projectId+"/testCase/filter" + "?" + ( keyWord ? "keyWord="+keyWord : "")
                                                                       + "&"
                                                                       + ( status ? "status="+status : "")
                                                                       + "&"
                                                                       + ( criticality ? "criticality="+criticality : "")
                                                                       + "&"
                                                                       + (order ? "order=" + order : "") + "&" + (attribut ? "attribut=" + attribut : ""));
    }

    getTestCaseTags(id) {
        return this.get("/testCase/"+ id+"/tags");
    }

    addTestTag(id, tag){
        return this.post("/testCase/"+id+"/tags", tag);
    }

    removeTestTag(id, tag){
        return this.delete("/testCase/"+id+"/tags", tag);
    }


    updateTestCase(id, testCase) {
        return this.post("/testCase/update/"+ id, testCase);
    }

    addCondition(id, conditionId){
        return this.post("/testCase/"+id+"/conditions/"+conditionId);
    }

    removeCondition(id, conditionId){
        return this.delete("/testCase/"+id+"/conditions/"+conditionId);
    }

    getCondition(id, withDepth){
        return this.get("/testCase/"+id+"/conditions" + (withDepth ? "?withDepth=" + withDepth : ""));
    }

    getDependOf(id, withDepth){
        return this.get("/testCase/"+id+"/dependOf" + (withDepth ? "?withDepth=" + withDepth : ""));
    }

    deleteTestCase(id) {
        return this.delete("/testCase/" + id);
    }
}

module.exports = singleton(TestCaseService);