Prérequis Développeur
=====================

- Maven 3
- Postgresql 9 minimum
- npm

Démarrer l'application
======================

L'application se compose de deux parties : le backend et le frontend.

Teko Backend
------------

Il faut au préalable avoir une base de données `teko` (sous postgresql donc) et l'utilisateur `teko` ayant les droits dessus.
```
CREATE USER teko WITH PASSWORD 'xxx';
CREATE DATABASE teko OWNER teko;
```

Il faut ensuite lancer le script contenu dans `src/main/resources/initDb.sql` pour initialiser la base de données.\
```
psql -d teko -U teko -f {PATH_TEKO}/src/main/resources/initDb.sql
```

Ensuite, il est nécessaire de donner à l'utilisateur les droits sur les nouvelles entrées en bases de données :\
```
GRANT SELECT, INSERT, UPDATE, DELETE ON project TO teko;
GRANT SELECT, INSERT, UPDATE, DELETE ON testcase TO teko;
GRANT SELECT, INSERT, UPDATE, DELETE ON teststep TO teko;
```

Une fois ces formalités validées, lancer simplement `mvn tomcat7:run` !

Teko Frontend
-------------

Pour le front, il suffit d'installer l'application et ses modules : `npm install` (qui se chargera d'installer suivant le contenu de package.json).\
Ensuite, pour lancer l'application, `npm start` vous permettra de lancer webpack-dev-server et de profiter du frontend !

