let translate = require("counterpart");

<pageNotFound>
  <div class="notification-no-results">
    <i class="icon icon-bullhorn icon-size--huge pulsate-fwd"></i>
    <br/>
    {translate("common.404")}
  </div>

  <script>
    this.translate = translate;

  </script>
</pageNotFound>
