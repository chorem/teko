let testStepService = require("../../service/TestStepService");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require("counterpart");

<TestStepInline>
  <div>
    <li class="list-item">
      <TestStep test-step="{testStep}" test-case-id="{testCaseId}" />
      <span>
        <button class="icon-on-hover link"
                title="{translate('common.update')}"
                onclick="{editTestStep}"
                if="{login && opts.alterable}">
          <span class="icon-edit"></span>
        </button>
        <button class="icon-on-hover link"
                title="{translate('common.delete')}"
                onclick="{deleteConfirmation}"
                if="{login && opts.alterable}">
          <span class="icon-delete"></span>
        </button>
      </span>
    </li>
  </div>

  <TestStepEditForm if = { isEdit } test-step="{testStep}" />
  <div class="error">{error}</div>

    <script>
        this.translate = translate;
        this.isEdit = false;
        this.testCaseId = this.opts.testCaseId;
        this.testStep = this.opts.data;
        this.login = localStorage.getItem('accessToken');

        this.deleteConfirmation = () => {
            message = translate('teststep.confirmDeleteLabel');
            this.confirmModal(message, this.testStep.label).then((confirm) => {
                this.delete();
            });
        };

        this.editTestStep = () => {
            this.isEdit = true;
        };

        this.delete = () => {

            testStepService.deleteTestStep(this.testStep.id, this.testCaseId)
            .then(() => {
                eventBus.trigger('step-deleted');
            })
            .catch((msg) => {
                this.error = msg || translate('teststep.deletionError');
                this.update();
            });

        };

        eventBus.on('locale-changed', () => {
          this.update();
        });

    </script>

</TestStepInline>