let translate = require('counterpart');
let route = require("riot-route");
let axios = require('axios');
let qs = require('qs');
let usersService = require("../../service/UserService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus.js");


<UserLogin class="columns-display-for-large-screen">
    <div class="sheet">
      <div class="sheet-header">
        { translate('user.login')}
      </div>
      <div class="sheet-content inputs-full-width">
        <form ref="user" onsubmit="{login}" class="form-content">
          <ul class="form-list">
              <li>
                  <label for="mail">{translate('user.mail.mail')}</label>
                  <input type="email" name="mail" id="mail1" placeholder="{translate('user.mail.placeholder')}" required autofocus>
              </li>
              <li>
                  <label for="password">{translate('user.password.password')}</label>
                  <input type="password" name="password" placeholder="{translate('user.password.placeholder')}" required>
              </li>
          </ul>

          <div class="form-buttons">
            <input type="submit" value="{translate('user.login')}" class="button--main">
          </div>

          <div if="{error}" class="notification-error">
            <i class="icon icon-bullhorn pulsate-fwd"></i>
            {error}
          </div>
        </form>
      </div>
    </div>

    <UserForm sign-up-form="1">

    <script>
        this.translate = translate;
        this.error = "";

        this.login = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let user = FormUtils.formToMap(this.refs.user);
            axios.post(window.BACKEND_URL + "/v1/users/login", qs.stringify(user))
                .then((login) => {
                localStorage.accessToken = login.data;
                eventBus.trigger('login');
                route('/');
            })
                .catch((msg) => {
                  console.log(msg);
                this.error = msg.message || translate('user.saveError');
                this.update();
            });
        };

        eventBus.on('locale-changed', () => {
            this.update();
        });
    </script>

</UserLogin>