let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');
let FormUtils = require("../../utils/FormUtils");
let jwt = require('jsonwebtoken');

<TestCaseResultForm class="execution-form" if="{testCaseResult && testCaseResult.validity}">

  <div class="form-buttons">
    <button type="button" onclick="{invalidTestResult}" class="button--validation-ko">
      <i class="icon icon-ko"/>Non conforme
    </button>
    <button type="button" onclick="{validTestResult}" class="button--validation-ok">
      <i class="icon icon-ok"/> Conforme
    </button>
    <a if="{testCaseResult.validity == 'OK' || testCaseResult.validity == 'KO'}" href="" class="button button-execution--NOT_TESTED" onclick="{cancelTestResult}">Cancel</a>
  </div>

  <form method="post" ref="testCaseResultForm" onsubmit="{addTestResult}"
    class="columns-display-for-large-screen">
    <input type="hidden" value="{testCaseResult.testCaseId}" name="testCaseId" />

    <div>
      <label for="comment">Commentaire</label>
      <textarea rows="3" cols="50" name="comment" placeholder="your comment" value="{testCaseResult.comment}"/>
    </div>
    <div>
      <label for="info">Information</label>
      <textarea rows="3" cols="50" name="info" value="{testCaseResult.info}" disabled="disabled"/>
    </div>
  </form>

  <script>
    this.translate = translate;
    this.testCaseResult = this.opts.testCaseResult ;

    // Reload on each change in navigation
    eventBus.on('navigation-testcaseresult-changed', (testCaseResult) => {
      this.testCaseResult = testCaseResult;
      this.update();
    });

    this.validTestResult = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let testCaseResultExecution = FormUtils.formToMap(this.refs.testCaseResultForm);
        if(this.testCaseResult.validity == 'CAN_NOT_TESTED'){
            testCaseResultExecution.info += translate("campaign.execution.error");
        }
        testCaseResultExecution.validity = 'OK';
        testCaseResultExecution.lastUserId = jwt.decode(localStorage.getItem('accessToken')).userId;
        eventBus.trigger('update-testcaseresult-execution', testCaseResultExecution);
    };

    this.invalidTestResult = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let testCaseResultExecution = FormUtils.formToMap(this.refs.testCaseResultForm);
        if(this.testCaseResult.validity == 'CAN_NOT_TESTED' ){
            testCaseResultExecution.info += translate("campaign.execution.error");
        }
        testCaseResultExecution.validity = 'KO';
        testCaseResultExecution.lastUserId = jwt.decode(localStorage.getItem('accessToken')).userId;
        eventBus.trigger('update-testcaseresult-execution', testCaseResultExecution);
    };

    this.cancelTestResult = (e) => {
        e.preventDefault();
        e.stopPropagation();
        let testCaseResultExecution = FormUtils.formToMap(this.refs.testCaseResultForm);
        let info = testCaseResultExecution.info;
        split = info.split('\n');
        if(split.length > 4){
            testCaseResultExecution.info = split[0] + '\n' + split[1] + '\n' + split[2] + '\n' + split[3] + '\n';
            testCaseResultExecution.validity = 'CAN_NOT_TESTED';
        }else{
            testCaseResultExecution.validity = 'NOT_TESTED';
        }
        eventBus.trigger('update-testcaseresult-execution', testCaseResultExecution);
    };

    eventBus.on('locale-changed', () => {
      this.update();
    });

  </script>

</TestCaseResultForm>