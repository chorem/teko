package org.chorem.teko.backend.reports;

import net.sf.jasperreports.engine.JasperReport;

import java.util.List;

/**
 * Created by couteau on 25/03/17.
 */
public class ProjectTestCasesReport {
    protected String projectName;
    protected String generationDate;
    protected List<TestCaseReportExtract> testCases;
    protected JasperReport subReport;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(String generationDate) {
        this.generationDate = generationDate;
    }

    public List<TestCaseReportExtract> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TestCaseReportExtract> testCases) {
        this.testCases = testCases;
    }

    public JasperReport getSubReport() {
        return subReport;
    }

    public void setSubReport(JasperReport subReport) {
        this.subReport = subReport;
    }
}
