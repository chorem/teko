let projectsService = require("../../service/ProjectService");
let testCasesService = require("../../service/TestCaseService");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require("counterpart");

<Project>
  <div class="list {aside : testCaseId}" if={!showTestCaseForm}>
    <div class="list-header" if="{project}">
      <div class="list-header-title">
        <b>{testCasesCount}</b>
        <span if="{!testCaseId}">{ translate("testcase.label")}</span>
      </div>
      <div if="{!testCaseId}" class="list-header-button align-right">
        <button onclick="{showFilters}" class="{active : testCasesFiltersShow}">
          <i class="icon icon-filter" title="{translate("testcase.filter")}"></i>
          {translate("common.filterAndSort")}
        </button>
      </div>
      <div if="{login}" class="list-header-button {align-right: testCaseId}">
        <button onclick="{createTestCase}">
          <i class="icon icon-plus"></i>
          <span if="{!testCaseId}">{ translate("testcase.create") }</span>
        </button>
      </div>
      <div if="{testCaseId}" class="list-header-button">
        <button onclick="{unselectTestCase}">
          <i class="icon icon-cancel"></i>
        </button>
      </div>
    </div>

    <TestCases project-id={project.id}
               if="{project}"
               test-case-id="{testCaseId}"
               filters-show="{testCasesFiltersShow}" />

    <div class="notification-error" if={!project}>
        {error || translate("project.notFound")}
    </div>
  </div>


  <TestCase if={!showTestCaseForm} test-case-id="{testCaseId}" show={testCaseId} />

  <TestCaseForm if={showTestCaseForm} project-id={project.id}/>

  <script>
    this.translate = translate;
    this.project = { id: this.opts.projectId};
    this.testCasesCount = 0;
    this.testCaseId = null;
    this.testCasesFiltersShow = false;
    this.login = localStorage.getItem('accessToken');

    this.fetchProject = () => {
      projectsService.getProject(this.opts.projectId)
      .then((project) => {
          this.project = project;
          document.title = "Teko - " + project.name + " - " + this.translate('testcase.label');
          eventBus.trigger('loading-end');
      })
      .catch((msg) => {
          this.error = msg
          this.project = null;
          this.update();
      });
    }
    this.fetchProject();

    this.fetchTestCasesCount = () => {
      testCasesService.getTestCases(this.opts.projectId)
      .then((testCases) => {
        this.testCasesCount = 0;
        if (testCases) {
          this.testCasesCount = testCases.length
        }
        this.update();
      })
    };
    this.fetchTestCasesCount();

    this.createTestCase = () => {
      this.showTestCaseForm = true;
    };

    this.unselectTestCase = () => {
      this.testCaseId = null;
    };

    this.showFilters = () => {
      this.testCasesFiltersShow = !this.testCasesFiltersShow;
    }

    eventBus.on('testcase-created', () => {
      this.fetchTestCasesCount();
    });

    eventBus.on('testcase-deleted', () => {
      this.testCaseId = null;
      this.fetchTestCasesCount();
      this.update();
    });

    eventBus.on('navigation-testcase-changed', (id) => {
      this.testCaseId = id;
      this.update();
    });

    eventBus.on('fiter-testcase-changed', (testCaseLength) => {
      this.testCasesCount = testCaseLength;
      this.update();
    });

    eventBus.on('locale-changed', () => {
      this.update();
    });
  </script>

</Project>
