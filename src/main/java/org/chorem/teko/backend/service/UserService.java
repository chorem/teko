package org.chorem.teko.backend.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.chorem.teko.backend.TekoConfig;
import org.chorem.teko.backend.dao.ProjectDao;
import org.chorem.teko.backend.dao.UserDao;
import org.chorem.teko.backend.dto.RegisterDto;
import org.chorem.teko.backend.entity.User;
import org.chorem.teko.backend.exceptions.LoginException;
import org.chorem.teko.backend.exceptions.UnauthorizedException;
import org.nuiton.spgeed.SqlSession;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by lebras on 19/06/18.
 *
 * @author ymartel (martel@codelutin.com)
 */
@Path("/api/v1/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserService extends TekoAbstractService {

    @Inject
    TekoConfig tekoConfig;

    @Inject
    TokenService tokenService;

    @GET
    @Path("/ping")
    public boolean ping() {
        return true;
    }

    private String encryptPassword(String password) {

        int hashCost = tekoConfig.securityHashCost();
        String hash = BCrypt.withDefaults().hashToString(hashCost, password.toCharArray());
        return hash;
    }

    private boolean isValidPassword(String given, String hashedPassword) {

        BCrypt.Result validation = BCrypt.verifyer().verify(given.toCharArray(), hashedPassword.toCharArray());
        return validation.verified;
    }

    @POST
    @Path("")
    public UUID create(RegisterDto registerDto,
                       @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();

        tokenService.checkToken(authorization);

        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        String hashedPassword = encryptPassword(registerDto.getPassword());
        user.setPassword(hashedPassword);
        user.setId(UUID.randomUUID());
        UserDao dao = session.getDao(UserDao.class);
        dao.createUser(user);
        return user.getId();

    }

    @GET
    @Path("")
    public User[] getAll(@QueryParam("order") boolean order,
                         @QueryParam("attribut") String attribut,
                         @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        tokenService.checkToken(authorization);
        UserDao userDao = session.getDao(UserDao.class);

        return userDao.getAll(attribut, order);

    }

    @GET
    @Path("/{id}")
    public User getUser(@PathParam("id") UUID id) throws Exception {
        SqlSession session = createSqlSession();
        UserDao dao = session.getDao(UserDao.class);

        return dao.getUser(id);
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String login(@FormParam("mail") String mail,
                        @FormParam("password") String password) throws Exception {
        SqlSession session = createSqlSession();
        UserDao dao = session.getDao(UserDao.class);

        User user = dao.getUserPassword(mail);
        if (user == null || !isValidPassword(password, user.getPassword())) {
            throw new LoginException("Invalid combinaison.");
        }

        return tokenService.createToken(user);
    }

    @POST
    @Path("/signin")
    public UUID signIn(RegisterDto registerDto,
                       @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();

        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setMail(registerDto.getMail());
        String hashedPassword = encryptPassword(registerDto.getPassword());
        user.setPassword(hashedPassword);
        user.setId(UUID.randomUUID());
        UserDao dao = session.getDao(UserDao.class);
        dao.createUser(user);
        return user.getId();

    }

    @POST
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateUser(@PathParam("id") UUID id,
                           @FormParam("firstName") String firstName,
                           @FormParam("lastName") String lastName,
                           @FormParam("newPassword") String newPassword,
                           @FormParam("password") String password,
                           @FormParam("mail") String mail,
                           @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();

        UUID sessionUserId = tokenService.checkToken(authorization);

        if (sessionUserId.equals(id)) {
            UserDao dao = session.getDao(UserDao.class);
            User user = dao.getUserPassword(mail);
            if (isValidPassword(password, user.getPassword())) {
                String hashedPassword = encryptPassword(newPassword);
                dao.updateUser(id, firstName, lastName, hashedPassword);
            } else {
                throw new UnauthorizedException("Invalid given password");
            }
        } else {
            throw new UnauthorizedException("Not allowed to modify user");
        }
    }

    // TODO: 25/02/2020 ymartel : disable for the moment. Need Role management
    //@DELETE
    //@Path("/{id}")
    public void deleteUser(@PathParam("id") UUID id,
                           @HeaderParam("Authorization") String authorization,
                           @FormParam("password") String password) throws Exception {
        SqlSession session = createSqlSession();
        UUID userUuid = tokenService.checkToken(authorization);

        UserDao userDao = session.getDao(UserDao.class);
        User user = userDao.getFullUserById(userUuid);
        if (!isValidPassword(password, user.getPassword())) {
            throw new UnauthorizedException("Invalid given password");
        }

        // Do'nt delete user : anonymize him
        String randomPassword = encryptPassword(UUID.randomUUID().toString());
        LocalDateTime now = LocalDateTime.now();
        userDao.updateUser(id, "anonymzedUser" + now, null, randomPassword);

        // Remove it from all projects
        ProjectDao projectDao = session.getDao(ProjectDao.class);
        projectDao.removeAuthorizedUserFromAllProjects(userUuid);

        //TODO ymartel 09/07/2020 : What do we do if user is a project owner ?
    }

}
