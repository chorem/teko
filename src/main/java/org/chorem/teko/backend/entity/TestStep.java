package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Created by couteau on 05/02/17.
 **/
public class TestStep {

    protected UUID id;
    protected String label;
    protected StepCategory category;

    protected OffsetDateTime creationDate;

    public TestStep() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public StepCategory getCategory() {
        return category;
    }

    public void setCategory(StepCategory category) {
        this.category = category;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
