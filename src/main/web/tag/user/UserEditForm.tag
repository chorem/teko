let usersService = require("../../service/UserService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');
let axios = require('axios');
let qs = require('qs');


<UserEditForm>
  <div class="sheet">
    <div class="sheet-header">
      { translate('user.update')}
    </div>

    <div class="sheet-content">
      <form ref="user" onsubmit="{edit}">
        <ul class="form-content">
            <li>
                <label for="firstName">{translate('user.firstName.name')}</label>
                <input type="text" name="firstName" placeholder="{translate('user.firstName.placeholder')}" autofocus>
                <input type="hidden" name="id">
                <input type="hidden" name="mail">
            </li>
            <li>
                <label for="lastName">{translate('user.lastName.name')}</label>
                <input type="text" name="lastName" placeholder="{translate('user.lastName.placeholder')}" autofocus>
            </li>
            <li>
                <label for="password">{translate('user.password.current')}</label>
                <input type="password" name="password" placeholder="{translate('user.password.placeholder')}" required>
            </li>
            <li>
                <label for="newPassword">{translate('user.password.new')}</label>
                <input type="password" name="newPassword" id="ps1" placeholder="{translate('user.password.placeholder')}" required>
            </li>
            <li>
                <label for="newPassword">{translate('user.password.confirmNew')}</label>
                <input type="password" name="newPassword" id="ps2" placeholder="{translate('user.password.placeholder')}" required onBlur="{checkPassword}">
                <i class="icon icon-cancel" if="{password == false}"></i>
            </li>
        </ul>

        <div class="form-buttons">
          <button class="button--secondary"
                  type="button"
             onclick="{cancelAction}">{ translate('common.cancel')}</button>
          <input if="{password}" type="submit" value="{translate('common.update')}" class="button--main">
        </div>

        <div if="{error}" class="error-notification">{error}</div>
    </form>

    <script>
        this.translate = translate;
        this.error = "";
        this.password = true;

        usersService.getUser(this.opts.userId)
            .then((user) => {
            FormUtils.fillForm(this.refs.user, user);
        });

        this.checkPassword = () =>{
            if (document.getElementById("ps1").value == document.getElementById("ps2").value) {
                this.password = true;
            } else {
                this.password = false;
            }
            this.update();
        }

        this.edit = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let user = FormUtils.formToMap(this.refs.user);
            usersService.updateUser(user)
                .then(() => {
                eventBus.trigger('user-updated');
            this.parent.showUserEditForm = false;
            this.parent.update();
        })
        .catch((msg) => {
                this.error = msg || translate('user.saveError');
            this.update();
        });
        };

        this.cancelAction = () => {
            this.parent.showUserEditForm = false;
            this.parent.update();
        };

        eventBus.on('locale-changed', () => {
            this.update();
        });
    </script>

</UserEditForm>