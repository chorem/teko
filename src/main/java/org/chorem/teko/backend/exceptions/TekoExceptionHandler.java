package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TekoExceptionHandler implements ExceptionMapper<TekoException> {

    @Override
    public Response toResponse(TekoException e) {
        return Response.status(e.getHttpStatus())
            .entity(e.getMessage())
            .build();
    }
}
