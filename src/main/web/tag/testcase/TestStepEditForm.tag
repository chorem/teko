let testStepService = require("../../service/TestStepService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<TestStepEditForm>
  <div class="modal-overlay"></div>

  <form method="post" ref="testStep" onsubmit="{edit}" class="modal">
      <div class="modal-title">{ translate('teststep.edit')}</div>

      <div class="modal-content">
        <ul class="form-list">
          <li>
            <label for="category">{ translate("teststep.category.label") }</label>
            <select name="category">
              <option value="PREREQUISITE">{ translate("teststep.category.prerequisite") }</option>
              <option value="ACTION">{ translate("teststep.category.action" ) }</option>
              <option value="RESULT">{ translate("teststep.category.result" ) }</option>
            </select>
          </li>
          <li>
            <label for="label">{translate('teststep.description.label')}</label>
            <input type="text" name="label" placeholder="{translate('teststep.description.placeholder')}" required>
          </li>
        </ul>
      </div>

      <div if="{error}" class="error-notification">{error}</div>

      <div class="modal-actions">
        <a href="" class="button button--discreet" onclick="{cancelAction}">{ translate('common.cancel')}</a>
        <div class="button button-submit-wrapper">
          <input type="submit" value="{translate('common.update')}">
        </div>
      </div>
  </form>

    <script>
        this.translate = translate;
        this.error = "";
        this.testStep = this.opts.testStep;

        testStepService.getTestStep(this.testStep.id)
        .then((testStep) => {
            FormUtils.fillForm(this.refs.testStep, testStep);
        });

        this.edit = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let testStep = FormUtils.formToMap(this.refs.testStep);
            testStepService.updateTestStep(this.testStep.id, testStep)
            .then(() => {
                eventBus.trigger('step-updated', this.testStep.id);
                this.parent.isEdit = false;
                this.parent.update();
            })
            .catch((msg) => {
                this.error = msg || translate('teststep.saveError');
                this.update();
            });
        };

        this.cancelAction = () => {
            this.parent.isEdit = false;
            this.parent.update();
        }

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</TestStepEditForm>