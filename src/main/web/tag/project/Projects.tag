let projectsService = require("../../service/ProjectService");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<Projects>
  <div class="list-pagination">
    <div class="list-items-count">
      {translate('project.x_projects', titleI18nOpts)}
    </div>

    <div class="list-filters--area">
      <button onclick="{showFilters}"
         class="button {active : projectsFiltersShow}">
        <i class="icon icon-filter"></i> {translate("common.filterAndSort")}
      </button>

      <div if="{this.projectsFiltersShow}" class="list-filters">
        <input type=""
               id="keyWord"
               onchange="{filterProjects}"
               placeholder="{ translate('project.filter') }">

        {translate("common.sort")}
        <select id="attribut" onChange="{filterProjects}">
          <option value="date" selected>Chronologique</option>
          <option value="name">Alphabétique</option>
        </select>

        <button onclick="{sortProjects}" title="Inverser l'ordre" class="link">
          <i data-icon class="{orderBool ? 'icon-arrow-down' : 'icon-arrow-down icon--animated-rotated'}"></i>
        </button>
      </div>
    </div>

    <a if="{localStorage.getItem('accessToken')}"
      href="/project/add"
      title="{ translate("project.create") }"
      class="button button--secondary">
      <i class="icon icon-plus"></i> { translate("project.create") }
    </a>
  </div>


  <div class="grid">

    <ul class="grid-items" id="maincontent">
      <li each="{project in projects}"
        class="grid-item hover-shadow hover-underline">
        <a href="/project/{project.id}" title="{project.name}" class="a--without-decoration">
          <div class="grid-item-title hover-underline--position">{project.name}</div>
          <div class="grid-item-content">
            <div class="grid-item-properties">
              <b>{project.nbTestCases | 0}</b>
              <span class="lowercase">{translate('testcase.label')}</span>
            </div>
            <div class="grid-item-properties">
              <b>{project.nbUserStories | 0}</b>
              <span class="lowercase">{translate('userstory.label')}</span>
            </div>
            <div class="grid-item-properties text--smaller">
              {translate('campaign.execution.last')} : (01/01/2017)</div>
          </div>
        </a>
      </li>
    </ul>
  </div>

  <ProjectForm if={showProjectForm}/>

    <script>
        this.translate = translate;
        this.projects = [];
        this.titleI18nOpts = { count : 0};
        this.orderBool = false;
        this.projectsFiltersShow = false;

        document.title = "Teko - " + this.translate('project.label');

        this.fetchProjects = () => {
          eventBus.trigger('loading-start');
          projectsService.getProjects("", false, "date")
          .then((projects) => {
              if(projects) {
                  this.projects = projects;
                  this.titleI18nOpts = {count: projects.length};
              }
              eventBus.trigger('loading-end');
              this.update();
          })
        };
        this.fetchProjects();

        this.showFilters = () => {
          this.projectsFiltersShow = !this.projectsFiltersShow;
        }

        this.sortProjects = () => {
            this.orderBool = !this.orderBool;
            this.filterProjects();
        }

        this.filterProjects = () => {
            let keyWord = document.getElementById("keyWord").value;
            let order = this.orderBool;
            let attribut = document.getElementById("attribut").value;
            projectsService.getProjects(keyWord, order, attribut)
                .then((projects) => {
                if (projects) {
                    this.projects = projects;
                    this.titleI18nOpts = { count : projects.length};
                } else {
                    this.projects = [];
                    this.titleI18nOpts = { count : 0};
                }
                this.update();
            });
        };

        eventBus.on('project-created', () => {
            this.fetchProjects();
        });

        eventBus.on('login', () => {
            this.fetchProjects();
        });

        eventBus.on('logout', () => {
            this.fetchProjects();
        });

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</Projects>
