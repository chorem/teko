let userService = require("../../service/UserService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<UserForm>
  <div class="sheet">
    <div class="sheet-header" if="{this.opts.signUpForm}">
      {translate('user.signup')}
    </div>
    <div class="sheet-header" if="{!this.opts.signUpForm}">
      {translate('user.create')}
    </div>

    <div class="sheet-content">
      <form method="post" ref="user" onsubmit="{create}">
        <ul class="form-content">
            <li>
                <label for="firstName">{translate('user.firstName.name')}</label>
                <input type="text" name="firstName" placeholder="{translate('user.firstName.placeholder')}" autofocus>
            </li>
            <li>
                <label for="lastName">{translate('user.lastName.name')}</label>
                <input type="text" name="lastName" placeholder="{translate('user.lastName.placeholder')}" autofocus>
            </li>
            <li>
                <label for="mail">{translate('user.mail.mail')}</label>
                <input type="email" name="mail" id="mail1" placeholder="{translate('user.mail.placeholder')}" required autofocus>
            </li>
            <li>
                <label for="password">{translate('user.password.password')}</label>
                <input type="password" name="password" id="ps1" placeholder="{translate('user.password.placeholder')}" required>
            </li>
            <li>
                <label for="password">{translate('user.password.confirm')}</label>
                <input type="password" name="password" id="ps2" placeholder="{translate('user.password.placeholder')}" required onBlur="{checkPassword}">
                <i class="icon icon-cancel" if="{password == false}"></i>
            </li>
        </ul>

        <div class="form-buttons">
          <button class="button--secondary"
                  type="button"
                  onclick="{cancelAction}">{ translate('common.cancel')}</button>
          <input type="submit" value="{translate('common.create')}" class="button--main">
        </div>

        <div if="{error}" class="notification-error">
          <i class="icon icon-bullhorn pulsate-fwd"></i>
          {error}
        </div>
    </form>

    <script>
        this.translate = translate;
        this.error = "";
        this.password = false;

        this.checkPassword = () =>{
            if (document.getElementById("ps1").value == document.getElementById("ps2").value) {
                this.password = true;
            } else {
                this.password = false;
            }
            this.update();
        }

        this.create = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let user = FormUtils.formToMap(this.refs.user);
            userService.signInUser(user)
                .then((newUserId) => {
                eventBus.trigger('user-created', newUserId);
                this.parent.showUserForm = false;
                this.error = null;
                this.parent.update();
            })
                .catch((msg) => {
                this.error = msg || 'error';
                this.update();
            });
        };

        this.cancelAction = () => {
            this.parent.showUserForm = false;
            this.parent.update();
        };

        eventBus.on('locale-changed', () => {
            this.update();
        });
    </script>
</UserForm>