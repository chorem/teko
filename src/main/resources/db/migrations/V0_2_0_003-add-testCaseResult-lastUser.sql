ALTER TABLE testCaseResult ADD COLUMN lastUserId UUID REFERENCES users(id);

UPDATE testCaseResult SET lastUserId = '3cd769d1-3d62-4708-9026-2331a2c3f608';

ALTER TABLE testCaseResult ALTER COLUMN lastUserId SET NOT NULL;