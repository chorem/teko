package org.chorem.teko.backend.service;

import com.google.common.collect.Lists;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.chorem.teko.backend.TekoTechnicalException;
import org.chorem.teko.backend.dao.CampaignExecutionDao;
import org.chorem.teko.backend.dao.ProjectDao;
import org.chorem.teko.backend.dao.TestCampaignDao;
import org.chorem.teko.backend.dao.TestCaseDao;
import org.chorem.teko.backend.dao.TestCaseResultDao;
import org.chorem.teko.backend.dao.TestStepDao;
import org.chorem.teko.backend.entity.CampaignExecution;
import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.TestCampaign;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestCaseResult;
import org.chorem.teko.backend.entity.TestStep;
import org.chorem.teko.backend.reports.ProjectTestCasesReport;
import org.chorem.teko.backend.reports.TestCampaignExecutionReport;
import org.chorem.teko.backend.reports.TestCaseExecutionReportExtract;
import org.chorem.teko.backend.reports.TestCaseReportExtract;
import org.chorem.teko.backend.reports.TestStepReportExtract;
import org.nuiton.spgeed.SqlSession;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by couteau on 25/03/17.
 */
@Path("/api/reports")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReportsService extends TekoAbstractService {

    public static final String DATETIME_FORMAT_FR ="dd/MM/yyyy HH:mm";
    public static final String DATETIME_FORMAT_EN ="yyyy/MM/dd HH:mm";

    @GET
    @Path("/ping")
    public boolean ping() {
        return true;
    }

    @GET
    @Path("/testCase/{projectId}/pdf/{locale}")
    @Produces("application/pdf")
    public Response getProjectTestCasePdfReport(@PathParam("projectId") UUID projectUUID,
                         @PathParam("locale") String locale) throws Exception {

        //Generate report data set
        ProjectTestCasesReport report = generateProjectTestCaseReport(projectUUID, locale);

        //Get report template
        JasperReport jasperReport = getReportForFile("/jasperreports/ProjectTestCasesReport.jrxml");

        //Generate report to PDF
        try {
            Map<String, Object> parameters = getTemplateTranslation(locale);
            String documentName = parameters.get("labelTestCasesReport").toString();
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Lists.newArrayList(report));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            Response.ResponseBuilder response = Response.ok((Object) JasperExportManager.exportReportToPdf(jasperPrint));
            response.header("Content-Disposition", "attachment; filename="+ report.getProjectName() +"-" + documentName + ".pdf");
            return response.build();

        } catch (JRException e) {
            throw new TekoTechnicalException(e);
        }
    }


    @GET
    @Path("/testCase/{projectId}/odt/{locale}")
    @Produces("application/odt")
    public Response getProjectTestCaseOdtReport(@PathParam("projectId") UUID projectUUID,
                                             @PathParam("locale") String locale) throws Exception {

        //Generate report data set
        ProjectTestCasesReport report = generateProjectTestCaseReport(projectUUID, locale);

        //Get report template
        JasperReport jasperReport = getReportForFile("/jasperreports/ProjectTestCasesReport.jrxml");

        //Generate report to PDF
        try {
            Map<String, Object> parameters = getTemplateTranslation(locale);
            String documentName = parameters.get("labelTestCasesReport").toString();
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Lists.newArrayList(report));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            JROdtExporter exporter = new JROdtExporter();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
            exporter.exportReport();

            Response.ResponseBuilder response = Response.ok((Object) out.toByteArray());
            response.header("Content-Disposition", "attachment; filename="+ report.getProjectName() +"-" + documentName + ".odt");
            return response.build();

        } catch (JRException e) {
            throw new TekoTechnicalException(e);
        }
    }

    @GET
    @Path("/campaign/execution/{executionId}/pdf/{locale}")
    @Produces("application/pdf")
    public Response getTestCampaignExecutionReport(@PathParam("executionId") UUID executionUUID,
                         @PathParam("locale") String locale) throws Exception {

        //Prepare data
        TestCampaignExecutionReport report = generateTestCampaignExecutionReport(executionUUID, locale);

        //Get report template
        JasperReport jasperReport = getReportForFile("/jasperreports/CampaignExecutionReport.jrxml");

        //Generate report to PDF
        try {
            Map<String, Object> parameters = getTemplateTranslation(locale);
            String documentName = parameters.get("labelCampaignExecutionReport").toString();
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Lists.newArrayList(report));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            Response.ResponseBuilder response = Response.ok((Object) JasperExportManager.exportReportToPdf(jasperPrint));
            response.header("Content-Disposition", "attachment; filename="+ report.getCampaignName() +"-" + documentName +".pdf");
            return response.build();

        } catch (JRException e) {
            throw new TekoTechnicalException(e);
        }
    }

    @GET
    @Path("/campaign/execution/{executionId}/odt/{locale}")
    @Produces("application/odt")
    public Response getTestCampaignExecutionOdtReport(@PathParam("executionId") UUID executionUUID,
                         @PathParam("locale") String locale) throws Exception {

        //Prepare data
        TestCampaignExecutionReport report = generateTestCampaignExecutionReport(executionUUID, locale);

        //Get report template
        JasperReport jasperReport = getReportForFile("/jasperreports/CampaignExecutionReport.jrxml");

        //Generate report to PDF
        try {
            Map<String, Object> parameters = getTemplateTranslation(locale);
            String documentName = parameters.get("labelCampaignExecutionReport").toString();
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(Lists.newArrayList(report));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            JROdtExporter exporter = new JROdtExporter();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
            exporter.exportReport();

            Response.ResponseBuilder response = Response.ok((Object) out.toByteArray());
            response.header("Content-Disposition", "attachment; filename="+ report.getCampaignName() +"-" + documentName +".odt");

            return response.build();

        } catch (JRException e) {
            throw new TekoTechnicalException(e);
        }
    }

    protected ProjectTestCasesReport generateProjectTestCaseReport(UUID projectUUID, String locale) throws Exception {

        //Prepare data
        ProjectTestCasesReport report = new ProjectTestCasesReport();
        SqlSession session = createSqlSession();
            ProjectDao dao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);

            Project project = dao.getProject(projectUUID);
            report.setProjectName(project.getName());

            TestCase[] testCases = testCaseDao.findAllByProjectId(projectUUID, "date", true);
            List<TestCaseReportExtract> textCasesExtracts = new ArrayList<>();
            if (null != testCases) {
                for (TestCase testCase : testCases) {
                    TestCaseReportExtract extract = new TestCaseReportExtract();
                    extract.setId(testCase.getId());
//                  extract.setStatus(testCase.getStatus());//TODO ymartel 20170704 : see how we really manage result when we'll be here !
                    extract.setName(testCase.getLabel());
                    extract.setCriticality(testCase.getCriticality().toString());

                    TestStep[] testStepsOfTestCase = testStepDao.getAllForTestCase(testCase.getId());
                    List<TestStepReportExtract> testStepList = new ArrayList<>();
                    if (null != testStepsOfTestCase) {
                        for (TestStep testStep : testStepsOfTestCase) {
                            TestStepReportExtract testStepExtract = new TestStepReportExtract();
                            testStepExtract.setId(testStep.getId());
                            testStepExtract.setLabel(testStep.getLabel());
                            testStepExtract.setCategoryName(testStep.getCategory().name());

                            testStepList.add(testStepExtract);
                        }
                    }
                    extract.setTestSteps(testStepList);
                    extract.setTestCaseSubReport(getReportForFile("/jasperreports/TestStepReportExtract.jrxml"));

                    textCasesExtracts.add(extract);
                }
            }
            report.setTestCases(textCasesExtracts);
            report.setSubReport(getReportForFile("/jasperreports/TestCaseReportExtract.jrxml"));



        report.setGenerationDate(getGenerationDateWithLocale(new Date(), locale));

        return report;
    }

    protected JasperReport getReportForFile(String templatePath){

        //compile report
        JasperReport jasperReport;
        try (InputStream inputStream = getClass().getResourceAsStream(templatePath)) {
            jasperReport = JasperCompileManager.compileReport(inputStream);
        } catch (IOException e) {
            throw new TekoTechnicalException("Could not close inputStream for " + templatePath, e);
        } catch (JRException e) {
            throw new TekoTechnicalException("Could not compile jasper report for " + templatePath, e);
        }

        return jasperReport;
    }

    protected TestCampaignExecutionReport generateTestCampaignExecutionReport(UUID executionUUID,
                                                                              String locale) throws Exception {
        TestCampaignExecutionReport report = new TestCampaignExecutionReport();

        SqlSession session = createSqlSession();
            CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);

            CampaignExecution campaignExecution = campaignExecutionDao.findById(executionUUID);
            TestCampaign testCampaign = testCampaignDao.findById(campaignExecution.getTestCampaignId());
            Project project = projectDao.getProject(testCampaign.getProjectId());

            report.setProjectName(project.getName());
            report.setCampaignName(testCampaign.getLabel());
            report.setExecutionDate(campaignExecution.getExecutionDate().format(DateTimeFormatter.ISO_DATE));

            TestCaseResult[] testCaseResults = testCaseResultDao.findAllByIds(campaignExecution.getTestCaseResultIds());
            List<TestCaseExecutionReportExtract> textCasesExtracts = new ArrayList<>();

            if (testCaseResults != null) {
                for (TestCaseResult testCaseResult : testCaseResults) {
                    TestCase testCase = testCaseResult.getTestCase();
                    TestCaseExecutionReportExtract tcExecutionReportExtract = new TestCaseExecutionReportExtract();
                    tcExecutionReportExtract.setName(testCase.getLabel());
                    tcExecutionReportExtract.setCriticality(testCase.getCriticality().toString());
                    tcExecutionReportExtract.setComment(testCaseResult.getComment());
                    tcExecutionReportExtract.setValidity(testCaseResult.getValidity());

                    TestStep[] testStepsOfTestCase = testStepDao.getAllForTestCase(testCase.getId());
                    if (testStepsOfTestCase != null) {
                        for (TestStep testStep : testStepsOfTestCase) {
                            TestStepReportExtract testStepExtract = new TestStepReportExtract();
                            testStepExtract.setId(testStep.getId());
                            testStepExtract.setLabel(testStep.getLabel());
                            testStepExtract.setCategoryName(testStep.getCategory().name());

                            tcExecutionReportExtract.addTestStepReport(testStepExtract);
                        }
                    }
                    tcExecutionReportExtract.setTestStepSubReport(getReportForFile("/jasperreports/TestStepReportExtract.jrxml"));

                    textCasesExtracts.add(tcExecutionReportExtract);
                }
            }
            report.setTestCaseResults(textCasesExtracts);
            report.setSubReport(getReportForFile("/jasperreports/TestCaseResultReportExtract.jrxml"));

        report.setGenerationDate(getGenerationDateWithLocale(new Date(), locale));
        return report;
    }

    protected String getGenerationDateWithLocale (Date date, String locale) {
        String format = DATETIME_FORMAT_EN;
        if (locale.equals("fr")) {
            format = DATETIME_FORMAT_FR;
        }

        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(format);
        return dateTimeFormat.format(date);
    }

    protected Map<String, Object> getTemplateTranslation(String locale) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("labelTestCasesReport", "Acceptance test plan");
        parameters.put("labelCampaignExecutionReport", "Campaign execution report");
        parameters.put("labelCampaignExecutionLabel", "Campaign execution of");
        parameters.put("labelCreationDate", "Produced on :\r\n");
        parameters.put("labelPage", "Page");
        parameters.put("labelCriticity", "Criticity");
        parameters.put("labelStatus", "Status");
        parameters.put("labelComment", "Comment");

        if (locale.equals("fr")) {
            parameters.put("labelTestCasesReport", "Cahier de recette");
            parameters.put("labelCampaignExecutionReport", "Rapport d'exécution de la campagne");
            parameters.put("labelCampaignExecutionLabel", "Exécution du");
            parameters.put("labelCreationDate", "Généré le :\r\n");
            parameters.put("labelPage", "Page");
            parameters.put("labelCriticity", "Criticité");
            parameters.put("labelStatus", "État");
            parameters.put("labelComment", "Commentaire");
        }
        return parameters;
    }
}
