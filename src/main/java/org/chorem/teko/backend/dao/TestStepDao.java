package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.TestStep;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

import java.util.UUID;

/**
 * Created by couteau on 21/04/17.
 */
public interface TestStepDao {

    @Update(sql = "INSERT INTO testStep (id, label, category, creationDate) "
            + "VALUES ${testStep | values("
            + "'id', 'label', 'category', 'creationDate'"
            + ")}")
    public int createTestStep(TestStep testStep);

    @Select(sql = "SELECT ts.id, ts.label, ts.category, ts.creationDate "
                + " FROM teststep ts "
                + " WHERE ts.id IN ( select unnest(tc.steps) from testcase tc where tc.id = ${testCaseId} ) "
                + " ORDER BY ts.creationDate ASC ")
    public TestStep[] getAllForTestCase(UUID testCaseId);

    @Select(sql = "SELECT ts.id, ts.label, ts.category, ts.creationDate"
                + " FROM teststep ts "
                + " WHERE ts.id=${id}")
    public TestStep getTestStep(UUID id);

    @Update(sql = "UPDATE teststep SET "
            + "label = ${testStep.label}, "
            + "category = ${testStep.category} "
            + "WHERE id = ${id}")
    public int updateTestStep(UUID id, TestStep testStep);

    @Update(sql = "DELETE FROM teststep "
                + "WHERE id = ${id}")
    public int deleteTestStep(UUID id);

    /**
     * Remove occurrences of {@link TestStep} in {@link org.chorem.teko.backend.entity.TestCase}.
     */
    @Update(sql = "UPDATE testCase "
        + " SET steps = array_remove(steps, ${testStepUuid}) "
        + " WHERE ${testStepUuid} = ANY(steps)")
    public int removeTestStepOccurrences(UUID testStepUuid);

    @Update(sql = "DELETE FROM teststep ts "
                + " WHERE NOT EXISTS ( SELECT tc FROM testCase tc WHERE ts.id = ANY (tc.steps) )")
    public int deleteOrphanTestStep();
}
