let testStepService = require("../../service/TestStepService");
let route = require("riot-route");
let translate = require('counterpart');
let eventBus = require("../../utils/TekoMagicEventBus");

<TestStep>
  <span>{testStep.label}</span>

    <script>
        this.testStep = {};
        this.testCaseId = this.opts.testCaseId;

        testStepService.getTestStep(this.opts.testStep.id)
        .then((testStep) => {
          this.testStep = testStep;
          this.update();
        });

        eventBus.on('locale-changed', () => {
          this.update();
        });

    </script>

</TestStep>