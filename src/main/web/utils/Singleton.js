let singletons = window.__singletons__ = new Map();

module.exports = (Klass) => {
    let instance = singletons.get(Klass);
    if (!instance) {
        instance = new Klass();
        singletons.set(Klass, instance);
    }
    return instance;
};
