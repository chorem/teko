let translate = require('counterpart');
let projectsService = require("../../service/ProjectService");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let jwt = require('jsonwebtoken');

<HeaderContext class="header--right">
  <div class="header-context-title">
    <div class="header-context-title--area">
      <h1 if="{!project}">{translate('project.label')}</h1>
      <h1 if="{project}">{project.name}</h1>

      <span class="header-context-title--area-right hide-on-small-devices">
        <a href="#" if="{login}">
          {translate('user.account')}
        </a>
        <span class="icon icon-user header-context-title-icon"></span>
        <a href="/login" if="{!login}">
          {translate('user.login')}
        </a>
        <a href="/" if="{login}"
           onclick="{logout}">
           {translate('user.logout')}
        </a>
      </span>
    </div>
  </div>

  <div if="{project}" class="header-context-tabs">
    <ul class="tabs">
      <li>
        <a href="/project/{project.id}"
            class="menu-link {active : windowLocation.includes('project/')}">
          <span class="show-on-menu-opened">{translate('testcase.label')}</span>
        </a>
      </li>
      <li>
        <a href="/project-campaigns/{project.id}"
            class="menu-link {active : windowLocation.includes('campaign')}">
          <span class="show-on-menu-opened">{translate('campaign.label')}</span>
        </a>
      </li>
      <li>
        <a href="/reports/{project.id}"
            class="menu-link {active : windowLocation.includes('reports')}">
          <span class="show-on-menu-opened">{translate('report.label')}</span>
        </a>
      </li>
      <li>
        <a href="/settings/{project.id}"
            class="menu-link {active : windowLocation.includes('settings')}">
          <span class="show-on-menu-opened">{translate('settings.label')}</span>
        </a>
      </li>
    </ul>
  </div>

  <script>
    this.translate = translate;
    this.login = localStorage.getItem('accessToken');
    this.user = undefined;
    this.windowLocation;

    this.fetchProject = () => {
      projectsService.getProject(this.currentProjectId)
      .then((project) => {
          this.project = project;

          if(localStorage.getItem('accessToken')){
              this.user = jwt.decode(localStorage.getItem('accessToken')).userId;
          }

          this.update();
      })
      .catch((msg) => {
          this.project = null;
          this.update();
      });
    };

    this.logout = () => {
        localStorage.removeItem('accessToken');
        this.login = undefined;
        eventBus.trigger('logout');
        this.update();
    }

    eventBus.on('project-changed', (projectId) => {
      this.windowLocation = window.location.toString();
      this.currentProjectId = projectId;
      if (null != projectId) {
        this.fetchProject();
      } else {
        this.project = null;
        this.update();
      }
    });

    eventBus.on('login', () => {
        this.login = localStorage.getItem('accessToken');
    });
  </script>
</HeaderContext>