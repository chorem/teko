package org.chorem.teko.backend.dao;

import org.apache.commons.lang3.StringUtils;
import org.chorem.teko.backend.entity.Project;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.query.Query;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by couteau on 27/01/17.
 */
public interface ProjectDao extends SpgeedDao {

    @Update(sql = "INSERT INTO project (id, name, creatorId, visibility, creationDate) "
            + " VALUES ${project | values("
            + " 'id', 'name','creatorId', 'visibility::visibility', 'creationDate'"
            + " )}")
    public int createProject(Project project);

    default Project[] getProjects(String keyWord, String attribut, boolean order, UUID userId) throws Exception{

        Map<String, Object> map = new HashMap();
        String sql = "SELECT p.id, p.name, (SELECT count(*) from testcase tc where tc.projectId = p.id) nbTestCases, "
                   + "p.creatorId, p.authorizedUsersId, p.visibility, p.creationDate "
                   + "FROM project p WHERE (visibility = 'PUBLIC' ";

        if(null != userId) {
            map.put("userId", userId);
            sql += " OR ${userId} = ANY (authorizedUsersId) OR creatorId = ${userId}";
        }

        if(StringUtils.isNotBlank(keyWord)){
            map.put("keyWord", keyWord);
            sql += " ) AND (UPPER(name) LIKE UPPER(${keyWord}) ";
        }

        sql+= ") ORDER BY p.";

        if("name".equals(attribut)){
            sql += "name ";
        }else{
            sql += "creationDate ";
        }
        if(order){
            sql += "ASC";
        }else{
            sql += "DESC";
        }
        Query query = new Query(getSession(), sql, map, Project[].class);
        return (Project[]) query.executeQuery();
    }

    @Update(sql = "UPDATE project "
            + " SET authorizedUsersId = array_append(authorizedUsersId, ${userId} )"
            + " WHERE id = ${projectId}")
    public int addAuthorizedUser(UUID projectId, UUID userId);

    @Update(sql = "UPDATE project "
            + " SET authorizedUsersId = array_remove(authorizedUsersId, ${userId} )"
            + " WHERE id = ${projectId}")
    public int removeAuthorizedUser(UUID projectId, UUID userId);

    @Update(sql = "UPDATE project "
            + " SET authorizedUsersId = array_remove(authorizedUsersId, ${userId} )"
            + " WHERE ${userId} = ANY (authorizedUsersId)")
    public int removeAuthorizedUserFromAllProjects(UUID userId);

    @Select(sql = "SELECT DISTINCT unnest(tags) AS tags FROM testcase WHERE projectId = ${projectId}")
    public Object[] getProjectTags(UUID projectId);

    @Select(sql = "SELECT id, name, creatorId, authorizedUsersId, visibility, creationDate FROM project "
            + " WHERE id=${projectId}")
    public Project getProject(UUID projectId);

    @Update(sql = "UPDATE project SET "
            + " name = ${project.name},"
            + " visibility = ${project.visibility}::visibility"
            + " WHERE id = ${id}")
    public int updateProject(UUID id, Project project);

    @Update(sql = "DELETE FROM project "
            + " WHERE id=${id}")
    public int deleteProject(UUID id);

    @Select(sql = "SELECT EXISTS (SELECT 1 FROM testcase "
        + " WHERE projectid = ${projectId} )")
    public boolean isProjectReferencedByTestCase(UUID projectId);

    @Select(sql = "SELECT EXISTS (SELECT 1 FROM project "
        + " WHERE id = ${projectId} "
        + " AND ( visibility = 'PUBLIC' "
        + " OR creatorId = ${userId} "
        + " OR ${userId} = ANY (authorizedUsersId) )"
        + " )")
    public boolean isUserAuthorizedOnProject(UUID userId, UUID projectId);
}
