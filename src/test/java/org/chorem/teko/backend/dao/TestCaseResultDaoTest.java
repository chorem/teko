package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.TestCaseResult;
import org.chorem.teko.backend.entity.Validity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestCaseResultDaoTest extends AbstractDaoTest {

    @Test
    public void testCreate() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create a testase result
            TestCaseResult campaign = new TestCaseResult();
            UUID testCaseResult01Id = UUID.randomUUID();
            campaign.setId(testCaseResult01Id);
            campaign.setTestCaseId(TEST_CASE_ONE_UUID);
            campaign.setValidity(Validity.OK);
            campaign.setComment("All right");
            campaign.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));

            TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
            int createResult = testCaseResultDao.create(campaign);
            Assertions.assertEquals(1, createResult);

        }
    }

    @Test
    public void testGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create some testase result
            TestCaseResult testCaseResult01 = new TestCaseResult();
            UUID testCaseResult01Id = UUID.randomUUID();
            testCaseResult01.setId(testCaseResult01Id);
            testCaseResult01.setTestCaseId(TEST_CASE_ONE_UUID);
            testCaseResult01.setValidity(Validity.OK);
            testCaseResult01.setComment("All right");
            testCaseResult01.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));

            TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
            testCaseResultDao.create(testCaseResult01);

            TestCaseResult testCaseResult02 = new TestCaseResult();
            UUID testCaseResult02Id = UUID.randomUUID();
            testCaseResult02.setId(testCaseResult02Id);
            testCaseResult02.setTestCaseId(TEST_CASE_TWO_UUID);
            testCaseResult02.setValidity(Validity.OK);
            testCaseResult02.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));

            testCaseResultDao.create(testCaseResult02);

            {
                TestCaseResult testCaseResult = testCaseResultDao.findById(testCaseResult01Id);
                Assertions.assertNotNull(testCaseResult);
                Assertions.assertEquals(testCaseResult01Id, testCaseResult.getId());
                Assertions.assertNotNull(testCaseResult.getTestCase());
                Assertions.assertEquals(TEST_CASE_ONE_UUID, testCaseResult.getTestCase().getId());
                Assertions.assertEquals("All right", testCaseResult.getComment());
                Assertions.assertEquals(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"), testCaseResult.getLastUserId());
            }

            {
                TestCaseResult[] all = testCaseResultDao.findAll();
                Assertions.assertEquals(2, all.length);
            }

            {
                TestCaseResult[] all = testCaseResultDao.findAllByIds(new UUID[]{testCaseResult01Id});
                Assertions.assertEquals(1, all.length);
                Assertions.assertEquals(testCaseResult01Id, all[0].getId());
                all = testCaseResultDao.findAllByIds(new UUID[]{testCaseResult01Id, testCaseResult02Id});
                Assertions.assertEquals(2, all.length);
                Assertions.assertNotEquals(all[0].getId(), all[1].getId());
            }

            TestCaseResult testCaseResult03 = new TestCaseResult();
            UUID testCaseResult03Id = UUID.randomUUID();
            testCaseResult03.setId(testCaseResult03Id);
            testCaseResult03.setTestCaseId(TEST_CASE_ONE_UUID);
            testCaseResult03.setValidity(Validity.KO);
            testCaseResult03.setComment("Something missing, the question's what ?");
            testCaseResult03.setLastUserId(UUID.fromString("3cd769d1-3d62-4708-9026-2331a2c3f608"));

            testCaseResultDao.create(testCaseResult03);

            {
                TestCaseResult[] all = testCaseResultDao.findAll();
                Assertions.assertEquals(3, all.length);
            }

        }
    }
}
