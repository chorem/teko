package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * fc4918 - 11.2. 422 Unprocessable Entity
 * The 422 (Unprocessable Entity) status code means the server
 * understands the content type of the request entity
 * (hence a 415 (Unsupported Media Type) status code is inappropriate),
 * and the syntax of the request entity is correct (thus a 400 (Bad Request)
 * status code is inappropriate) but was unable to process the contained instructions.
 * For example, this error condition may occur if an XML request body contains well-formed (i.e., syntactically correct), but semantically erroneous, XML instructions.
 *
 * @author ymartel(martel@codelutin.com)
 */
public class LoginExceptionHandler implements ExceptionMapper<LoginException> {

    @Override
    public Response toResponse(LoginException e) {
        return Response.status(422) //XXX : waiting for a Response.Status.UNPROCESSABLE_ENTITY
                .entity(e)
                .build();
    }
}