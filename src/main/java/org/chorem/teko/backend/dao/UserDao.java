package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.User;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.query.Query;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by lebras on 19/06/18.
 */
public interface UserDao extends SpgeedDao {

    @Update(sql = " INSERT INTO users (id, firstName, lastName, mail, password, creationDate) "
                + " VALUES ${user | values("
                + " 'id', 'firstName', 'lastName', 'mail', 'password', 'creationDate'"
                + " )}")
    public int createUser(User user);

    default User[] getAll(String attribut, boolean order) throws Exception{

        Map map = new HashMap();
        String sql =" SELECT id, firstName, lastName, mail, creationDate FROM users ORDER BY ";

        if("mail".equals(attribut) || "creationDate".equals(attribut)) {
            sql += attribut;
        }
        if(order){
            sql += " ASC";
        }else{
            sql += " DESC";
        }
        Query query = new Query(getSession(), sql, map, User[].class);
        return (User[]) query.executeQuery();
    }

    @Select(sql = " SELECT id, firstName, lastName, password, mail"
                + " FROM Users"
                + " WHERE mail=${mail}")
    public User getUserPassword(String mail);

    @Select(sql = " SELECT id, firstName, lastName, mail, creationDate"
                + " FROM Users"
                + " WHERE id=${userId}")
    public User getUser(UUID userId);

    @Select(sql = " SELECT id, firstName, lastName, mail, password"
                + " FROM Users"
                + " WHERE id=${userId}")
    public User getFullUserById(UUID userId);

    @Select(sql = " SELECT id, password"
                + " FROM Users"
                + " WHERE mail = ${userMail}")
    public User login(String userMail);

    @Update(sql = " UPDATE users SET "
                + " firstName = ${firstName},"
                + " lastName = ${lastName},"
                + " password = ${password} "
                + " WHERE id = ${userId}")
    public int updateUser(UUID userId, String firstName, String lastName, String password);

    @Update(sql = " DELETE FROM users "
                + " WHERE id=${userId}")
    public int deleteUser(UUID userId);
}
