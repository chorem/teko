CREATE TYPE visibility AS ENUM ('PUBLIC', 'PRIVATE');

ALTER TABLE project ADD COLUMN visibility        visibility  default 'PUBLIC';
ALTER TABLE project ADD COLUMN creatorId         UUID        REFERENCES users(id);
ALTER TABLE project ADD COLUMN authorizedUsersId UUID[];

UPDATE project SET visibility = 'PUBLIC';
UPDATE project SET creatorId = '3cd769d1-3d62-4708-9026-2331a2c3f608';

ALTER TABLE project ALTER COLUMN creatorId SET NOT NULL;