package org.chorem.teko.backend.reports;

import net.sf.jasperreports.engine.JasperReport;

import java.util.List;
import java.util.UUID;

/**
 * Created by couteau on 29/03/17.
 */
public class TestCaseReportExtract {

    protected UUID id;
    protected String name;
    protected String criticality;
    protected List<TestStepReportExtract> testSteps;
    protected JasperReport testCaseSubReport;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public List<TestStepReportExtract> getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(List<TestStepReportExtract> testSteps) {
        this.testSteps = testSteps;
    }

    public JasperReport getTestCaseSubReport() {
        return testCaseSubReport;
    }

    public void setTestCaseSubReport(JasperReport testCaseSubReport) {
        this.testCaseSubReport = testCaseSubReport;
    }
}
