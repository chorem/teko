package org.chorem.teko.backend.service;

import org.chorem.teko.backend.dao.TestCaseDao;
import org.chorem.teko.backend.dao.TestStepDao;
import org.chorem.teko.backend.entity.TestStep;
import org.nuiton.spgeed.SqlSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestStepService extends TekoAbstractService {

    @GET
    @Path("/testStep/ping")
    public boolean ping() {
        return true;
    }

    @POST
    @Path("/testCase/{testCaseId}/testStep")
    public UUID create(@PathParam("testCaseId") UUID testCaseId,
                       TestStep testStep) throws Exception {

        SqlSession session = createSqlSession();
        TestStepDao dao = session.getDao(TestStepDao.class);
        UUID stepUuid = UUID.randomUUID();
        testStep.setId(stepUuid);
        int createResult = dao.createTestStep(testStep);
        if (createResult == 1) {
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            testCaseDao.addStep(testCaseId, stepUuid);
        }

        return testStep.getId();
    }

    @GET
    @Path("/testCase/{testCaseId}/testStep")
    public TestStep[] getAllForProject(@PathParam("testCaseId") UUID testCaseId) throws Exception {
        SqlSession session = createSqlSession();
        TestStepDao dao = session.getDao(TestStepDao.class);

        return dao.getAllForTestCase(testCaseId);
    }


    @GET
    @Path("/testStep/{id}")
    public TestStep getTestStep(@PathParam("id") UUID id) throws Exception {
        SqlSession session = createSqlSession();
        TestStepDao dao = session.getDao(TestStepDao.class);

        return dao.getTestStep(id);
    }

    @POST
    @Path("/testStep/{id}")
    public void updateTestStep(@PathParam("id") UUID id,
                               TestStep testStep) throws Exception {
        SqlSession session = createSqlSession();
        TestStepDao dao = session.getDao(TestStepDao.class);
        dao.updateTestStep(id, testStep);
    }

    @DELETE
    @Path("testCase/{testCaseId}/testStep/{stepId}")
    public void deleteTestStep(@PathParam("testCaseId") UUID testCaseId,
                               @PathParam("stepId") UUID stepId,
                               TestStep testStep) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
        testCaseDao.removeStep(testCaseId, stepId);

        // If there is no other reference, simply delete this new orphan step !
        boolean testStepReferenced = testCaseDao.isTestStepReferenced(stepId);
        if (!testStepReferenced) {
            TestStepDao testStepDao = session.getDao(TestStepDao.class);
            testStepDao.deleteTestStep(stepId);
        }
    }
}
