package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.TestCampaign;
import org.chorem.teko.backend.entity.TestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.nuiton.spgeed.SqlSession;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestCampaignDaoTest extends AbstractDaoTest {

    @Test
    public void testCreate() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create campaign
            TestCampaign campaign = new TestCampaign();
            UUID campaign01Id = UUID.randomUUID();
            campaign.setId(campaign01Id);
            String campaignName = "Version 0.1-alpha";
            campaign.setLabel(campaignName);
            campaign.setProjectId(PROJECT_ONE_UUID);
            campaign.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID});
            campaign.setStatus(Status.AVAILABLE);

            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            int createResult = testCampaignDao.create(campaign);
            Assertions.assertEquals(1, createResult);
        }
    }

    @Test
    public void testGet() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create other test cases
            TestCase testCase02 = new TestCase();
            testCase02.setLabel("Test Case 02");
            UUID testCase02Id = UUID.randomUUID();
            testCase02.setId(testCase02Id);
            testCase02.setRef("1");
            testCase02.setStatus(Status.AVAILABLE);
            testCase02.setProjectId(PROJECT_ONE_UUID);
            TestCase testCase03 = new TestCase();
            testCase03.setLabel("Test Case 03");
            UUID testCase03Id = UUID.randomUUID();
            testCase03.setId(testCase03Id);
            testCase03.setRef("1");
            testCase03.setStatus(Status.AVAILABLE);
            testCase03.setProjectId(PROJECT_ONE_UUID);

            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            testCaseDao.createTestCase(testCase02);
            testCaseDao.createTestCase(testCase03);

            //create campaigns
            TestCampaign campaign01 = new TestCampaign();
            UUID campaign01Id = UUID.randomUUID();
            campaign01.setId(campaign01Id);
            String campaign01Name = "Version 0.1-alpha";
            campaign01.setLabel(campaign01Name);
            campaign01.setProjectId(PROJECT_ONE_UUID);
            campaign01.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, testCase02Id});
            campaign01.setStatus(Status.AVAILABLE);

            TestCampaign campaign02 = new TestCampaign();
            UUID campaign02Id = UUID.randomUUID();
            campaign02.setId(campaign02Id);
            String campaign02Name = "Version 0.1";
            campaign02.setLabel(campaign02Name);
            campaign02.setProjectId(PROJECT_ONE_UUID);
            campaign02.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, testCase02Id, testCase03Id});
            campaign02.setStatus(Status.DRAFT);

            TestCampaign campaign03 = new TestCampaign();
            UUID campaign03Id = UUID.randomUUID();
            campaign03.setId(campaign03Id);
            String campaign03Name = "Version 0.2-alpha";
            campaign03.setLabel(campaign03Name);
            campaign03.setProjectId(PROJECT_ONE_UUID);
            campaign03.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, testCase02Id});
            campaign03.setStatus(Status.AVAILABLE);

            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            testCampaignDao.create(campaign01);
            testCampaignDao.create(campaign02);
            testCampaignDao.create(campaign03);

            {
                TestCampaign testCampaign = testCampaignDao.findById(campaign01Id);
                Assertions.assertNotNull(testCampaign);
                Assertions.assertEquals(campaign01Id, testCampaign.getId());
                Assertions.assertEquals(PROJECT_ONE_UUID, testCampaign.getProjectId());
                Assertions.assertEquals(campaign01Name, testCampaign.getLabel());
                Assertions.assertEquals(2, testCampaign.getTestCaseIds().length);
                Assertions.assertEquals(Status.AVAILABLE, testCampaign.getStatus());
            }

            {
                TestCampaign testCampaign = testCampaignDao.findById(campaign02Id);
                Assertions.assertNotNull(testCampaign);
                Assertions.assertEquals(campaign02Id, testCampaign.getId());
                Assertions.assertEquals(PROJECT_ONE_UUID, testCampaign.getProjectId());
                Assertions.assertEquals(campaign02Name, testCampaign.getLabel());
                Assertions.assertEquals(3, testCampaign.getTestCaseIds().length);
                Assertions.assertEquals(Status.DRAFT, testCampaign.getStatus());
            }

            {
                TestCampaign[] all = testCampaignDao.findWithFilter(PROJECT_ONE_UUID, "AVAILABLE",null, true, "date");
                Assertions.assertEquals(2, all.length);
            }

            {
                TestCampaign[] all = testCampaignDao.findAllByStatus(Status.AVAILABLE);
                Assertions.assertEquals(2, all.length);
                Assertions.assertEquals(campaign01Id, all[0].getId());
            }

            {
                TestCampaign[] all = testCampaignDao.findAllByStatus(Status.OBSOLETE);
                Assertions.assertNull(all);
            }

            {
                TestCampaign[] all = testCampaignDao.findWithFilter(PROJECT_ONE_UUID,null, "%1%", true, "date");
                Assertions.assertEquals(2, all.length);
                Assertions.assertEquals(campaign01Id, all[0].getId());
                all = testCampaignDao.findWithFilter(PROJECT_ONE_UUID,"AVAILABLE", null, true, "date");
                Assertions.assertEquals(2, all.length);
                Assertions.assertEquals(campaign01Id, all[0].getId());
            }

            {
                TestCampaign[] all = testCampaignDao.findWithFilter (PROJECT_ONE_UUID,"AVAILABLE", "%1%", true, "date");
                Assertions.assertEquals(1, all.length);
                Assertions.assertEquals(campaign01Id, all[0].getId());
            }

        }
    }

    @Test
    public void testUpdate() throws Exception {

        try (SqlSession session = createSqlSession()) {
            fillDatabase(session);

            //create campaign
            TestCampaign campaign = new TestCampaign();
            UUID campaign01Id = UUID.randomUUID();
            campaign.setId(campaign01Id);
            String campaignName = "Version 0.1-alpha";
            campaign.setLabel(campaignName);
            campaign.setProjectId(PROJECT_ONE_UUID);
            campaign.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID});
            campaign.setStatus(Status.DRAFT);

            TestCampaignDao testCampaignDao = session.getDao(TestCampaignDao.class);
            testCampaignDao.create(campaign);

            // update status
            {
                testCampaignDao.updateStatus(campaign01Id, Status.AVAILABLE);
                TestCampaign testCampaign = testCampaignDao.findById(campaign01Id);
                Assertions.assertEquals(Status.AVAILABLE, testCampaign.getStatus());
            }

            // update object
            {

                TestCase testCase02 = new TestCase();
                testCase02.setLabel("Test Case 02");
                UUID testCase02Id = UUID.randomUUID();
                testCase02.setId(testCase02Id);
                testCase02.setRef("1");
                testCase02.setStatus(Status.AVAILABLE);
                testCase02.setProjectId(PROJECT_ONE_UUID);

                TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
                testCaseDao.createTestCase(testCase02);

                String newLabel = "Version 0.1-Beta";
                campaign.setLabel(newLabel);
                campaign.setStatus(Status.CLOSED);
                campaign.setTestCaseIds(new UUID[]{TEST_CASE_ONE_UUID, testCase02Id});
                testCampaignDao.update(campaign);
                TestCampaign testCampaign = testCampaignDao.findById(campaign01Id);
                Assertions.assertEquals(newLabel, testCampaign.getLabel());
                Assertions.assertEquals(Status.CLOSED, testCampaign.getStatus());
                Assertions.assertEquals(2, testCampaign.getTestCaseIds().length);

            }
        }
    }
}
