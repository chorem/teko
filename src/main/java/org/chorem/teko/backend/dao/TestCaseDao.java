package org.chorem.teko.backend.dao;

import org.apache.commons.lang3.StringUtils;
import org.chorem.teko.backend.entity.TestCase;
import org.nuiton.spgeed.SpgeedDao;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;
import org.nuiton.spgeed.query.Query;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by couteau on 05/02/17.
 */
public interface TestCaseDao extends SpgeedDao {

    @Update(sql = "INSERT INTO testCase (id, projectId, label, ref, status, criticality, creationDate) "
            + "VALUES ${testCase | values("
            + "'id', 'projectId', 'label', 'ref', 'status', 'criticality', 'creationDate'"
            + ")}")
    public int createTestCase(TestCase testCase);

    default TestCase[] findAllByProjectId(UUID projectId, String attribut, boolean order) throws Exception{

        Map<String, Object> map = new HashMap();
        map.put("projectId", projectId);

        String sql = " SELECT tc.id, tc.label, tc.ref, tc.status, tc.projectId, tc.criticality, tc.creationDate, tc.tags,"
                   + " ( "
                   + "  SELECT json_agg(stepsFrom.*) "
                   + "  FROM ( "
                   + "    SELECT ts.* FROM teststep ts "
                   + "    JOIN unnest (tc.steps) WITH ORDINALITY o(id, ord) USING (id) "
                   + "    ORDER BY o.ord "
                   + "  ) as stepsFrom "
                   + " ) as steps, "
                   + " CASE WHEN EXISTS("
                   + "     SELECT * FROM testCampaign WHERE tc.id = ANY(testCaseIds)"
                   + " ) OR EXISTS("
                   + "     SELECT * FROM testCase WHERE tc.id = ANY (conditions)"
                   + " ) OR EXISTS ("
                   + " SELECT * FROM testCaseResult WHERE tc.id = testCaseId "
                   + " ) THEN 'false' ELSE 'true' END"
                   + " as alterable "
                   + " FROM testcase tc "
                   + " WHERE tc.projectId = ${projectId} "
                   + " ORDER BY tc.";

        if("label".equals(attribut)){
            sql += "label ";
        }else{
            sql += "creationDate ";
        }
        if(order){
            sql += "ASC";
        }else{
            sql += "DESC";
        }

        Query query = new Query(getSession(), sql, map, TestCase[].class);
        return (TestCase[]) query.executeQuery();
    }

    @Select(sql = "SELECT tc.id, tc.label, tc.ref, tc.status, tc.projectId, tc.criticality, tc.creationDate, "
                + "("
                + "  SELECT json_agg(stepsFrom.*) "
                + "  FROM ( "
                + "    SELECT ts.* FROM teststep ts "
                + "    JOIN unnest (tc.steps) WITH ORDINALITY o(id, ord) USING (id) "
                + "    ORDER BY o.ord "
                + "  ) as stepsFrom "
                + ") as steps, "
                + " CASE WHEN EXISTS("
                + "     SELECT * FROM testCampaign WHERE tc.id = ANY(testCaseIds)"
                + " ) OR EXISTS("
                + "     SELECT * FROM testCase WHERE tc.id = ANY (conditions)"
                + " ) OR EXISTS ("
                + " SELECT * FROM testCaseResult WHERE tc.id = testCaseId "
                + " ) THEN 'false' ELSE 'true' END"
                + " as alterable"
                + " FROM testcase tc "
                + " WHERE tc.id=${id}")
    public TestCase findById(UUID id);

    @Select(sql = "SELECT tc.id, tc.label, tc.ref, tc.status, tc.projectId, tc.criticality, tc.creationDate, tc.conditions, "
                + "("
                + "  SELECT json_agg(stepsFrom.*) "
                + "  FROM ( "
                + "    SELECT ts.* FROM teststep ts "
                + "    JOIN unnest (tc.steps) WITH ORDINALITY o(id, ord) USING (id) "
                + "    ORDER BY o.ord "
                + "  ) as stepsFrom "
                + " ) as steps, "
                + " CASE WHEN EXISTS("
                + "     SELECT * FROM testCampaign WHERE tc.id = ANY(testCaseIds)"
                + " ) OR EXISTS("
                + "     SELECT * FROM testCase WHERE tc.id = ANY (conditions)"
                + " ) OR EXISTS ("
                + " SELECT * FROM testCaseResult WHERE tc.id = testCaseId "
                + " ) THEN 'false' ELSE 'true' END"
                + " as alterable"
                + " FROM testcase tc "
                + " WHERE tc.id = ANY(${ids})"
                + " ORDER BY tc.creationDate ASC ")
    public TestCase[] findAllByIds(UUID[] ids);

    default TestCase[] findAllWithFilter(UUID projectUUID, String keyWord, String  criticality, String status, boolean order, String attribut) throws Exception{
        Map<String, Object> map = new HashMap();
        map.put("projectUUID", projectUUID);
        String sql ="SELECT tc.id, tc.label, tc.ref, tc.status, tc.projectId, tc.criticality, tc.creationDate, "
                + " CASE WHEN EXISTS("
                + "     SELECT * FROM testCampaign WHERE tc.id = ANY(testCaseIds)"
                + " ) OR EXISTS("
                + "     SELECT * FROM testCase WHERE tc.id = ANY (conditions)"
                + " ) OR EXISTS ("
                + " SELECT * FROM testCaseResult WHERE tc.id = testCaseId "
                + " ) THEN 'false' ELSE 'true' END"
                + " as alterable "
                + " FROM testcase tc"
                + " WHERE tc.projectId = ${projectUUID} ";
        if(StringUtils.isNotBlank(keyWord)){
            map.put("keyWord", keyWord);
            sql += "AND UPPER(tc.label) LIKE UPPER(${keyWord}) ";
        }
        if(StringUtils.isNotBlank(criticality)){
            map.put("criticality", criticality);
            sql += "AND tc.criticality = ${criticality} ";
        }
        if(StringUtils.isNotBlank(status)){
            map.put("status", status);
            sql += "AND tc.status = ${status} ";
        }

        sql += "ORDER BY ";
        if("label".equals(attribut)){
            sql += "tc.label ";
        }else{
            sql += "tc.creationDate ";
        }
        if(order){
            sql += "ASC";
        }else{
            sql += "DESC";
        }

        Query query = new Query(getSession(), sql, map, TestCase[].class);
        return (TestCase []) query.executeQuery();
    }

    @Update(sql = "UPDATE testcase SET "
            + "label = ${testCase.label}, "
            + "ref = ${testCase.ref}, "
            + "status = ${testCase.status}, "
            + "criticality = ${testCase.criticality} "
            + "WHERE id = ${id}")
    public int updateTestCase(UUID id, TestCase testCase);

    @Update(sql = "UPDATE testcase "
        + " SET steps = array_append(steps, ${stepUuid} )"
        + " WHERE id = ${testCaseId}")
    public int addStep(UUID testCaseId, UUID stepUuid);

    @Update(sql = "UPDATE testcase "
            + " SET tags = array_append(tags, ${tag}::text)"
            + " WHERE id = ${testCaseId}"
            + " AND ((tags @> ARRAY[${tag}::text]) = false OR tags IS NULL)" )
    public int addTag(UUID testCaseId, String tag);

    @Update(sql = "UPDATE testcase "
            + " SET steps = array_remove(steps, ${stepUuid}) "
            + " WHERE id = ${testCaseId}")
    public int removeStep(UUID testCaseId, UUID stepUuid);

    @Update(sql = "UPDATE testCase "
            + " SET tags = array_remove(tags, ${tag}::text) "
            + " WHERE id = ${testCaseId}")
    public int removeTag(UUID testCaseId, String tag);

    @Select(sql = "SELECT tags FROM testcase WHERE id = ${testCaseId}")
    public TestCase getTestCaseTags (UUID testCaseId);

    @Select(sql = " SELECT id, label, ref, status, projectId, criticality, creationDate "
                + " FROM testcase "
                + " WHERE ${tag}::text = ANY(tags) "
                + " AND projectId = ${projectUUID} "
                + " ORDER BY creationDate")
    public TestCase[] getTestCaseByTag(String tag, UUID projectUUID);

    @Select(sql = "SELECT tc.id, tc.label, tc.ref, tc.status, tc.projectId, tc.criticality, tc.creationDate, "
            + "("
            + "  SELECT json_agg(stepsFrom.*) "
            + "  FROM ( "
            + "    SELECT ts.* FROM teststep ts "
            + "    JOIN unnest (tc.steps) WITH ORDINALITY o(id, ord) USING (id) "
            + "    ORDER BY o.ord "
            + "  ) as stepsFrom "
            + ") as steps "
            + " FROM testcase tc "
            + " WHERE ${testStepUuid} = ANY(tc.steps)"
            + " ORDER BY tc.creationDate ASC ")
    public TestCase[] getAllForTestStep(UUID testStepUuid);

    @Select(sql = "SELECT EXISTS (SELECT 1 FROM testcase "
        + " WHERE ${stepUuid} = ANY(steps) )")
    public boolean isTestStepReferenced(UUID stepUuid);

    @Update(sql = "UPDATE testCase "
            + " SET conditions = array_append(conditions, ${conditionTestId} )"
            + " WHERE id = ${testCaseId}"
            + " AND ${testCaseId} != ${conditionTestId}")
    public int addCondition(UUID testCaseId, UUID conditionTestId);

    @Update(sql = "UPDATE testCase "
            + " SET conditions = array_remove(conditions, ${conditionTestId} )"
            + " WHERE id = ${testCaseId}")
    public int removeCondition(UUID testCaseId, UUID conditionTestId);

    @Select(sql = " SELECT id, label FROM testCase WHERE id IN "
                + " (SELECT unnest(conditions) FROM testCase WHERE id = ${testCaseId})")
    public TestCase[] getConditions (UUID testCaseId);

    @Select(sql = " SELECT id, projectId, label, ref, status, criticality, creationDate "
                + " FROM testCase "
                + " WHERE ${testCaseId} = ANY (conditions)")
    public TestCase[] getDependOf (UUID testCaseId);

    @Update(sql = " DELETE FROM testcase "
                + " WHERE id = ${id}")
    public int deleteTestCase(UUID id);

}
