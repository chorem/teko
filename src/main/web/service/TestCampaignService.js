let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class TestCampaignService extends FetchService {

    getCampaigns(status) {
        return this.get("/testCampaign" + (status ? "?status="+status : ""));
    }

    getProjectCampaigns(projectId, status, keyWord, order, attribut) {
        return this.get("/project/"+projectId+"/testCampaign" + "?" + (status ? "status="+status : "")
                                                              + "&"
                                                              + ( keyWord ? "keyWord=" + keyWord : "")
                                                              + "&"
                                                              + ( order ? "order=" + order : "")+ "&" + ( attribut ? "attribut=" + attribut : ""));

    }

    createCampaign(projectId, campaign) {
        return this.post("/project/"+projectId+"/testCampaign", campaign);
    }

    getCampaign(id) {
        return this.get("/testCampaign/"+ id);
    }

    deleteCampaign(id) {
        return this.delete("/testCampaign/"+ id);
    }

    updateCampaign(id, campaign) {
        return this.post("/testCampaign/"+ id, campaign);
    }

    updateCampaignStatus(id, status) {
        return this.post("/testCampaign/"+ id + "/status", status);
    }

    getCampaignTestCases(id) {
        return this.get("/testCampaign/"+ id + "/testCases");
    }

    executeCampaign(campaignExecution) {
        return this.post("/testCampaign/" + campaignExecution.testCampaignId + "/executions", campaignExecution);
    }

    getCampaignExecutions(campaignId) {
        return this.get("/testCampaign/" + campaignId + "/executions");
    }

    getCampaignExecutionTestResults(campaignExecution) {
        let resultIds = campaignExecution.testCaseResultIds.join("&testCaseResultId=");
        return this.get("/testCampaign/" + campaignExecution.testCampaignId + "/executions/" + campaignExecution.id + "/results?testCaseResultId=" + resultIds);
    }
}

module.exports = singleton(TestCampaignService);