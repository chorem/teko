let route = require("riot-route");
let translate = require('counterpart');
let eventBus = require("../../utils/TekoMagicEventBus");

<Menu>
  <a href="#" onclick="{showHideMenu}" class="header-link--as-title">
    <span>Menu</span>
  </a>
  <div class="menu-display {opened: showMenu, closed: !showMenu}" show="{showMenu}">

    <div class="menu-content">
      <button class="menu-button sticky--top-right" onclick="{showHideMenu}">
        <span class="icon-cancel"></span>
      </button>

      <button onclick="{goTo('/')}" class="menu-button">
        <span class="icon icon-home"></span>
        {translate('common.home')}
      </button>
      <button if="{login}"
              onclick="{goTo('/users')}"
              class="menu-button">
        <span class="icon icon-users"></span>
        {translate('user.users')}
      </button>

      <button if="{login}" class="menu-button">
        <span class="icon icon-user"></span>
        <span>{translate('user.account')}</span>
      </button>
      <button if="{!login}" onclick="{goTo('/login')}" class="menu-button">
        <span class="icon icon-log-in"></span>
        <span>{translate('user.login')}</span>
      </button>
      <button if="{login}" onclick="{logout}" class="menu-button">
        <span class="icon icon-log-out"></span>
        <span>{translate('user.logout')}</span>
      </button>
    </div>
    <!--a href="#"
        class="menu-link"
        if="{this.opts.currentProjectId}">
      <span class="icon icon-user-story"></span>
      <span class="show-on-menu-opened">{translate('userstory.label')}</span>
    </a-->

    <!--span
        class="dropdown">
      <span class="icon icon-settings"></span>
      <span class="show-on-menu-opened">{translate('common.language.label')}</span>
      <ul class="dropdown-content">
        <li>
          <a href=""
            onclick="{changeLocale('en')}"
            class="{active : locale == 'en'}">
            <img alt="Logo Teko" src="/../img/flag-en.png" class="flag-icon"/> {translate('common.language.en')}
          </a>
        </li>
        <li>
          <a href=""
            onclick="{changeLocale('fr')}"
            class="{active : locale == 'fr'}">
            <img alt="Logo Teko" src="/../img/flag-fr.png" class="flag-icon"/> {translate('common.language.fr')}
          </a>
        </li>
      </ul>
      {this.currentProjectId}
    </span-->
  </div>

    <script>
      this.translate = translate;
      this.showMenu = false;
      this.currentProjectId = undefined;
      this.locale = translate.getLocale();
      this.login = localStorage.getItem('accessToken');

      this.on('update', function(e) {
        // this hack to get the opts.currentProjectId, and not been troubled with this.update()
        if (this.opts.currentProjectId) {
          this.currentProjectId = this.opts.currentProjectId;
        }
      });

      this.changeLocale = (newLocale) => {
        return (e) => {
          translate.setLocale(newLocale);
          this.locale = newLocale;
          eventBus.trigger('locale-changed');
        };
      };

      this.showHideMenu = () => {
        this.showMenu = !this.showMenu;
      }

      this.goTo = (url) => {
        return (e) => {
          this.showMenu = false;
          route(url);
        }
      }

      this.logout = () => {
          localStorage.removeItem('accessToken');
          this.login = undefined;
          eventBus.trigger('logout');
          this.update();
      }

      eventBus.on('user-deleted', () => {
          route('/');
          this.logout();
      })

      eventBus.on('project-loaded', (project) => {
        this.currentProjectId = project.id;
        this.update();
      });

      eventBus.on('login', () => {
          this.login = localStorage.getItem('accessToken');
      });

    </script>
</Menu>