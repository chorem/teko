let projectsService = require("../service/ProjectService");
let testCasesService = require("../service/TestCaseService");
let reportsService = require("../service/ReportsService");
let eventBus = require("../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require("counterpart");

<Reports>
  <div class="sheet">
    <div class="sheet-header">
      { translate("report.label") }
    </div>

    <div if="{error}" class="notification-error" if={!project}>
      {error || translate("project.notFound")}
    </div>

    <div class="sheet-content" if={project}>
      <ul class="list-items list-items--card" id="maincontent">
        <li>
          <span>{ translate("report.acceptanceTestPlan") } ({ translate("x_testcases", testCasesI18nOpts) })</span>
          <button onclick="{generatePDFReport}"
                  class="button--secondary">
            { translate("common.download.pdf") }
          </button>
          <button onclick="{generateODTReport}"
                  class="button--secondary">
            { translate("common.download.odt") }
          </button>
        </li>
    </div>
  </div>

  <script>
      this.translate = translate;
      this.project = { id: this.opts.projectId};
      this.testCasesI18nOpts = { count : 0, scope: 'testcase'};
      this.testCaseId = null;

      this.fecthProject = () => {
        projectsService.getProject(this.opts.projectId)
        .then((project) => {
            this.project = project;
            document.title = "Teko - " + project.name + " - " + this.translate('report.label');
        })
        .catch((msg) => {
            this.error = msg
            this.project = null;
            this.update();
        });
      }
      this.fecthProject();

      this.fetchTestCasesCount = () => {
        testCasesService.getTestCases(this.opts.projectId)
        .then((testCases) => {
          if (testCases) {
            this.testCasesI18nOpts.count = testCases.length
            this.testCasesI18nOpts.scope = 'testcase';
            this.update();
          }
        })
      };
      this.fetchTestCasesCount();

      this.generatePDFReport = () => {
        let url = reportsService.getProjectTestCasesPdfReportsUrl(this.project.id, this.translate.getLocale());
        window.open(url);
      };

      this.generateODTReport = () => {
        let url = reportsService.getProjectTestCasesOdtReportsUrl(this.project.id, this.translate.getLocale());
        window.open(url);
      };

      eventBus.on('locale-changed', () => {
        this.update();
      });
  </script>

</Reports>
