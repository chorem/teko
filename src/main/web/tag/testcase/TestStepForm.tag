let testStepService = require("../../service/TestStepService");
let FormUtils = require("../../utils/FormUtils");
let route = require("riot-route");
let translate = require('counterpart');
let eventBus = require("../../utils/TekoMagicEventBus");

<TestStepForm>
    <li class="teststeps-list-input-field group">
      <form method="post" ref="testStep" onsubmit="{create}" class="input-append">
        <input type="hidden" name="category" value="{category.toUpperCase()}" disabled="{!opts.alterable}" />
        <input type="text"
               name="label"
               placeholder="{translate('teststep.description.placeholder.'+category)}"
               disabled="{!opts.alterable}">
        <button type="submit"
                class="button"
                value=""
                title="{ translate('common.create') }"
                if="{opts.alterable}"
                tabindex="-1">
          { translate('common.add') }
        </button>
      </form>
    </li>
    <li if="{error}" class="error-notification">{error}</li>

    <script>
        this.error = "";
        this.translate = translate;
        this.category = this.opts.category;

        this.create = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let testStep = FormUtils.formToMap(this.refs.testStep);
            if (testStep.label.length) {
              testStepService.createTestStep(this.opts.testCaseId, testStep)
              .then(() => {
                  eventBus.trigger('step-updated');
              })
              .catch((msg) => {
                  this.error = msg || translate('teststep.saveError');
                  this.update();
              });
            }

            // FIXME LK 27/07/17 : Vide le champ à la soumission
            // Problème: En cas d'erreur il ne faudrait pas vider le champ
            e.target.children.label.value='';
        };

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</TestStepForm>