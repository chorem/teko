package org.chorem.teko.backend;

import io.quarkus.arc.config.ConfigProperties;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * Created by couteau on 27/01/17.
 */
@ConfigProperties(prefix = "teko")
public interface TekoConfig {

    @ConfigProperty(name = "frontend.url")
    public String frontendUrl();

    @ConfigProperty(name = "webSecurityKey")
    public String webSecurityKey();

    @ConfigProperty(name = "securityHashCost", defaultValue = "12")
    public int securityHashCost();
}
