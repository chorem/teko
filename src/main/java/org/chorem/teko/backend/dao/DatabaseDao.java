package org.chorem.teko.backend.dao;

import org.nuiton.spgeed.annotations.Script;

/**
 * Created by couteau on 27/01/17.
 */
public interface DatabaseDao {

    @Script(file = "initDb.sql")
    boolean createDataBase();

    @Script(file = "db/migrations/V0_2_0_001-add-users.sql")
    boolean updateDatabaseWithUser();

    @Script(file = "db/migrations/V0_2_0_002-add-project-visibility.sql")
    boolean updateDatabaseWithProjectVisibility();

    @Script(file = "db/migrations/V0_2_0_003-add-testCaseResult-lastUser.sql")
    boolean updateDatabaseWithTestCaseResultLastUser();
}
