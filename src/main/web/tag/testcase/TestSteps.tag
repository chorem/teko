require("./TestStepInline.tag");
let testStepService = require("../../service/TestStepService");
let route = require("riot-route");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<TestSteps>
  <div each="{category in categories}" class="teststeps-category">
    <label>{ translate('teststep.category.' + category) }</label>

    <ul>
      <testStepInline each="{testStep in getTestSteps(category)}"
                      data="{ testStep }"
                      test-case-id="{testCaseId}"
                      alterable="{alterable}">
      </testStepInline>

      <testStepForm category="{category}" test-case-id="{testCaseId}" if="{login && alterable}" alterable="{alterable}">
      </testStepForm>

    </ul>
  </div>

    <script>
        this.translate = translate;

        this.testSteps = {};
        this.categories = ["prerequisite", "action", "result"];
        this.testCaseId = this.opts.testCaseId;
        this.alterable;
        this.login = localStorage.getItem('accessToken');

        this.getTestSteps = (category) => {
          return this.testSteps[category];
        }

        this.separateStepsByCategory = (testSteps) => {
          if (testSteps) {
            testSteps.forEach((testStep) => {
              category = testStep.category.toLowerCase();
              if (!this.testSteps[category]) {
                this.testSteps[category] = [];
              }
              this.testSteps[category].push(testStep);
            });
          }
        };

        eventBus.on('navigation-testcase-changed', (testCaseId, alterable) => {
          this.alterable = alterable;
          testStepService.getTestSteps(testCaseId)
          .then((testSteps) => {
              this.testCaseId = testCaseId;
              this.testSteps = {};
              this.separateStepsByCategory(testSteps);
              this.update();
          });
        });

        eventBus.on('step-deleted', () => {
            testStepService.getTestSteps(this.testCaseId)
            .then((testSteps) => {
                this.testSteps = {};
                this.separateStepsByCategory(testSteps);
                this.update();
            });
        });

        eventBus.on('step-updated', () => {
            testStepService.getTestSteps(this.testCaseId)
            .then((testSteps) => {
                this.testSteps = {};
                this.separateStepsByCategory(testSteps);
                this.update();
            });
        });

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</TestSteps>