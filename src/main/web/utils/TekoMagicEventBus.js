let singleton = require("./Singleton");

class TekoMagicEventBus {
    constructor() {
        riot.observable(this);
    }
}

module.exports = singleton(TekoMagicEventBus);