package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class User {
    protected UUID id;
    protected String firstName;
    protected String lastName;
    protected String mail;
    protected String password;
    protected OffsetDateTime creationDate;

    public User() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //XXX mlebras 20180704 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setfirstname(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //XXX mlebras 20180704 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setlastname(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
