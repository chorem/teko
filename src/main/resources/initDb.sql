-- Database definition

-- Enum

CREATE TYPE status AS ENUM ('DRAFT', 'AVAILABLE', 'CLOSED', 'OBSOLETE');

CREATE TYPE validity AS ENUM ('NOT_TESTED', 'CAN_NOT_TESTED', 'KO', 'OK');


CREATE TABLE IF NOT EXISTS project(
    id              UUID        PRIMARY KEY,
    name            TEXT        UNIQUE NOT NULL,
    creationDate    TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS testcase(
    id              UUID        PRIMARY KEY,
    projectId       UUID        REFERENCES project(id) NOT NULL,
    label           TEXT        NOT NULL,
    ref             TEXT,
    status          TEXT,
    steps           UUID[],
    conditions      UUID[],
    criticality     TEXT,
    tags            TEXT[],
    creationDate    TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS teststep(
    id              UUID        PRIMARY KEY,
    label           TEXT        NOT NULL,
    category        TEXT        NOT NULL,
    creationDate    TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS testCampaign(
    id              UUID        PRIMARY KEY,
    projectId       UUID        REFERENCES project(id) NOT NULL,
    label           TEXT        NOT NULL,
    status          status      DEFAULT 'DRAFT',
    testCaseIds     UUID[],
    creationDate    TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS campaignExecution(
    id                  UUID        PRIMARY KEY,
    testCampaignId      UUID        REFERENCES testCampaign(id) NOT NULL,
    executionDate       TIMESTAMPTZ NOT NULL,
    testCaseResultIds   UUID[],
    creationDate        TIMESTAMPTZ NOT NULL
);

CREATE TABLE IF NOT EXISTS testCaseResult(
    id              UUID        PRIMARY KEY,
    testCaseId      UUID        REFERENCES testcase(id) NOT NULL,
    validity        validity,
    comment         TEXT,
    creationDate    TIMESTAMPTZ NOT NULL
);