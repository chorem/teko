require("./TestStepInline.tag");
let testStepService = require("../../service/TestStepService");
let translate = require('counterpart');
let eventBus = require("../../utils/TekoMagicEventBus");

<TestStepsDisplay>
  <div class="teststeps--read-only {animationClass}"
    <div each="{category in categories}" class="teststeps-category">
      <label>{ translate('teststep.category.' + category) }</label>

      <ul>
        <li each="{testStep in getTestSteps(category)}"  class="list-item">
          <TestStep
                        test-step="{ testStep }"
                        test-case-id="{testCaseId}" />
        </li>
      </ul>
    </div>
  </div>

    <script>
        this.translate = translate;

        this.testSteps = {};
        this.categories = ["prerequisite", "action", "result"];
        this.testCaseId = this.opts.testCaseId;
        this.animationClass = "appear";

        this.getTestSteps = (category) => {
          if (this.testSteps[category]) {
            return this.testSteps[category];
          } else {
            return [];
          }
        }

        this.separateStepsByCategory = (testSteps) => {
          if (testSteps) {
            testSteps.forEach((testStep) => {
              category = testStep.category.toLowerCase();
              if (!this.testSteps[category]) {
                this.testSteps[category] = [];
              }
              this.testSteps[category].push(testStep);
            });
          }
        };

        // Load steps for the test case, and make a map indexed by the steps category
        this.loadTestSteps = (testCaseId) => {

          testStepService.getTestSteps(testCaseId)
          .then((testSteps) => {
              this.testSteps = {};
              this.separateStepsByCategory(testSteps);
              this.animationClass = "appear";
              this.update();
          });
        };

        // Load steps now !
        this.loadTestSteps(this.testCaseId);

        // and reload them on each test case change !
        eventBus.on('navigation-testcase-changed', (testCaseId) => {
          this.testCaseId = testCaseId;
          this.animationClass = "";

          this.loadTestSteps(testCaseId);
        });

        eventBus.on('locale-changed', () => {
          this.update();
        });

    </script>

</TestStepsDisplay>