# Mise à jour de la police d'icônes

* Ajout d'icônes au format svg dans le dossier ```doc/icons```
* Exécuter le script ```fontcustom.sh```

# Source des icônes

* https://github.com/stephenhutchings/typicons.font
* https://www.fontawesome.com/
* https://feathericons.com/