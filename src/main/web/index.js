let route = require("riot-route");
require("./tag/Site.tag");
require("./tag/layout/Footer.tag");
require("./tag/layout/Header.tag");
require("./tag/project/Projects.tag");

require("./less/teko.less");

riot.mount("*");
route.base("/");
route.start(true);
