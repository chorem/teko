package org.chorem.teko.backend;

/**
 * Created by couteau on 25/03/17.
 */
public class TekoTechnicalException extends RuntimeException {

    private static final long serialVersionUID = -6861476631481807344L;

    public TekoTechnicalException() {
    }

    public TekoTechnicalException(String message) {
        super(message);
    }

    public TekoTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public TekoTechnicalException(Throwable cause) {
        super(cause);
    }

}