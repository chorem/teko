let campaignService = require("../../service/TestCampaignService");
let testStepService = require("../../service/TestStepService");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');
let jwt = require('jsonwebtoken');

<CampaignExecutions>

    <div class="list aside" if={campaign}>
      <div class="list-header">
        <div class="list-title list-header-title">
          {translate("campaignExecution.history")}
        </div>
      </div>

      <ul class="list-items">
        <li each="{execution in campaignExecutions}"
            class="list-item list-item--link {hidden : execution != currentExecution && currentExecution}">
          <a class="back-button"
              if={currentExecution}
              onclick="{parent.showTestCaseResult.bind(parent, null)}">
            <i class="icon icon-back"></i>
          </a>
          <a onclick="{parent.showTestCaseResult.bind(parent, execution)}"
              class="{active : execution == currentExecution} {with-back-button : execution == currentExecution}">
            { translateExecution(execution) }
            <span class="test-execution-results-right">
              <span if="{execution.testCaseResults.length == execution.success}" class="test-execution-result--OK"></span>
              <span if="{execution.testCaseResults.length > execution.success}" class="test-execution-result--KO"></span>
            </span>
          </a>
        </li>
        <li if="{!campaignExecutions.length}" class="list-item">
          {translate("campaignExecution.zero")}
        </li>
      </ul>

      <CampaignExecution execution="{currentExecution}" if="{currentExecution}"/>
    </div>


    <script>
      this.translate = translate;
      this.campaign = undefined;
      this.dateOpts = {'type' :'date'};
      this.currentExecution = undefined;
      this.campaignExecutions = [];

      if(localStorage.getItem('accessToken')){
          this.user = jwt.decode(localStorage.getItem('accessToken')).userId;
      };

      eventBus.on('navigation-campaign-changed', (campaignId) => {
        campaignService.getCampaign(campaignId)
        .then((campaign) => {
          if (campaign) {
            this.campaign = campaign;
            this.currentExecution = undefined;
            this.update();
          }
        });

        campaignService.getCampaignExecutions(campaignId)
        .then((campaignExecutions) => {
          if (campaignExecutions) {
            this.campaignExecutions = [];
            campaignExecutions.forEach((campaignExecution) => {
              campaignExecution.success = 0;
              let campaignTestCaseResults = campaignExecution.testCaseResults;
              campaignTestCaseResults.forEach((testCaseResult) => {
                if (testCaseResult.validity == "OK") {
                  campaignExecution.success = campaignExecution.success + 1;
                }
              });

              this.campaignExecutions.push(campaignExecution);
            });
          } else {
            this.campaignExecutions = [];
          }
          this.update();
        });
      });

      this.translateExecution = (execution) => {
        return translate("campaignExecution.summary", { "year" : execution.executionDate.year, "month" : execution.executionDate.monthValue, "day" : execution.executionDate.dayOfMonth });
      };

      this.showTestCaseResult = (execution) => {
        this.currentExecution = execution;
        this.update();
        eventBus.trigger('campaignExecution-changed', execution);
      };

      eventBus.on('locale-changed', () => {
        this.update();
      });

    </script>

</CampaignExecutions>