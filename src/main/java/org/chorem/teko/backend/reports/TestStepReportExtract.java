package org.chorem.teko.backend.reports;

import net.sf.jasperreports.engine.JasperReport;
import org.chorem.teko.backend.entity.TestStep;

import java.util.List;
import java.util.UUID;

/**
 * Created by lkaufmann on 13/09/17.
 */
public class TestStepReportExtract {

    protected UUID id;
    protected String label;
    protected String categoryName;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
