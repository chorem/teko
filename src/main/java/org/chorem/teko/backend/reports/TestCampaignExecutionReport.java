package org.chorem.teko.backend.reports;

import net.sf.jasperreports.engine.JasperReport;

import java.util.List;

/**
 * Created by couteau on 25/03/17.
 */
public class TestCampaignExecutionReport {
    protected String projectName;
    protected String campaignName;
    protected String executionDate;
    protected String generationDate;
    protected List<TestCaseExecutionReportExtract> testCaseResults;
    protected JasperReport subReport;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(String executionDate) {
        this.executionDate = executionDate;
    }

    public String getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(String generationDate) {
        this.generationDate = generationDate;
    }

    public List<TestCaseExecutionReportExtract> getTestCaseResults() {
        return testCaseResults;
    }

    public void setTestCaseResults(List<TestCaseExecutionReportExtract> testCaseResults) {
        this.testCaseResults = testCaseResults;
    }

    public JasperReport getSubReport() {
        return subReport;
    }

    public void setSubReport(JasperReport subReport) {
        this.subReport = subReport;
    }
}
