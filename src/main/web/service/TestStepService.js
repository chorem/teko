let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class TestStepService extends FetchService {

    getTestSteps(testCaseId) {
        return this.get("/testCase/"+testCaseId+"/testStep");
    }

    createTestStep(testCaseId, testStep) {
        return this.post("/testCase/"+testCaseId+"/testStep", testStep);
    }

    getTestStep(id) {
        return this.get("/testStep/"+ id);
    }

    updateTestStep(id, testStep) {
        return this.post("/testStep/"+ id, testStep);
    }

    deleteTestStep(stepId, testCaseId) {
        return this.delete("/testCase/" + testCaseId + "/testStep/" + stepId);
    }
}

module.exports = singleton(TestStepService);