package org.chorem.teko.backend.entity;

/**
 * Created by lebras on 01/06/18.
 **/
public enum Validity {
    NOT_TESTED,
    CAN_NOT_TESTED,
    KO,
    OK,
    ;
}
