package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class TestCampaign {

    protected UUID id;

    protected UUID projectId;

    protected String label;

    protected UUID[] testCaseIds;

    protected Status status;

    protected boolean alterable;

    protected OffsetDateTime creationDate;

    public TestCampaign() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getProjectId() {
        return projectId;
    }

    public void setProjectId(UUID projectId) {
        this.projectId = projectId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public UUID[] getTestCaseIds() {
        return testCaseIds;
    }

    public void setTestCaseIds(UUID[] testCaseIds) {
        this.testCaseIds = testCaseIds;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    //XXX ymartel 20170718 : must have setters with only lower case cause spgeed cannot manage camel case for the moment
    public void setProjectid(UUID projectId) {
        this.projectId = projectId;
    }

    public void setTestcaseids(UUID[] testCaseIds) {
        this.testCaseIds = testCaseIds;
    }

    public boolean isAlterable() {
        return alterable;
    }

    public void setAlterable(boolean alterable) {
        this.alterable = alterable;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
