-- Table users
CREATE TABLE IF NOT EXISTS users(
    id              UUID         PRIMARY KEY,
    firstName       TEXT,
    lastName        TEXT,
    mail            TEXT         UNIQUE NOT NULL,
    password        TEXT         NOT NULL,
    salt            TEXT         NOT NULL,
    creationDate    TIMESTAMPTZ  NOT NULL
);

INSERT INTO users (id, firstName, lastName, mail, password, salt, creationDate)
                  VALUES ('3cd769d1-3d62-4708-9026-2331a2c3f608',
                          'admin',
                          'admin',
                          'admin@email.com',
                          '0ef69ef49f262d81fd2e3199ff07a3bd54078154a169f13457a00ad5050cbe91', --admin
                          '8WjrRihR8EbCqHpHfIO5xQ==',
                          '2018-07-12 14:37:14.307+02');

-- ALTER TABLE testCaseResult ADD COLUMN lastUser UUID REFERENCES users(id) NOT NULL;