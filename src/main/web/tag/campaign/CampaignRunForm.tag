let route = require("riot-route");
let translate = require('counterpart');
let projectsService = require("../../service/ProjectService");
let campaignService = require("../../service/TestCampaignService");
let testStepService = require("../../service/TestStepService");
let testCaseService = require("../../service/TestCaseService");
let eventBus = require("../../utils/TekoMagicEventBus");
let campaignRunStatistics = require("../sharedComponents/campaignRunStatistics.tag");

<CampaignRunForm class="main-content--with-sticky-footer">
    <div class="list aside header-gap">
      <div class="list-header">
        <div class="list-header-title">
          {testCases.length} { translate("testcase.label")}
        </div>
      </div>
      <ul class="list-items">
        <li each="{testCase in testCases}" class="list-item list-item--link">
          <a href=""
            onclick="{parent.loadTestSteps.bind(parent, testCase.id, testCase.label)}"
            class="{active : testCase.id == currentTestCaseId}">
            {testCase.label}
          </a>
          <span if="{testCaseResults && testCaseResults[testCase.id]}"
                class="test-execution-result--{testCaseResults[testCase.id].validity;}"></span>
          <span class="test-execution-results-left test-execution-result--WARNING"
                if="{testCaseResults && testCaseResults[testCase.id] && testCaseResults[testCase.id].info.split('\n').length > 4;}"></span>
        </li>
      </ul>
    </div>

    <div class="sticky-footer">
      <div class="display-in-a-row">
        <a href="/project-campaigns/{projectId}" class="button  button--discreet" >{ translate("common.cancel")}</a>
        <div>{translate("campaignExecution.title", titleOpts)}</div>
        <div>{Object.keys(testResults).length} / {testCases.length} ({Math.round(Object.keys(testResults).length / testCases.length * 100)} %)
          {countTestCaseWithResult('OK'); } <i class="icon-ok"/>
          {countTestCaseWithResult('KO'); } <i class="icon-ko"/>
        </div>
        <a href=""
          class="button {active : true}"
          onclick="{validCampaignExecution}">{translate("campaignExecution.validate")}</a>
      </div>

      <div class="chart-progress-bar">
        <div class="bar bar--ok" title="Traités" style="flex: {countTestCaseWithResult('OK'); }"></div>
        <div class="bar bar--ko" title="Traités" style="flex: {countTestCaseWithResult('KO'); }"></div>
        <div class="bar bar--empty" title="Non traités" style="flex: {testCases.length - Object.keys(testResults).length}"></div>
      </div>
    </div>

    <div class="sheet main-content">
      <div class="list-header" if="{currentTestCaseLabel}">
        <div class="list-header-button align-left">
          <button onclick="{previousTestCase}">
            <i data-icon class="icon icon-arrow-left"></i>
          </button>
        </div>
        <div class="list-header-title">
          {currentTestCaseLabel}
        </div>
        <div class="list-header-button align-right">
          <button onclick="{nextTestCase}" if="{currentTestCaseId != -1}">
            <i data-icon class="icon icon-arrow-right"></i>
          </button>
        </div>
      </div>
      <div class="sheet-content" if="{currentTestCaseId && currentTestCaseId != -1}">
        <TestCaseResultForm test-case-result="{testCaseResults[currentTestCaseId]}"  />
        <TestStepsDisplay test-case-id="{currentTestCaseId}"/>
      </div>

      <div class="sheet-content" if="{currentTestCaseId && currentTestCaseId == -1}">
        <div class="notification-no-results">
          {translate("campaignExecution.end")}
        </div>
      </div>
    </div>

    <script>
        this.translate = translate;
        this.campaign = undefined;
        this.testCases = [];
        this.testCaseResults = {};
        this.currentTestCaseLabel = undefined;
        this.testResults = {};
        this.titleOpts = {'campaignLabel' :''};
        this.projectId = undefined;
        this.project = undefined;

        this.loadTestSteps = (testCaseId, testCaseLabel) => {
            eventBus.trigger('navigation-testcase-changed', testCaseId);
            eventBus.trigger('navigation-testcaseresult-changed', this.testCaseResults[testCaseId]);
            this.currentTestCaseLabel = testCaseLabel;
            this.currentTestCaseId = testCaseId;

            this.update();
        };

        campaignService.getCampaign(this.opts.campaignId)
            .then((campaign) => {
            if (campaign) {
                this.campaign = campaign;
                this.projectId = campaign.projectId;
                this.testCases = campaign.testCases;
                this.testCases.forEach((testCase) => {
                    if(testCase.conditions != null && testCase.conditions.length > 0){
                        this.genInfos(testCase.id).then ((info) => {
                            this.testCaseResults[testCase.id] = {'testCaseId': testCase.id, 'validity': 'CAN_NOT_TESTED', 'comment': '', 'info':info};
                        })
                    }else{
                        this.testCaseResults[testCase.id] = {'testCaseId': testCase.id, 'validity': 'NOT_TESTED', 'comment': '','info':''};
                    }
                });
                this.titleOpts = {'campaignLabel' : campaign.label};
                this.update();
            }
        }).then( () => {
            projectsService.getProject(this.projectId)
                .then((project) => {
                eventBus.trigger('project-loaded', project);
                this.currentProjectId = this.projectId;
                this.project = project;
                this.update();
            })
            this.loadTestSteps(this.testCases[0].id, this.testCases[0].label);
        });

        this.genInfos = (testCaseId) => {
            return new Promise ((resolve) => {
                testCaseService.getCondition(testCaseId)
                .then((conditions) => {
                    let info = translate('campaign.execution.necessary');
                    conditions.forEach((condition) => {
                        if(condition.id != testCaseId){
                            info += condition.label + '; ';
                        }
                    });
                    info = info.substring(0,info.length-2) + '. ';
                    let split = info.split('\n');
                    info += translate('campaign.execution.validate') + split[1];
                    resolve(info);
                })
            })
        }

        this.validCampaignExecution = () => {
            let testCaseResultsArray = new Array();
            //delete this.testCaseResults.info;
            for (let testCaseId in this.testCaseResults) {
                delete this.testCaseResults[testCaseId].info;
                testCaseResultsArray.push(this.testCaseResults[testCaseId]);
            }

            let campaignExecution = { 'testCampaignId' : this.opts.campaignId, 'testCaseResults' : testCaseResultsArray};
            campaignService.executeCampaign(campaignExecution)
            .then(() => {
                route('/project-campaigns/'+this.campaign.projectId);
            });
        };

        this.editValidity = (testCaseId) => {
            return new Promise ((resolve) =>{
                testCaseService.getDependOf(testCaseId)
                    .then((sons) => {
                    if(sons != null && sons.length > 0){
                        sons.forEach((son) => {
                            var info = this.testCaseResults[son.id].info;
                            var label = this.currentTestCaseLabel;
                            var regex = new RegExp("\n.*\n.*(\n|\n.*; )" + label + "(\.|;)");
                            var test = regex.test(info);

                            if(test && this.testCaseResults[testCaseId].validity == 'OK'){
                                var split = info.split('\n');
                                info = split[3];
                                regex = new RegExp('(; )?' + label + '(;|.)');
                                info = info.replace(regex, ';');
                                info = info.substring(0,info.length -2) + '. ';
                                if(info[0] == ';'){
                                    info = info.substring(2, info.length);
                                }
                                split[3] = info;
                                info = '';
                                if(split[3].length < 3){
                                    for (var i = 0; i < 2; i++){
                                        info += split[i] + '\n';
                                    }
                                    if(this.testCaseResults[son.id].validity =='CAN_NOT_TESTED'){
                                        this.testCaseResults[son.id].validity = 'NOT_TESTED';
                                    }
                                }else{
                                    for (var i = 0; i < split.length; i++){
                                        info += split[i] + '\n';
                                    }
                                    info = info.substring(0, info.length-1);
                                }
                            }

                            if(!test && this.testCaseResults[testCaseId].validity != 'OK'){
                                var split = info.split('\n');
                                if(split.length == 3){
                                    info+= 'Cas de test non validés:\n' + label + '. ';
                                    if(this.testCaseResults[son.id].validity == 'NOT_TESTED'){
                                        this.testCaseResults[son.id].validity = 'CAN_NOT_TESTED';
                                    }else{
                                        info += translate("campaign.execution.error");
                                    }
                                }else{
                                    info = split[3];
                                    info = info.replace('. ','; ' + label + '. ');
                                    split[3] = info;
                                    info = '';
                                    for (var i = 0; i < split.length; i++){
                                        info += split[i] + '\n';
                                    }
                                    info = info.substring(0, info.length-1);
                                }
                            }

                            this.testCaseResults[son.id].info = info;
                        });
                    }
                    resolve();
                })
            })
        }

        this.nextTestCase = (index) => {
            if (typeof index != "number") {
                index = this.testCases.findIndex((element) => element.id == this.currentTestCaseId);
            }

            if (index < this.testCases.length) {
                if (this.testCases[index].id != this.currentTestCaseId) {
                    this.currentTestCaseId = this.testCases[index].id;
                    this.currentTestCaseLabel = this.testCases[index].label;

                    this.update();
                    this.loadTestSteps(this.currentTestCaseId, this.currentTestCaseLabel);
                } else {
                    this.nextTestCase(index + 1);
                }
            } else {
                this.currentTestCaseLabel = undefined;
                this.currentTestCaseId = -1;
                this.update();
            }
        }

        this.previousTestCase = (index) => {
          if (typeof index != "number") {
              index = this.testCases.findIndex((element) => element.id == this.currentTestCaseId);
          }

          if (index >= 0) {
            if (this.testCases[index].id != this.currentTestCaseId) {
                this.currentTestCaseId = this.testCases[index].id;
                this.currentTestCaseLabel = this.testCases[index].label;
                this.update();
                this.loadTestSteps(this.currentTestCaseId, this.currentTestCaseLabel);
            } else {
                this.previousTestCase(index - 1);
            }
          }
        },

        this.countTestCaseWithResult = (result) => {
          return Object.values(this.testResults).filter((x) => x === result).length;
        }

        eventBus.on('update-testcaseresult-execution', (testCaseResultExecution) => {
            this.testCaseResults[testCaseResultExecution.testCaseId] = testCaseResultExecution;
            this.testResults[testCaseResultExecution.testCaseId] = testCaseResultExecution.validity;
            this.editValidity(testCaseResultExecution.testCaseId).then(() => {
                this.nextTestCase();
            })
        });

        eventBus.on('locale-changed', () => {
            this.update();
        });
    </script>

</CampaignRunForm>