let route = require("riot-route");
let translate = require('counterpart');
let projectsService = require("../../service/ProjectService");
let campaignService = require("../../service/TestCampaignService");
let testCasesService = require("../../service/TestCaseService");
let testCaseService = require("../../service/TestCaseService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");

<CampaignEditForm>
  <div class="sheet">
    <div class="sheet-header">
      {translate("campaign.update")}
    </div>

    <div class="sheet-content">
      <div if="{error}" class="error-notification">{error}</div>

      <form method="post" ref="testCampaign" onsubmit="{modify}">
        <input type="hidden" name="id">
        <input type="hidden" name="projectId">

        <ul class="form-content">
          <li>
            <label for="label">{ translate("campaign.name.label")}</label>
            <input type="text" name="label" placeholder="{translate('campaign.name.placeholder')}" required>
          </li>

          <li>
            <label>{ translate("testcase.label")}</label>

            <div class="form-selection-buttons">
              <button onclick="{selectAllTestCases}"
                      type="button">
                { translate("common.selectAll")}
              </button>
              <button onclick="{showFilters}"
                  type="button"
                 class="button {active : projectsFiltersShow}">
                <i class="icon icon-filter"></i> {translate("testcase.filter")}
              </button>

              <div class="list-filters" if="{projectsFiltersShow}">
                <i class="icon icon-filter" title="{translate("testcase.filter")}"></i>
                <select id="criticality" onChange="{filterTestCase}">
                  <option value="">{translate('criticality.ALL')}</option>
                  <option value="LOW">{translate('criticality.LOW')}</option>
                  <option value="MEDIUM">{translate('criticality.MEDIUM')}</option>
                  <option value="HIGH">{translate('criticality.HIGH')}</option>
                  <option value="CRITICAL">{translate('criticality.CRITICAL')}</option>
                </select>
                <select id="status" onChange="{filterTestCase}">
                      <option value="">{translate('status.ALL')}</option>
                      <option value="DRAFT">{translate('status.DRAFT')}</option>
                      <option value="AVAILABLE">{translate('status.AVAILABLE')}</option>
                      <option value="CLOSED">{translate('status.CLOSED')}</option>
                </select>
                <input type="" id="keyWord" onchange="{filterTestCase}" size="12" placeholder="{ translate('testcase.filter') }" >
              </div>
            </div>

            <ul class="form-selection-list">
              <li each="{testCase in testCases}" if="{(testCase.status != 'DRAFT' || campaignStatus == 'DRAFT')}">

                <span if="{testCase.status == 'DRAFT' && campaignStatus == 'DRAFT'  && testCaseFiltre[testCase.id]}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         multiplechoices="true"
                         onchange="{parent.manageDraftTestCase.bind(parent)}"
                         checked="{testCaseChecked[testCase.id] == true}"
                         onclick="{modifChecked(testCase.id)}"/>
                  <label for="{testCase.id}" class="checkbox-label"> draft
                    <span class="board-list-item-extra-infos">
                      <i class="icon icon-draft"></i>
                      { translate("common.draft") }
                    </span>
                    {testCase.label}
                  </label>
                  <a class="form-selection-list-action"
                     onclick="{parent.loadTestSteps.bind(parent, testCase.id, testCase.label)}">
                    <span class="icon icon-eye"></span>
                  </a>
                </span>

                <span if="{testCase.status != 'DRAFT' && testCaseFiltre[testCase.id]}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         multiplechoices ="true"
                         disabled if="{testCase.status == 'OBSOLETE'}"/>
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         multiplechoices ="true"
                         if="{testCase.status != 'OBSOLETE'}"
                         checked="{testCaseChecked[testCase.id] == true}"
                         onclick="{modifChecked(testCase.id)}"/>
                  <label for="{testCase.id}" class="checkbox-label">
                    <span class="board-list-item-extra-infos" if="{testCase.status == 'DRAFT'}">
                      <i class="icon icon-draft"></i>
                      { translate("common.draft") }
                    </span>
                    <span class="board-list-item-extra-infos" if="{testCase.status == 'OBSOLETE'}">
                      <i class="icon icon-draft"></i>
                      { translate("common.obsolete") }
                    </span>
                    {testCase.label}
                  </label>
                  <a class="form-selection-list-action"
                     onclick="{parent.loadTestSteps.bind(parent, testCase.id, testCase.label)}">
                    <span class="icon icon-eye"></span>
                  </a>
                </span>
                <span if="{testCaseFiltre[testCase.id] == false}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         if="{testCase.status != 'OBSOLETE'}"
                         multiplechoices ="true"
                         disabled
                         checked="{testCaseChecked[testCase.id] == true}" />
                  <label for="{testCase.id}" class="checkbox-label"><font color="#d3d3d3">{testCase.label}</font></label>
                </span>
              </li>
            </ul>
          </li>
        </ul>
      </form>

      <div class="form-buttons">
        <a href="/project-campaigns/{projectId}" class="button button-secondary">{ translate('common.cancel')}</a>
        <button class="button--main" onclick="{saveDraft}" if="{campaignStatus == 'DRAFT'}">{ translate('common.saveDraft')}</button>
        <button class="button--main" onclick="{modify}" if="{usedDraftTestCases.length == 0 && campaignStatus != 'DRAFT'}">{ translate('common.update')}</button>
        <button class="button--main" onclick="{modify}" if="{usedDraftTestCases.length == 0 && campaignStatus == 'DRAFT'}">{ translate('common.create')}</button>
        <button class="button--disabled" if="{usedDraftTestCases.length > 0}">{ translate('common.update')}</button>
        <div class="warning-notification" if="{usedDraftTestCases.length > 0}">
            { translate('campaign.canOnlySaveDraft')}
        </div>
      </div>

      <div if="{currentTestCaseLabel}" class="sheet-slide-pannel">
        <div class="sheet-slide-pannel-title">
          {currentTestCaseLabel} : {translate("campaign.testSteps")}
          <button onclick="{hideSlidePannel}">
            <span class="icon-cancel"></span>
          </button>
        </div>
        <TestStepsDisplay test-case-id="{currentTestCaseId}"/>
      </div>
    </div>
  </div>

    <script>
        this.translate = translate;
        this.testCases = [];
        this.currentTestCaseLabel = undefined;
        this.currentTestCaseId = undefined;
        this.projectId = undefined;
        this.project = { name : '' };
        this.campaignStatus = "DRAFT";
        this.usedTests = [];
        this.usedDraftTestCases = [];
        this.testCaseFiltre = {};
        this.testCaseChecked = {};

          campaignService.getCampaign(this.opts.campaignId)
          .then((testCampaign) => {
              this.testCampaign = testCampaign;
              this.projectId = testCampaign.projectId;
              this.campaignStatus = testCampaign.status;

              testCasesService.getTestCases(testCampaign.projectId)
              .then((testCases) => {
                  this.testCases = testCases;
                  testCases.forEach((testCase) => {
                    this.testCaseChecked[testCase.id] = false;
                    let index = -1;
                      do{
                          index++;
                          if(testCases[index].id == testCase.id){
                              this.testCaseFiltre[testCase.id] = true;
                          }else{
                              this.testCaseFiltre[testCase.id] = false;
                          }
                      }while(this.testCaseFiltre[testCase.id] != true && index < testCases.length-1);
                    });
                  this.update();
                  FormUtils.fillForm(this.refs.testCampaign, testCampaign);
              });

              campaignService.getCampaignTestCases(this.opts.campaignId)
              .then((usedTests) => {
                this.usedTests = usedTests;
                usedTests.forEach((testCase) => {
                  usedTests.forEach((testCase) => {
                    this.testCaseChecked[testCase.id] = true;
                  });
                  if (testCase.status == "DRAFT" && this.usedDraftTestCases.indexOf(testCase.id) < 0) {
                    this.usedDraftTestCases.push(testCase.id);
                  }
                })
                this.update();
                FormUtils.fillForm(this.refs.testCampaign, testCampaign);
              });

          })

        .then( () => {
          projectsService.getProject(this.projectId)
          .then((project) => {
            eventBus.trigger('project-loaded', project);
            this.project = project;
            this.update();
          })
        });

        this.modifChecked = (testCaseId) => {
            return (e) => {
                this.testCaseChecked[testCaseId] = !this.testCaseChecked[testCaseId];
                if(this.testCaseChecked[testCaseId]){
                    testCasesService.getCondition(testCaseId, true)
                        .then((conditions) => {
                        conditions.forEach(function(condition){
                            this.testCaseChecked[condition.id] = this.testCaseChecked[testCaseId];
                        });
                        this.update();
                    })
                }else{
                    testCasesService.getDependOf(testCaseId, true)
                        .then((sons) => {
                        sons.forEach(function(son){
                            this.testCaseChecked[son.id] = this.testCaseChecked[testCaseId];
                        });
                        this.update();
                    })
                }
                this.update();
            }
        }

        this.filterTestCase = () => {
            let status = document.getElementById("status").value;
            let keyWord = document.getElementById("keyWord").value;
            let criticality = document.getElementById("criticality").value;
            this.testCaseFiltre = {};
            testCaseService.getTestCasesWithFilter(this.projectId, keyWord, status, criticality)
              .then((testCases) => {
                  if (testCases) {
                    this.testCases.forEach(function(testCase){
                      let index = -1;
                      do{
                        index++;
                        if(testCases[index].id == testCase.id){
                          this.testCaseFiltre[testCase.id] = true;
                        }else{
                          this.testCaseFiltre[testCase.id] = false;
                        }
                      }while(this.testCaseFiltre[testCase.id] != true && index < testCases.length-1);
                    });
                  }else{
                    this.testCases.forEach(function(testCase){
                      this.testCaseFiltre[testCase.id] = false;
                    });
                  };
                  this.update();
              })
        };

        this.showFilters = () => {
          this.projectsFiltersShow = !this.projectsFiltersShow;
        }

        this.loadTestSteps = (testCaseId, testCaseLabel) => {
            this.currentTestCaseLabel = testCaseLabel;
            this.currentTestCaseId = testCaseId;
            this.update();
            eventBus.trigger('navigation-testcase-changed', testCaseId);
        };

        this.hideSlidePannel = () => {
            this.currentTestCaseLabel = undefined;
            this.currentTestCaseId = undefined;
            this.update();
            eventBus.trigger('navigation-testcase-changed', undefined);
        };

        this.selectAllTestCases = () => {
          this.testCases.forEach((testcase) => {
            this.testCaseChecked[testcase.id] = true;
          });
          this.update();
        };

        this.manageDraftTestCase = (e) => {
          let element = e.target;
          let testCaseId = element.value;
          let index = this.usedDraftTestCases.indexOf(testCaseId);
          if (element.checked && index < 0) {
            this.usedDraftTestCases.push(testCaseId);
          } else if (!element.checked && index > -1) {
            this.usedDraftTestCases.splice(index, 1);
          }
        };

        this.saveDraft = () => {

            let testCampaign = FormUtils.formToMap(this.refs.testCampaign);
            testCampaign['status'] = "DRAFT";
            campaignService.updateCampaign(testCampaign.id, testCampaign)
            .then(() => {
                route('/project-campaigns/'+testCampaign.projectId);
            })
            .catch((msg) => {
                this.error = msg || translate('campaign.saveError');
                this.update();
            });
        };

        this.modify = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let testCampaign = FormUtils.formToMap(this.refs.testCampaign);
            testCampaign['status'] = "AVAILABLE";
            campaignService.updateCampaign(testCampaign.id, testCampaign)
            .then(() => {
                route('/project-campaigns/'+testCampaign.projectId);
            })
            .catch((msg) => {
                this.error = msg || translate('campaign.saveError');
                this.update();
            });
        };

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</CampaignEditForm>