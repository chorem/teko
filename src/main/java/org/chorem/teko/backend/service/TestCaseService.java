package org.chorem.teko.backend.service;

import org.chorem.teko.backend.dao.TestCaseDao;
import org.chorem.teko.backend.dao.TestStepDao;
import org.chorem.teko.backend.entity.Criticality;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.TestCase;
import org.nuiton.spgeed.SqlSession;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by couteau on 05/02/17.
 */
@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestCaseService extends TekoAbstractService {

    @Inject
    TokenService tokenService;

    @GET
    @Path("/testCase/ping")
    public boolean ping() {
        return true;
    }

    @POST
    @Path("/testCase/create/{projectId}")
    public UUID create(@PathParam("projectId") UUID projectId,
                       TestCase testCase,
                       @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        if (tokenService.isUserAllowedToSeeProject(authorization, projectId, session)) {
            TestCaseDao dao = session.getDao(TestCaseDao.class);
            testCase.setId(UUID.randomUUID());
            testCase.setProjectId(projectId);
            if (testCase.getStatus() == null) {
                testCase.setStatus(Status.DRAFT);
            }
            dao.createTestCase(testCase);
            return testCase.getId();
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCase/getAll/{projectId}")
    public TestCase[] getAllForProject(@PathParam("projectId") UUID projectId,
                                       @QueryParam("order") boolean order,
                                       @QueryParam("attribut") String attribut,
                                       @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        if (tokenService.isUserAllowedToSeeProject(authorization, projectId, session)) {
            return dao.findAllByProjectId(projectId, attribut, order);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCase/get/{id}")
    public TestCase getTestCase(@PathParam("id") UUID id,
                                @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase testCase = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, testCase.getProjectId(), session)) {
            return testCase;
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCase/{id}/tags")
    public String[] getTestCaseTags(@PathParam("id") UUID id,
                                    @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase testCase = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, testCase.getProjectId(), session)) {
            return dao.getTestCaseTags(id).getTags();
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }


    @POST
    @Path("/testCase/update/{id}")
    public void updateTestCase(@PathParam("id") UUID id,
                               TestCase testCase,
                               @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            dao.updateTestCase(id, testCase);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/testCase/{id}/tags")
    public void addTestTag(@PathParam("id") UUID id,
                           String tag,
                           @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            dao.addTag(id, tag);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/testCase/{id}/tags")
    public void removeTestTag(@PathParam("id") UUID id,
                              String tag,
                              @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            dao.removeTag(id, tag);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/project/{projectId}/testCase/filter")
    public TestCase[] getTestCaseWithFilter(@PathParam("projectId") UUID projectId,
                                            @QueryParam("keyWord") String keyWord,
                                            @QueryParam("status") Status status,
                                            @QueryParam("criticality") Criticality criticality,
                                            @QueryParam("order") boolean order,
                                            @QueryParam("attribut") String attribut,
                                            @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        if (tokenService.isUserAllowedToSeeProject(authorization, projectId, session)) {
            String statusString = status == null ? "" : status.toString();
            String criticalityString = criticality == null ? "" : criticality.toString();
            if (keyWord != null) {
                keyWord = "%" + keyWord + "%";
            }
            return dao.findAllWithFilter(projectId, keyWord, criticalityString, statusString, order, attribut);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/testCase/{id}/conditions/{conditionId}")
    public void addCondition(@PathParam("id") UUID id,
                             @PathParam("conditionId") UUID conditionId,
                             @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            Set<TestCase> conditions = getConditions(conditionId, true, authorization);
            TestCase[] condition = dao.getConditions(id);
            if (condition != null) {
                for (TestCase testCase : condition) {
                    for (TestCase test : conditions) {
                        if (test.getId().equals(testCase.getId())) {
                            dao.removeCondition(id, testCase.getId());
                            break;
                        }
                    }
                }
            }
            dao.addCondition(id, conditionId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/testCase/{id}/conditions/{conditionId}")
    public void removeCondition(@PathParam("id") UUID id,
                                @PathParam("conditionId") UUID conditionId,
                                @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            dao.removeCondition(id, conditionId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCase/{id}/conditions")
    public Set<TestCase> getConditions(@PathParam("id") UUID id,
                                       @QueryParam("withDepth") boolean withDepth,
                                       @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            TestCase[] conditions = dao.getConditions(id);
            Set<TestCase> complete = new HashSet<>();
            if (conditions != null) {
                for (TestCase testCase : conditions) {
                    complete.add(testCase);
                    if (withDepth) {
                        complete.addAll(getConditions(testCase.getId(), true, authorization));
                    }
                }
            }
            return complete;
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCase/{id}/dependOf")
    public Set<TestCase> getDependOf(@PathParam("id") UUID id,
                                     @QueryParam("withDepth") boolean withDepth,
                                     @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            TestCase[] dependOf = dao.getDependOf(id);
            Set<TestCase> complete = new HashSet<>();
            if (dependOf != null) {
                for (TestCase testCase : dependOf) {
                    complete.add(testCase);
                    if (withDepth) {
                        complete.addAll(getDependOf(testCase.getId(), true, authorization));
                    }
                }
            }
            return complete;
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/testCase/{id}")
    public void deleteTestCase(@PathParam("id") UUID id,
                               @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        TestCaseDao dao = session.getDao(TestCaseDao.class);
        TestCase tc = dao.findById(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, tc.getProjectId(), session)) {
            dao.deleteTestCase(id);
            TestStepDao testStepDao = session.getDao(TestStepDao.class);
            testStepDao.deleteOrphanTestStep();
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }
}
