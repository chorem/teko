package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.CampaignExecution;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public interface CampaignExecutionDao {

    @Update(sql = "INSERT INTO campaignExecution (id, testCampaignId, executionDate, testCaseResultIds, creationDate) "
                + "VALUES ${campaignExecution | values('id', 'testCampaignId', 'executionDate', 'testCaseResultIds', 'creationDate')}")
    public int create(CampaignExecution campaignExecution);

    @Select(sql = "SELECT id, testCampaignId, executionDate, testCaseResultIds, creationDate "
                + " FROM campaignExecution "
                + " WHERE id=${id}")
    public CampaignExecution findById(UUID id);

    @Select(sql = "SELECT id, testCampaignId, executionDate, testCaseResultIds, creationDate "
                + " FROM campaignExecution "
                + " ORDER BY executionDate ASC")
    public CampaignExecution[] findAll();

    @Select(sql = "SELECT ce.id, ce.testCampaignId, ce.executionDate, ce.testCaseResultIds, creationDate, "
                + "("
                + "  SELECT json_agg(testCaseResultFrom.*) "
                + "  FROM ( "
                + "    SELECT tcr.* FROM testcaseresult tcr "
                + "    JOIN unnest (ce.testCaseResultIds) WITH ORDINALITY o(id, ord) USING (id) "
                + "    ORDER BY o.ord "
                + "  ) as testCaseResultFrom "
                + ") as testCaseResults "
                + " FROM campaignExecution ce"

                + " WHERE testCampaignId=${testCampaignId}"
                + " ORDER BY executionDate ASC ")
    public CampaignExecution[] findAllByTestCampaign(UUID testCampaignId);

    @Update(sql = "DELETE FROM campaignExecution WHERE id=${id}")
    public int delete(UUID id);

}
