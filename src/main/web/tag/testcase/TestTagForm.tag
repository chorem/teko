let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<TestTagForm>
  <form method="post" ref="testTag" onsubmit="{create}" class="input-append">
      <input type="text"
             name="label"
             placeholder="{translate('testtag.name.placeholder')}"
             disabled="{!opts.alterable}">
      <button type="submit"
              class="button"
              value=""
              title="{ translate('common.create') }"
              if="{login && opts.alterable}">
        { translate('common.add') }
      </button>
  </form>
  <div if="{error}" class="error-notification">{error}</div>

  <script>
    this.error = "";
    this.translate = translate;
    this.login = localStorage.getItem('accessToken');

    this.create = (e) => {
      e.preventDefault();
      e.stopPropagation();

      let testTag = FormUtils.formToMap(this.refs.testTag);
      eventBus.trigger('addtesttag', testTag.label);
      this.update();
      e.target.children.label.value='';
    };

    eventBus.on('locale-changed', () => {
      this.update();
    });
  </script>

</TestTagForm>