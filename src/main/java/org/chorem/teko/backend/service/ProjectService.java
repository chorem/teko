package org.chorem.teko.backend.service;

import org.chorem.teko.backend.TekoTechnicalException;
import org.chorem.teko.backend.dao.ProjectDao;
import org.chorem.teko.backend.entity.Project;
import org.nuiton.spgeed.SqlSession;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

/**
 * Created by couteau on 27/01/17.
 */
@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProjectService extends TekoAbstractService {

    @Inject
    TokenService tokenService;

    @GET
    @Path("/project/ping")
    public boolean ping() {
        return true;
    }

    @POST
    @Path("/project/create")
    public UUID create(Project project,
                       @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        UUID userId = tokenService.checkToken(authorization);
        ProjectDao dao = session.getDao(ProjectDao.class);
        project.setId(UUID.randomUUID());
        project.setCreatorId(userId);
        dao.createProject(project);

        return project.getId();
    }

    @GET
    @Path("/project/get")
    public Project[] getProjects(@QueryParam("keyWord") String keyWord,
                                 @QueryParam("order") boolean order,
                                 @QueryParam("attribut") String attribut,
                                 @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();

        UUID userId;

        if (authorization == null || "Bearer undefined".equals(authorization)) {
            userId = null;
        } else {
            userId = tokenService.checkToken(authorization);
        }
        ProjectDao dao = session.getDao(ProjectDao.class);

        if (keyWord != null) {
            keyWord = "%" + keyWord + "%";
        }

        Project[] projects = dao.getProjects(keyWord, attribut, order, userId);
        return projects;
    }

    @GET
    @Path("/project/get/{id}")
    public Project getProject(@PathParam("id") UUID id,
                              @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        Project project = dao.getProject(id);
        if (tokenService.isUserAllowedToSeeProject(authorization, id, session)) {
            return project;
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/project/{projectId}/tags")
    public Object[] getProjectTags(@PathParam("projectId") UUID projectId) throws Exception {
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        return dao.getProjectTags(projectId);
    }

    @POST
    @Path("/project/update/{id}")
    public void updateProject(@PathParam("id") UUID id,
                              Project project,
                              @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        UUID userId = tokenService.checkToken(authorization);
        if (project.getCreatorId().equals(userId)) {
            dao.updateProject(id, project);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/project/{id}/users/{userId}")
    public void addAuthorizedUser(@PathParam("id") UUID id,
                                  @PathParam("userId") UUID newUserId,
                                  @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        UUID currentUserId = tokenService.checkToken(authorization);
        Project project = dao.getProject(id);
        if (project.getCreatorId().equals(currentUserId)) {
            dao.addAuthorizedUser(id, newUserId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/project/{id}/users/{userId}")
    public void removeAuthorizedUser(@PathParam("id") UUID id,
                                     @PathParam("userId") UUID removedUserId,
                                     @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        ProjectDao dao = session.getDao(ProjectDao.class);
        UUID currentUserId = tokenService.checkToken(authorization);
        Project project = dao.getProject(id);
        if (project.getCreatorId().equals(currentUserId)) {
            dao.removeAuthorizedUser(id, removedUserId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/project/{id}")
    public void deleteProject(@PathParam("id") UUID id,
                              @HeaderParam("Authorization") String authorization) throws Exception {
        SqlSession session = createSqlSession();
        // First, check project is no more used
        ProjectDao projectDao = session.getDao(ProjectDao.class);
        UUID currentUserId = tokenService.checkToken(authorization);
        Project project = projectDao.getProject(id);
        if (project.getCreatorId().equals(currentUserId)) {
            boolean isProjectReferenced = projectDao.isProjectReferencedByTestCase(id);
            if (isProjectReferenced) {
                //TODO ymartel 20170707 : manage exceptions !
                throw new TekoTechnicalException("This project contains testCases, it cannot be deleted.");
            }

            // Ok let's go!
            projectDao.deleteProject(id);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

}
