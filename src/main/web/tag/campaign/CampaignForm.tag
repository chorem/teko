let campaignService = require("../../service/TestCampaignService");
let projectService = require("../../service/ProjectService");
let testCasesService = require("../../service/TestCaseService");
let testCaseService = require("../../service/TestCaseService");
let testStepService = require("../../service/TestStepService");
let FormUtils = require("../../utils/FormUtils");
let route = require("riot-route");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<CampaignForm>
  <div class="sheet">
    <div class="sheet-header">
      {translate("campaign.create")}
    </div>

    <div class="sheet-content">
      <div if="{error}" class="error-notification">{error}</div>

      <form method="post" ref="testCampaign" onsubmit="{create}">
        <input type="hidden" name="projectId" value="{projectId}" if="{projectId}" >

        <ul class="form-content">
          <li if="{!fromProject}">
            <label for="projectId">{ translate("campaign.project.label") }</label>
            <select name="projectId" onChange="{loadTestCases}" >
              <option value="" disabled selected>{translate('campaign.project.placeholder')}</option>
              <option each="{project in projects}" value="{project.id}" >{project.name}</option>
            </select>
          </li>
          <li>
            <label for="label">{ translate("campaign.name.label")}</label>
            <input type="text" name="label" placeholder="{translate('campaign.name.placeholder')}" required>
          </li>

          <li>
            <label>{ translate("testcase.label")}</label>

            <div class="form-selection-buttons">
              <button onclick="{selectAllTestCases}"
                      type="button">
                { translate("common.selectAll")}
              </button>
              <button onclick="{showFilters}"
                  type="button"
                 class="button {active : projectsFiltersShow}">
                <i class="icon icon-filter"></i> {translate("testcase.filter")}
              </button>

              <div class="list-filters" if="{projectsFiltersShow}">
                <select id="criticality" onChange="{filterTestCase}">
                  <option value="">{translate('criticality.ALL')}</option>
                  <option value="LOW">{translate('criticality.LOW')}</option>
                  <option value="MEDIUM">{translate('criticality.MEDIUM')}</option>
                  <option value="HIGH">{translate('criticality.HIGH')}</option>
                  <option value="CRITICAL">{translate('criticality.CRITICAL')}</option>
                </select>
                <select id="status" onChange="{filterTestCase}">
                      <option value="">{translate('status.ALL')}</option>
                      <option value="DRAFT">{translate('status.DRAFT')}</option>
                      <option value="AVAILABLE">{translate('status.AVAILABLE')}</option>
                      <option value="CLOSED">{translate('status.CLOSED')}</option>
                </select>
                <input type="" id="keyWord" onchange="{filterTestCase}" size="12" placeholder="{ translate('testcase.filter') }" >
              </div>
            </div>

            <ul class="form-selection-list">
              <li each="{testCase in testCases}">
                <span if="{testCase.status == 'DRAFT' && testCaseFiltre[testCase.id]}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         multiplechoices="true"
                         onchange="{parent.manageDraftTestCase.bind(parent)}"
                         checked="{testCaseChecked[testCase.id] == true}"
                         onclick="{modifChecked(testCase.id)}"/>
                  <label for="{testCase.id}"
                         class="checkbox-label">
                    <span class="board-list-item-extra-infos">
                      <i class="icon icon-draft"></i>
                      { translate("common.draft") }
                    </span>
                    {testCase.label}
                  </label>
                  <a class="form-selection-list-action"
                     onclick="{parent.loadTestSteps.bind(parent, testCase.id, testCase.label)}">
                    <span class="icon icon-eye"></span>
                  </a>
                </span>
                <span if="{testCase.status == 'AVAILABLE' && testCaseFiltre[testCase.id]}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         multiplechoices="true"
                         checked="{testCaseChecked[testCase.id] == true}"
                         onclick="{modifChecked(testCase.id)}"/>
                  <label for="{testCase.id}" class="checkbox-label">{testCase.label}</label>
                  <a class="form-selection-list-action"
                     onclick="{parent.loadTestSteps.bind(parent, testCase.id, testCase.label)}" >
                    <span class="icon icon-eye"></span>
                  </a>
                </span>
                <span if="{testCaseFiltre[testCase.id] == false}">
                  <input type="checkbox"
                         value="{testCase.id}"
                         id="{testCase.id}"
                         name="testCaseIds"
                         if="{testCase.status != 'OBSOLETE'}"
                         multiplechoices ="true"
                         disabled
                         checked="{testCaseChecked[testCase.id] == true}" />
                  <label for="{testCase.id}" class="checkbox-label"><font color="#d3d3d3">{testCase.label}</font></label>
                </span>
              </li>
            </ul>
          </li>
        </ul>

      </form>

      <div class="form-buttons">
        <a if="{fromProject}"
           class="button button-secondary"
            href="/project-campaigns/{projectId}" >{ translate('common.cancel')}</a>
        <a if="{!fromProject}"
           class="button button-secondary"
            href="/campaigns">{ translate('common.cancel')}</a>
        <button class="button--secondary" onclick="{saveAsDraft}">{ translate('common.saveDraft')}</button>
        <button class="button--main" onclick="{create}" if="{usedDraftTestCases.length == 0}">{ translate('common.create')}</button>
        <button class="button--disabled" if="{usedDraftTestCases.length > 0}">{ translate('common.create')}</button>
        <div class="notification-warning" if="{usedDraftTestCases.length > 0}">
            { translate('campaign.canOnlySaveDraft')}
        </div>
      </div>

      <div if="{currentTestCaseLabel}" class="sheet-slide-pannel">
        <div class="sheet-slide-pannel-title">
          {currentTestCaseLabel} : {translate("campaign.testSteps")}
          <button onclick="{hideSlidePannel}">
            <span class="icon-cancel"></span>
          </button>
        </div>
        <TestStepsDisplay test-case-id="{currentTestCaseId}"/>
      </div>
    </div>
  </div>

    <script>
        this.translate = translate;
        this.projects = undefined;
        this.testCases = [];
        this.currentTestCaseLabel = undefined;
        this.currentTestCaseId = undefined;
        this.projectId = this.opts.projectId;
        this.fromProject = this.opts.projectId ? true : false;
        this.usedDraftTestCases = [];
        this.testCaseFiltre = {};
        this.testCaseChecked = {};

        this.loadProjectTestCases = (projectId) => {
            testCasesService.getTestCases(projectId, false, "date")
            .then((testCases) => {
              this.testCases = testCases;
              this.testCases.forEach((testCase) => {
                  let index = -1;
                  do{
                      index++;
                      if(testCases[index].id == testCase.id){
                          this.testCaseFiltre[testCase.id] = true;
                      }else{
                          this.testCaseFiltre[testCase.id] = false;
                      }
                  }while(this.testCaseFiltre[testCase.id] != true && index < testCases.length-1);
              });
              this.update();
            });
        }

        if (this.projectId) {
          // load directly list of test cases
          this.loadProjectTestCases(this.projectId);
          projectService.getProject(this.projectId)
          .then((project) => {
              eventBus.trigger('project-loaded', project);
          })

        } else {
          // Begin with projects loading
          projectService.getProjects()
          .then((projects) => {
              this.projects = projects;
              this.update();
          });
        }

        this.modifChecked = (testCaseId) => {
            return (e) => {
                this.testCaseChecked[testCaseId] = !this.testCaseChecked[testCaseId];
                if(this.testCaseChecked[testCaseId]){
                    testCasesService.getCondition(testCaseId, true)
                        .then((conditions) => {
                        conditions.forEach((condition) => {
                            this.testCaseChecked[condition.id] = this.testCaseChecked[testCaseId];
                        });
                        this.update();
                    })
                }else{
                    testCasesService.getDependOf(testCaseId, true)
                        .then((sons) => {
                        sons.forEach((son) => {
                            this.testCaseChecked[son.id] = this.testCaseChecked[testCaseId];
                        });
                        this.update();
                    })
                }
                this.update();
            }
        }

        this.filterTestCase = () => {
            let status = document.getElementById("status").value;
            let keyWord = document.getElementById("keyWord").value;
            let criticality = document.getElementById("criticality").value;
            this.testCaseFiltre = {};
            testCaseService.getTestCasesWithFilter(this.projectId, keyWord, status, criticality)
              .then((testCases) => {
                  if (testCases) {
                    this.testCases.forEach((testCase) => {
                      let index = -1;
                      do{
                        index++;
                        if(testCases[index].id == testCase.id){
                          this.testCaseFiltre[testCase.id] = true;
                        }else{
                          this.testCaseFiltre[testCase.id] = false;
                        }
                      }while(this.testCaseFiltre[testCase.id] != true && index < testCases.length-1);
                    });
                  }else{
                    this.testCases.forEach((testCase) => {
                      this.testCaseFiltre[testCase.id] = false;
                    });
                  };
                  this.update();
              })
        };

        this.showFilters = () => {
          this.projectsFiltersShow = !this.projectsFiltersShow;
        }

        this.loadTestCases = (e) => {
            this.projectId = e.target.value;
            this.loadProjectTestCases(this.projectId);
        }

        this.loadTestSteps = (testCaseId, testCaseLabel) => {
            eventBus.trigger('navigation-testcase-changed', testCaseId);
            this.currentTestCaseLabel = testCaseLabel;
            this.currentTestCaseId = testCaseId;
            this.update();
        }

        this.hideSlidePannel = () => {
            this.currentTestCaseLabel = undefined;
            this.currentTestCaseId = undefined;
            this.update();
            eventBus.trigger('navigation-testcase-changed', undefined);
        };

        this.selectAllTestCases = () => {
          this.testCases.forEach((testcase) => {
            this.testCaseChecked[testcase.id] = true;
          });
          this.update();
        };

        this.manageDraftTestCase = (e) => {
          let element = e.target;
          let testCaseId = element.value;
          let index = this.usedDraftTestCases.indexOf(testCaseId);
          if (element.checked && index < 0) {
            this.usedDraftTestCases.push(testCaseId);
          } else if (!element.checked && index > -1) {
            this.usedDraftTestCases.splice(index, 1);
          }
        };

        this.saveAsDraft = () => {

            let testCampaign = FormUtils.formToMap(this.refs.testCampaign);
            testCampaign['status'] = "DRAFT";
            campaignService.createCampaign(testCampaign.projectId, testCampaign)
            .then(() => {
                route('/project-campaigns/'+testCampaign.projectId);
            })
            .catch((msg) => {
                this.error = msg || translate('campaign.saveError');
                this.update();
            });
        };

        this.create = (e) => {
            e.preventDefault();
            e.stopPropagation();

            let testCampaign = FormUtils.formToMap(this.refs.testCampaign);
            testCampaign['status'] = "AVAILABLE";
            campaignService.createCampaign(testCampaign.projectId, testCampaign)
            .then(() => {
                route('/project-campaigns/'+testCampaign.projectId);
            })
            .catch((msg) => {
                this.error = msg || translate('campaign.saveError');
                this.update();
            });
        };

      eventBus.on('locale-changed', () => {
        this.update();
      });
    </script>

</CampaignForm>