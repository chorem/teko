let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class ProjectService extends FetchService {

    getProjects(keyWord, order, attribut) {
        return this.get("/project/get" + "?" + (keyWord ? "keyWord=" + keyWord : "") + "&"
                                             + (order ? "order=" + order : "") + "&" + (attribut ? "attribut=" + attribut : ""));
    }

    createProject(project) {
        return this.post("/project/create", project);
    }

    getProject(id) {
        return this.get("/project/get/"+ id);
    }

    getProjectTags(projectId){
        return this.get("/project/"+projectId+"/tags");
    }

    addAuthorizedUser(id, userId){
        return this.post("/project/"+id+"/users/" + userId);
    }

    removeAuthorizedUser(id, userId){
        return this.delete("/project/"+id+"/users/" + userId);
    }

    deleteProject(id) {
        return this.delete("/project/"+ id);
    }

    updateProject(id, project) {
        return this.post("/project/update/"+ id, project);
    }
}

module.exports = singleton(ProjectService);