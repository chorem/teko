package org.chorem.teko.backend.entity;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Created by couteau on 05/02/17.
 */
public class TestCase {

    protected UUID id;
    protected UUID projectId;
    protected String label;
    protected String ref;
    protected Status status;
    protected Criticality criticality;
    protected String[] tags;
    protected TestStep[] steps;
    protected UUID[] conditions;
    protected OffsetDateTime creationDate;
    protected Boolean alterable;

    public TestCase() {
        this.creationDate = OffsetDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UUID getProjectId() {
        return projectId;
    }

    public void setProjectId(UUID projectId) {
        this.projectId = projectId;
    }

    /**
     * XXX ymartel 20170711 : need a setter without camel case
     * @param projectId
     */
    public void setProjectid(UUID projectId) {
        this.projectId = projectId;
    }

    public TestStep[] getSteps() {
        return steps;
    }

    public void setSteps(TestStep[] steps) {
        this.steps = steps;
    }

    public String[] getTags () { return tags;}

    public void setTags (String[] tags) {this.tags = tags;}

    public UUID[] getConditions() {
        return conditions;
    }

    public void setConditions(UUID[] conditions) {
        this.conditions = conditions;
    }

    public Criticality getCriticality() {
        return criticality;
    }

    public void setCriticality(Criticality criticality) {
        this.criticality = criticality;
    }

    public Boolean getAlterable() {
        return alterable;
    }

    public void setAlterable(Boolean alterable) {
        this.alterable = alterable;
    }

    public OffsetDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationdate(OffsetDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
