require("./layout/Footer.tag");
require("./layout/Header.tag");
require("./campaign/Campaigns.tag");
require("./campaign/CampaignEditForm.tag");
require("./campaign/CampaignForm.tag");
require("./campaign/CampaignRunForm.tag");
require("./campaign/CampaignExecutions.tag");
require("./campaign/CampaignExecution.tag");
require("./project/Projects.tag");
require("./project/Project.tag");
require("./project/ProjectForm.tag");
require("./project/ProjectEditForm.tag");
require("./project/ProjectSettings.tag");
require("./project/ProjectUsersForm.tag");
require("./Reports.tag");
require("./sharedComponents/confirmationModal.tag");
require("./sharedComponents/obsolesceConfirmationModal.tag");
require("./sharedComponents/pageNotFound.tag");
require("./testcase/TestCase.tag");
require("./testcase/TestCases.tag");
require("./testcase/TestCaseForm.tag");
require("./testcase/TestCaseEditForm.tag");
require("./testcase/TestCaseResultForm.tag");
require("./testcase/TestStep.tag");
require("./testcase/TestSteps.tag");
require("./testcase/TestStepsDisplay.tag");
require("./testcase/TestStepEditForm.tag");
require("./testcase/TestStepForm.tag");
require("./testcase/TestTagForm.tag");
require("./testcase/TestTags.tag");
require("./user/Users.tag");
require("./user/UserEditForm.tag");
require("./user/UserForm.tag");
require("./user/UserLogin.tag");

let translate = require('counterpart');
let route = require("riot-route");
let eventBus = require("../utils/TekoMagicEventBus");
let progressJs = require("progress.js");

<Site>
  <Header/>

  <ConfirmationModal/>
  <ObsolesceConfirmationModal/>

  <main ref="content"></main>

  <Footer/>

    <script>

        <!-- i18n part -->
        translate.registerTranslations('fr', require('../locales/fr'));
        translate.registerTranslations('en', require('../locales/en.json'));
        translate.registerTranslations('fr', require('../locales/fr.json'));
        var lang = localStorage.getItem('teko-option-lang');
        if (lang) {
          translate.setLocale(lang);
        } else {
          translate.setLocale('en');
        }
        progressLoader = new progressJs();

        route("/", () => {
            eventBus.trigger('project-changed', null);
            riot.mount(this.refs.content, "projects");
        });
        route("/#", () => {
            eventBus.trigger('project-changed', null);
            riot.mount(this.refs.content, "projects");
        });
        route("/project/add", (projectId) => {
            riot.mount(this.refs.content, "projectform");
        });
        route("/project/*/edit", (projectId) => {
            riot.mount(this.refs.content, "projecteditform", {projectId});
        });
        route("/project/*/editUsers", (projectId) => {
            riot.mount(this.refs.content, "projectusersform", {projectId});
        });
        route("/project/*#", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "project", {projectId});
        });
        route("/project/*", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "project", {projectId});
        });
        route("project-campaigns/*/add", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "campaignform", {projectId});
        });
        route("project-campaigns/*", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "campaigns", {projectId});
        });
        route("/campaigns/add", (Id) => {
            riot.mount(this.refs.content, "campaignform");
        });
        route("/campaigns", () => {
            riot.mount(this.refs.content, "campaigns");
        });
        route("/campaign/*/execution/run", (campaignId) => {
            riot.mount(this.refs.content, "campaignrunform", {campaignId});
        });
        route("/campaigns/*/edit", (campaignId) => {
            riot.mount(this.refs.content, "campaigneditform", {campaignId});
        });
        route("/settings/*", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "projectsettings", {projectId});
        });
        route("/reports/*", (projectId) => {
            eventBus.trigger('project-changed', projectId);
            riot.mount(this.refs.content, "reports", {projectId});
        });
        route("/login", () => {
            riot.mount(this.refs.content, "userlogin");
        });
        route("/users", () => {
            eventBus.trigger('project-changed', null);
            riot.mount(this.refs.content, "users");
        });
        route("/testcase/*/edit", (testCaseId) => {
            riot.mount(this.refs.content, "testcaseeditform", {testCaseId});
        });
        route("/*", () => {
            riot.mount(this.refs.content, "pagenotfound");
        });
        translate.onLocaleChange(function(newLocale, oldLocale) {
          translate.setLocale(newLocale);
          localStorage.setItem('teko-option-lang', newLocale);
        }, (msg) => {
          console.log(msg);
        });

        function listenKeyboardEvenets(e) {
          e = e || window.event;

          if (e.keyCode == '13') {
            //e.preventDefault();
            //eventBus.trigger('enter-key-pressed');
          }
          else if (e.keyCode == '27') {
            //eventBus.trigger('esc-key-pressed');
          }
        }
        document.onkeydown = listenKeyboardEvenets;

        eventBus.on('loading-start', function(){
          progressLoader.start();
        });
        eventBus.on('loading-end', function(){
          if(progressLoader.percent < 100) {
            progressLoader.end();
          }
        });
    </script>
</Site>
