package org.chorem.teko.backend.dao;

import org.chorem.teko.backend.entity.TestCaseResult;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
public interface TestCaseResultDao {

    @Update(sql = "INSERT INTO testCaseResult (id, testCaseId, validity, comment, lastUserId, creationDate) "
                + "VALUES ${testCaseResult | values("
                + "'id', 'testCaseId', 'validity::validity', 'comment', 'lastUserId', 'creationDate'"
                + ")}")
    public int create(TestCaseResult testCaseResult);

    @Select(sql = "SELECT tcr.id, tcr.testCaseId, tcr.validity, tcr.comment, tcr.lastUserId, tcr.creationDate, "
                + "("
                + "  SELECT json_agg(testCaseFrom.*) "
                + "  FROM ( "
                + "    SELECT tc.id, tc.projectId, tc.label, tc.ref, tc.status, tc.criticality "
                + "    FROM testcase tc "
                + "    WHERE tc.id = tcr.testCaseId "
                + "  ) as testCaseFrom "
                + ")::json->0 as testCase "
                + " FROM testCaseResult tcr "
                + " WHERE id=${id}")
    public TestCaseResult findById(UUID id);

    @Select(sql = "SELECT tcr.id, tcr.testCaseId, tcr.validity, tcr.comment, tcr.lastUserId, tcr.creationDate, "
                + "("
                + "  SELECT json_agg(testCaseFrom.*) "
                + "  FROM ( "
                + "    SELECT tc.id, tc.projectId, tc.label, tc.ref, tc.status, tc.criticality "
                + "    FROM testcase tc "
                + "    WHERE tc.id = tcr.testCaseId "
                + "  ) as testCaseFrom "
                + ")::json->0 as testCase "
                + " FROM testCaseResult tcr "
                + " WHERE tcr.id IN ${ids|collection()}"
                + " ORDER BY tcr.creationDate ASC ")
    public TestCaseResult[] findAllByIds(UUID[] ids);

    @Select(sql = "SELECT tcr.id, tcr.testCaseId, tcr.validity, tcr.comment, tcr.lastUserId, tcr.creationDate, "
                + "("
                + "  SELECT json_agg(testCaseFrom.*) "
                + "  FROM ( "
                + "    SELECT tc.id, tc.projectId, tc.label, tc.ref, tc.status, tc.criticality "
                + "    FROM testcase tc "
                + "    WHERE tc.id = tcr.testCaseId "
                + "  ) as testCaseFrom "
                + ")::json->0 as testCase "
                + " FROM testCaseResult tcr "
                + " ORDER BY tcr.creationDate ASC ")
    public TestCaseResult[] findAll();

    @Update(sql = "DELETE FROM testCaseResult WHERE id=${id}")
    public int delete(UUID id);

}
