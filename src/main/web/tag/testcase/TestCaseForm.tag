let testCaseService = require("../../service/TestCaseService");
let FormUtils = require("../../utils/FormUtils");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<TestCaseForm>
  <div class="sheet">
    <div class="sheet-header">
      { translate("testcase.create")}
    </div>
    <div if="{error}" class="error-notification">{error}</div>
    <div class="sheet-content">
      <form method="post" ref="testCase" onsubmit="{create}">
        <ul class="form-content">
          <li>
            <label for="label">{ translate("testcase.name.label")}</label>
            <input type="text" name="label" placeholder="{translate('testcase.name.placeholder')}" required autofocus class="autofocus">
          </li>

          <li>
            <label for="criticality">{ translate("testcase.criticality.label") }</label>
            <select name="criticality">
              <option value="LOW">{ translate("testcase.criticality.low") }</option>
              <option value="MEDIUM" selected>{ translate("testcase.criticality.medium" ) }</option>
              <option value="HIGH">{ translate("testcase.criticality.high" ) }</option>
              <option value="CRITICAL">{ translate("testcase.criticality.critical" ) }</option>
            </select>
          </li>
        </ul>

        <div class="form-buttons">
          <button class="button button--secondary" type="button" onclick="{cancelAction}">{ translate('common.cancel')}</button>
          <button class="button button--main" type="button" onclick="{saveAsDraft}">{ translate('common.saveDraft')}</button>
          <input type="submit" value="{translate('common.create')}" class="button button--main">
        </div>
    </form>

  <script>
        this.translate = translate;
        this.error = "";

        this.on("mount", function() {
          $(".autofocus").focus();
        });

        this.create = (e, status) => {
            e.preventDefault();
            e.stopPropagation();

            let testCase = FormUtils.formToMap(this.refs.testCase);

            if (status) {
              testCase['status'] = status;
            } else {
              testCase['status'] = "AVAILABLE";
            }
            testCaseService.createTestCase(this.opts.projectId, testCase)
            .then((newTestCaseId) => {
                this.parent.showTestCaseForm = false;
                this.parent.update();
                eventBus.trigger('testcase-created');
                eventBus.trigger('navigation-testcase-changed', newTestCaseId, true);
            })
            .catch((msg) => {
                this.error = msg || translate('testcase.saveError');
                this.update();
            });
        };

        this.saveAsDraft = (e) => {
            this.create(e, "DRAFT");
        };

        this.cancelAction = () => {
          this.parent.showTestCaseForm = false;
          this.parent.update();
        };

        eventBus.on('locale-changed', () => {
          this.update();
        });
    </script>

</TestCaseForm>