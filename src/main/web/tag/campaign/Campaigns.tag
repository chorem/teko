let campaignService = require("../../service/TestCampaignService");
let projectsService = require("../../service/ProjectService");
let FormUtils = require("../../utils/FormUtils");
let route = require("riot-route");
let translate = require('counterpart');
let eventBus = require("../../utils/TekoMagicEventBus");

<Campaigns>
  <CampaignExecutions campaign-id="{selectedCampaignId}"
                      show={selectedCampaignId}
                      class="column-right"/>

  <div class="list">
    <div class="list-header" if="{project.id}">
      <div class="list-header-title">
        <b>{campaignsCount}</b> { translate("campaign.label")}
      </div>
      <div class="list-header-button align-right">
        <button onclick="{showFilters}" class="{active : campaignsFiltersShow}">
          <i class="icon icon-filter" title="{translate("testcase.filter")}"></i>
          {translate("common.filterAndSort")}
        </button>
      </div>
      <div if="{login}" class="list-header-button">
        <a href="/project-campaigns/{this.opts.projectId}/add/" class="button">
          <i class="icon icon-plus"></i>
          <span>{ translate("common.create") }</span>
        </a>
      </div>
    </div>

    <div if="{this.campaignsFiltersShow}" class="list-filters">
      <select id="status" onChange="{filterCampaign}">
          <option value="">{translate('status.ALL')}</option>
          <option value="DRAFT">{translate('status.DRAFT')}</option>
          <option value="AVAILABLE" selected>{translate('status.AVAILABLE')}</option>
          <option value="CLOSED">{translate('status.CLOSED')}</option>
      </select>
      <input type="" id="keyWord" onchange="{filterCampaign}" placeholder="{ translate('campaign.filter') }" >
      <select id="attribut" onChange="{filterCampaign}">
          <option value="date" selected>Chronologique</option>
          <option value="label">Alphabétique</option>
      </select>
      <a onclick="{sortCampaigns}" title="Inverser l'ordre">
          <i data-icon class="icon-arrow-up" if="{!orderBool}"></i>
          <i data-icon class="icon-arrow-down" if="{orderBool}"></i>
      </a>
    </div>

    <ul class="list-items" id="maincontent" if="{project.id && !error}">
      <li if="{!campaigns || !campaigns.length}" class="notification-no-results">
        { translate("campaign.x_campaigns.zero") }
      </li>
      <li each="{campaign in campaigns}" class="list-item list-item--link {active : campaign.id == selectedCampaignId}">
        <a href=""
            onclick="{selectCampaign(campaign.id)}"
            class="{active : campaign.id == selectedCampaignId} a--without-decoration">
            {campaign.label}
            ({campaign.status})
            <span if="{campaign.project}">({campaign.project.name})</span>
            <span class="board-list-item-extra-infos" if="{campaign.status == 'DRAFT'}">
              <i class="icon icon-draft"></i>
              {translate("common.draft")}
            </span>
            <span class="board-list-item-extra-infos" if="{campaign.status == 'CLOSED'}">
              <i class="icon icon-stop"></i>
              {translate("common.obsolete")}
            </span>
            <div>
              <span if={campaign.testCaseIds} class="board-list-item-extra-infos">
                { translate("testcase.x_testcases", { count : campaign.testCaseIds.length}) }
              </span>
            </div>
        </a>

        <a href="/campaign/{campaign.id}/execution/run"
            class="item-action"
            title="{translate('campaign.run')}"
            aria-label="{translate('campaign.run')}"
            if="{login && campaign.status == 'AVAILABLE'}">
          <i class="icon icon-play"></i>
        </a>
        <a href="/campaigns/{campaign.id}/edit"
            class="item-action"
            title="{translate('common.update')}"
            aria-label="{translate('common.update')}"
            if="{login && campaign.alterable && campaign.status !='CLOSED'}">
        <span class="icon icon-edit"></span>
        <a class="item-action"
           href=""
           title="{translate('campaign.close')}"
           aria-label="{translate('campaign.close')}"
           onclick="{closeConfirmation(campaign)}"
           if="{login && campaign.status =='AVAILABLE'}">
          <span class="icon icon-cancel"></span>
        </a>
        <a class="item-action"
            href=""
            onclick="{deleteConfirmation(campaign)}"
            title="{translate('common.delete')}"
            aria-label="{translate('common.delete')}"
            if="{login && campaign.alterable && campaign.status !='CLOSED'}">
          <span class="icon icon-delete"></span>
        </a>
      </li>
    </ul>

    <div class="notification-error" if="{(!project.id && !loading) || error}">
      {error || translate("project.notFound")}
    </div>
  </div>

    <script>
        this.translate = translate;
        this.campaigns = [];
        this.campaignsCount = 0;
        this.project = {name : ''};
        this.orderBool = false;
        this.campaigns = false;
        this.loading = true;
        this.login = localStorage.getItem('accessToken');

        this.refreshCampaigns = () => {
          // List only current project campaigns
          if (this.opts.projectId) {
            projectsService.getProject(this.opts.projectId)
            .then((project) => {
                this.project = project;
                document.title = "Teko - " + this.project.name + " - " + this.translate('campaign.label');
                this.update();
                eventBus.trigger('project-loaded', project);
            });

            campaignService.getProjectCampaigns(this.opts.projectId, "AVAILABLE", "", false, "date")
            .then((campaigns) => {
                if (campaigns) {
                  this.campaigns = campaigns;
                  this.campaignsCount = campaigns.length;
                  this.update();
                }
            });

          // List all campaigns
          } else {
            campaignService.getCampaigns()
            .then((campaigns) => {
                if (campaigns) {
                  this.campaigns = campaigns;
                  this.campaignsCount = campaigns.length;
                  this.update();
                }
            });
          }
        };

        this.delete = (id) => {
            campaignService.deleteCampaign(id)
            .then(() => {
                this.refreshCampaigns();
            })
            .catch((msg) => {
                this.error = msg || translate('campaign.deletionError');
                this.update();
            });
        };

        this.refreshCampaigns();

        this.showFilters = () => {
          this.campaignsFiltersShow = !this.campaignsFiltersShow;
        }

        this.sortCampaigns = () => {
            this.orderBool = !this.orderBool;
            this.filterCampaign();
        }

        this.filterCampaign = () => {
            let status = document.getElementById("status").value;
            let keyWord = document.getElementById("keyWord").value;
            let order = this.orderBool;
            let attribut = document.getElementById("attribut").value;
            if (this.opts.projectId) {
              campaignService.getProjectCampaigns(this.opts.projectId, status, keyWord, order, attribut)
              .then((campaigns) => {
                  if (campaigns) {
                    this.campaigns = campaigns;
                    this.campaignsCount = campaigns.length;
                  } else {
                    this.campaigns = [];
                    this.campaignsCount = 0;
                  }
                  this.update();
              });

            } else {
              campaignService.getCampaigns(status, order, attribut)
              .then((campaigns) => {
                  if (campaigns) {
                    this.campaigns = campaigns;
                    this.campaignsCount = campaigns.length;
                    this.update();
                  }
              });
            }
        };

        this.selectCampaign = (id) => {
          return (e) => {
              this.selectedCampaignId = id;
              eventBus.trigger('navigation-campaign-changed', id);
          };
        };

        // Delete Modal
        this.deleteConfirmation = (campaign) => {
            return (e) => {
                message = translate('campaign.confirmDeleteLabel');
                this.confirmModal(message, campaign.label).then((confirm) => {
                    this.delete(campaign.id);
                });
            }
        };

        this.close = (campaign) => {
          campaignService.updateCampaignStatus(campaign.id, "CLOSED")
          .then(() => {
            this.update();
          })
          .catch((msg) => {
            this.error = msg || translate('campaign.saveError');
            this.update();
          });
        };

        // Close Modal
        this.closeConfirmation = (campaign) => {
            return (e) => {
                e.preventDefault();
                e.stopPropagation();

                message = translate('campaign.confirmCloseLabel');
                closeButton = translate('campaign.close');
                this.confirmModal(message, campaign.label, closeButton).then((confirm) => {
                    this.close(campaign);
                });
            }
        };

        eventBus.on('locale-changed', () => {
          this.update();
        });
        eventBus.on('loading-end', () => {
          this.loading = false;
        });

    </script>

</Campaigns>
