let singleton = require("../utils/Singleton");
let FetchService = require("./FetchService");

class UserService extends FetchService {

    getUsers(order, attribut) {
        return this.get("/v1/users" + "?" + (order ? "order=" + order : "") + "&" + (attribut ? "attribut=" + attribut : ""));
    }

    createUser(user) {
        return this.post("/v1/users", user);
    }

    updateUser(user) {
        return this.form("/v1/users/" + user.id, user);
    }

    getUser(id) {
        return this.get("/v1/users/"+ id);
    }

    deleteUser(id) {
        return this.delete("/v1/users/"+ id);
    }

    login(user) {
        return this.post("/v1/users/login", user);
    }

    signInUser(user) {
        return this.post("/v1/users/signin", user);
    }
}

module.exports = singleton(UserService);