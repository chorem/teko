let projectService = require("../../service/ProjectService");
let userService = require("../../service/UserService");
let route = require("riot-route");
let eventBus = require("../../utils/TekoMagicEventBus");
let translate = require('counterpart');

<ProjectUsersForm>
  <div class="sheet">
    <div class="sheet-header">
      <div class="board-title">
        { translate("project.members.label")}
      </div>
    </div>
    <div if="{error}" class="error-notification">{error}</div>
    <div class="sheet-content">
      <form >
          <ul class="form-content" if="{notAuthorizedUsers.length}">
            <li>
              <label for="users">{ translate("project.members.add")}</label>
              <select onchange="{addUser}" name="users">
                  <option value="" selected></option>
                  <option each="{user in notAuthorizedUsers}"
                          value="{user.id}"
                          id="{user.id}"
                          name="{user}">
                      <label for="{user.id}">{user.firstName} {user.lastName}</label>
                  </option>
              </select>
            </li>
          </ul>

          <ul>
              <li each="{user in authorizedUsers}" class="list-item">
                  <span class="board-list-item-label">
                    {user.firstName} {user.lastName}
                    <span if="{user.id == project.creatorId}">(admin)</span>
                  </span>
                  <span>
                    <a href="" onclick="{removeUser(user)}" title="{translate('common.delete')}">
                        <span class="icon icon-cancel"></span>
                    </a>
                  </span>
              </li>
          </ul>

        <div class="form-buttons">
          <button class="button--secondary" onclick="{cancelAction}">{ translate('common.cancel') }</a>
        </div>
      </form>
    </div>
  </div>

  <script>
      this.translate = translate;
      this.project = undefined;
      this.users = [];

      this.getAllUsers = () => {
          userService.getUsers(true, 'creationDate')
              .then((users)=> {
              if(users) {
                this.users = users;
              }
              this.update();
          })
      };

      this.fetchProjectUsers = () => {
          projectService.getProject(this.opts.projectId)
              .then((project) => {
              this.project = project;
              this.updateUserList();
              this.update();
          })
      }

      this.getAllUsers();
      this.fetchProjectUsers();

      this.addUser = (e) => {
          let user = e.target.value;
          if (user != "") {
              projectService.addAuthorizedUser(this.project.id, user)
                  .then(()=> {
                  this.fetchProjectUsers();
                  this.update();
              })
          }
      };

      this.removeUser = (user) => {
          return (e) => {
              projectService.removeAuthorizedUser(this.project.id, user.id)
                  .then(() => {
                  this.fetchProjectUsers();
                  this.update();
              })
          };
      };

      this.updateUserList = () => {
        this.authorizedUsers = [];
        this.notAuthorizedUsers = [];
        this.users.forEach((user) => {
          if (this.project.authorizedUsersId
              && this.project.authorizedUsersId.includes(user.id)
              || this.project.creatorId == user.id) {
            this.authorizedUsers.push(user);
          } else {
            this.notAuthorizedUsers.push(user);
          };
        });
      };

      this.cancelAction = () => {
          route('/settings/'+this.opts.projectId);
      };

      eventBus.on('locale-changed', () => {
          this.update();
      });
  </script>
</ProjectUsersForm>