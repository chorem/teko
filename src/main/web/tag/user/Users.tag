let userService = require("../../service/UserService");
let route = require("riot-route");
let translate = require("counterpart");
let eventBus = require("../../utils/TekoMagicEventBus");
let jwt = require('jsonwebtoken');

<Users>
  <div if="{!showUserForm && !showUserEditForm}">

    <div class="list">
      <div class="list-header">
        <div class="list-header-title">
          <b>{users.length}</b>
          { translate("user.users") }
        </div>
        <div class="list-header-button align-right">
          <button onclick="{showFilters}" class="{active : testCasesFiltersShow}">
            <i class="icon icon-filter" title="{translate("testcase.filter")}"></i>
            {translate("common.sort")}
          </button>
        </div>
        <div class="list-header-button">
          <a href="" onclick="{createUser}" class="button">
              <i class="icon icon-plus"></i>
              <span>{ translate("user.create") }</span>
          </a>
        </div>
      </div>

      <div if="{testCasesFiltersShow}" class="list-filters">
        <select id="attribut" onChange="{filterUser}" class="select--no-style">
            <option value="creationDate" selected>Chronologique</option>
            <option value="mail">Alphabétique</option>
        </select>
        <a onclick="{filter}" title="Inverser l'ordre">
          <i data-icon class="{orderBool ? 'icon-arrow-down' : 'icon-arrow-down icon--animated-rotated'}"></i>
        </a>
      </div>

      <ul class="list-items">
        <li each="{user in users}" class="list-item list-item--link">

          <a
             onclick="{editUser(user)}"
             class="a--without-decoration"
             title="{translate('common.update')}">
              <span>{user.mail}</span>
          </a>
        </li>
      </ul>

    </div>
  </div>

  <UserForm if={showUserForm}/>
  <UserEditForm if={showUserEditForm} user-id={focusUser.id}/>

  <script>
    this.translate = translate;
    this.users = [];
    this.orderBool = false;
    this.userId;

    document.title = "Teko - " + this.translate('user.users');

    this.fetchUsers = () => {
        userService.getUsers(false, "creationDate")
            .then((users) => {
            this.users = users;
            this.userId = jwt.decode(localStorage.getItem('accessToken')).userId;
            this.update();
        })
    };
    this.fetchUsers();

    this.editUser = (user) => {
        return (e) => {
            this.focusUser = user;
            this.showUserEditForm = true;
        }
    };

    this.filter = () => {
        this.orderBool = !this.orderBool;
        this.filterUser();
    }

    this.filterUser = () => {
        let order = this.orderBool;
        let attribut = document.getElementById("attribut").value;
        userService.getUsers(order, attribut)
            .then((users) => {
            if (users) {
                this.users = users;
            }else{
                this.users = [];
            }
            this.update();
        });
    };

    this.deleteConfirmation = (user) => {
        return (e) => {
            message = translate('user.confirmeDelete');
            this.confirmModal(message, user.mail).then((confirm) => {
                this.delete(user.id);
            });
        }
    };

    this.delete = (id) => {
        userService.deleteUser(id)
            .then(() => {
            eventBus.trigger('user-deleted', id);
    })
    .catch((msg) => {
            this.error = msg || translate('testcase.deletionError');
        this.update();
    });
    };

    this.createUser = () => {
        this.showUserForm = true;
    };

    this.showFilters = () => {
      this.testCasesFiltersShow = !this.testCasesFiltersShow;
    };

    eventBus.on('user-created', (id) => {
        this.fetchUsers();
    });

    eventBus.on('user-updated', () => {
        this.fetchUsers();
    });

    eventBus.on('user-deleted', () => {
        this.fetchUsers();
    });

    eventBus.on('locale-changed', () => {
        this.update();
    });
  </script>

</Users>