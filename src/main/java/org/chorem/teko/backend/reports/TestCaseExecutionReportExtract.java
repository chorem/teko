package org.chorem.teko.backend.reports;

import net.sf.jasperreports.engine.JasperReport;
import org.chorem.teko.backend.entity.Validity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by couteau on 29/03/17.
 */
public class TestCaseExecutionReportExtract {

    protected String name;
    protected String criticality;
    protected String comment;
    protected Validity validity;
    protected List<TestStepReportExtract> testStepReports;
    protected JasperReport testStepSubReport;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCriticality() {
        return criticality;
    }

    public void setCriticality(String criticality) {
        this.criticality = criticality;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Validity getValidity() {
        return validity;
    }

    public void setValidity(Validity validity) {
        this.validity = validity;
    }

    public List<TestStepReportExtract> getTestStepReports() {
        return testStepReports;
    }

    public void setTestStepReports(List<TestStepReportExtract> testSteps) {
        this.testStepReports = testSteps;
    }

    public void addTestStepReport(TestStepReportExtract testStepReportExtract) {
        if (this.testStepReports == null) {
            this.testStepReports = new ArrayList<>();
        }
        this.testStepReports.add(testStepReportExtract);
    }

    public JasperReport getTestStepSubReport() {
        return testStepSubReport;
    }

    public void setTestStepSubReport(JasperReport testStepSubReport) {
        this.testStepSubReport = testStepSubReport;
    }
}
