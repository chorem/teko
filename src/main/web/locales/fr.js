// Waiting for https://github.com/martinandert/date-names/pull/8

'use strict';

module.exports = {
  counterpart: {
    names: require('../plugins/date-names/fr'),
    pluralize: require('../plugins/pluralizers/fr'),

    formats: {
      date: {
        'default':  '%a, %e. %b %Y',
        long:       '%A, %e. %B %Y',
        short:      '%d.%m.%y'
      },

      time: {
        'default':  '%H:%M Uhr',
        long:       '%H:%M:%S %z',
        short:      '%H:%M'
      },

      datetime: {
        'default':  '%a, %e. %b %Y, %H:%M Uhr',
        long:       '%A, %e. %B %Y, %H:%M:%S %z',
        short:      '%d.%m.%y %H:%M'
      }
    }
  }
};