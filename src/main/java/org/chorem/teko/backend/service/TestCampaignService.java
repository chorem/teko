package org.chorem.teko.backend.service;

import com.google.common.collect.ImmutableSet;
import org.chorem.teko.backend.dao.CampaignExecutionDao;
import org.chorem.teko.backend.dao.ProjectDao;
import org.chorem.teko.backend.dao.TestCampaignDao;
import org.chorem.teko.backend.dao.TestCaseDao;
import org.chorem.teko.backend.dao.TestCaseResultDao;
import org.chorem.teko.backend.dto.TestCampaignDto;
import org.chorem.teko.backend.entity.CampaignExecution;
import org.chorem.teko.backend.entity.Project;
import org.chorem.teko.backend.entity.Status;
import org.chorem.teko.backend.entity.TestCampaign;
import org.chorem.teko.backend.entity.TestCase;
import org.chorem.teko.backend.entity.TestCaseResult;
import org.chorem.teko.backend.exceptions.TekoException;
import org.nuiton.spgeed.SqlSession;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * @author ymartel (martel@codelutin.com)
 */
@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestCampaignService extends TekoAbstractService {

    @Inject
    TokenService tokenService;

    @GET
    @Path("/testCampaign/ping")
    public boolean ping() {
        return true;
    }

    @POST
    @Path("/project/{projectId}/testCampaign")
    public UUID create(@PathParam("projectId") UUID projectId,
                       TestCampaign testCampaign,
                       @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);

        if (tokenService.isUserAllowedToSeeProject(authorization, projectId, session)) {
            testCampaign.setId(UUID.randomUUID());
            testCampaign.setProjectId(projectId);
            //TODO ymartel 20170719 : check testCaseId are ok for this project
            campaignDao.create(testCampaign);
        } else {
            throw new ForbiddenException("Not allowed");
        }
        return testCampaign.getId();
    }

    @GET
    @Path("/testCampaign")
    public Collection<TestCampaignDto> findAll(@QueryParam("status") Status status) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);

        TestCampaign[] testCampaigns;
        if (status == null) {
            testCampaigns = campaignDao.findAll();
        } else {
            testCampaigns = campaignDao.findAllByStatus(status);
        }

        ProjectDao projectDao = session.getDao(ProjectDao.class);

        List<TestCampaignDto> testCampaignDtos = new ArrayList<>();
        if (testCampaigns != null) {
            for (TestCampaign testCampaign : testCampaigns) {
                TestCampaignDto campaignDto = new TestCampaignDto(testCampaign);
                Project project = projectDao.getProject(testCampaign.getProjectId());
                campaignDto.setProject(project);
                testCampaignDtos.add(campaignDto);
            }
        }

        return ImmutableSet.copyOf(testCampaignDtos);
    }

    @GET
    @Path("/project/{projectId}/testCampaign")
    public TestCampaign[] findAllByProject(@PathParam("projectId") UUID projectId,
                                           @QueryParam("status") Status status,
                                           @QueryParam("keyWord") String keyWord,
                                           @QueryParam("order") boolean order,
                                           @QueryParam("attribut") String attribut,
                                           @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        if (tokenService.isUserAllowedToSeeProject(authorization, projectId, session)) {
            String statusString = status == null ? "" : status.toString();

            if (keyWord != null) {
                keyWord = "%" + keyWord + "%";
            }
            return campaignDao.findWithFilter(projectId, statusString, keyWord, order, attribut);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/testCampaign/{campaignId}")
    public void update(@PathParam("campaignId") UUID campaignId,
                       TestCampaign testCampaign,
                       @HeaderParam("Authorization") String authorization) throws Exception {

        if (!campaignId.equals(testCampaign.getId())) {
            throw new IllegalArgumentException("CampaignIds does not match");
        }

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            //TODO ymartel 20170719 : check testCaseId are ok for this project
            campaignDao.update(testCampaign);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/testCampaign/{campaignId}/status")
    public void updateStatus(@PathParam("campaignId") UUID campaignId,
                             Status status,
                             @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);

        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            campaignDao.updateStatus(campaignId, status);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @DELETE
    @Path("/testCampaign/{campaignId}")
    public void delete(@PathParam("campaignId") UUID campaignId,
                       @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            campaignDao.delete(campaignId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    /**
     * @param testCases
     * @return testCases sorted by conditions
     * Pour chaque cas de test, on verifit si toutes ses dépendances sont dans le tableau trié.
     * Si oui, il est mis dans le tableau
     * Sinon, on passe au suivant
     * par exemple, en entrée j'ai la liste A, B, C, D, E et F, avec les dépendances suivantes :
     * <p>
     * A dépend de B et C,
     * C dépend de D
     * E dépend de F et D
     * <p>
     * à la fin, on a le tableau suivant : B, D, F, C, E, A
     * @author mlebras
     */
    private TestCase[] sortTestCases(TestCase[] testCases) {
        int sizeTestCases = testCases.length;
        TestCase[] testCasesSorted = new TestCase[sizeTestCases];
        int sizeTestCasesSorted = 0;
        int index = -1;

        do {
            index = (index + 1) % sizeTestCases;
            boolean in = true;
            if (testCases[index].getConditions() != null && testCases[index].getConditions().length > 0) {// si il n'a pas de dépendance on place directement le cas de test dans le tableau trié
                if (sizeTestCasesSorted > 0) {//si le tableau est vide, on passe au suivant
                    for (UUID condition : testCases[index].getConditions()) {//pour chaque condition, on vérifit qu'elle est déjà triée
                        for (int i = 0; i < sizeTestCasesSorted; ++i) {
                            if (testCasesSorted[i].getId().equals(condition)) {
                                in = true;
                                break;// si oui on passe a la condition suivante
                            } else {
                                in = false;
                            }
                        }
                        if (!in) {
                            break;// sinon on passe au cas de test suivant
                        }
                    }
                } else {
                    in = false;
                }
            }

            if (in) {
                testCasesSorted[sizeTestCasesSorted] = testCases[index];//on place le cas de test a la suite
                sizeTestCasesSorted++;
                sizeTestCases--;
                for (int i = index; i < sizeTestCases; ++i) {
                    testCases[i] = testCases[i + 1];//on retire le cas de test placé
                }
                index--;
            }
        } while (sizeTestCases > 0);
        return testCasesSorted;
    }

    @GET
    @Path("/testCampaign/{campaignId}")
    public TestCampaignDto find(@PathParam("campaignId") UUID campaignId,
                                @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);

        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            ProjectDao projectDao = session.getDao(ProjectDao.class);
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);

            TestCampaignDto campaignDto = new TestCampaignDto(campaign);
            Project project = projectDao.getProject(campaign.getProjectId());
            campaignDto.setProject(project);
            TestCase[] testCases = testCaseDao.findAllByIds(campaign.getTestCaseIds());
            if (testCases != null) {
                testCases = sortTestCases(testCases);
                campaignDto.setTestCases(ImmutableSet.copyOf(testCases));
            }

            return campaignDto;
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @GET
    @Path("/testCampaign/{campaignId}/testCases")
    public TestCase[] findCampaignTestCases(@PathParam("campaignId") UUID campaignId,
                                            @HeaderParam("Authorization") String authorization) throws Exception {

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            //TODO : manage campaign does not exist
            // retrieve testCases
            UUID[] testCaseIds = campaign.getTestCaseIds();
            TestCaseDao testCaseDao = session.getDao(TestCaseDao.class);
            return testCaseDao.findAllByIds(testCaseIds);
        } else {
            throw new ForbiddenException("Not allowed");
        }
    }

    @POST
    @Path("/testCampaign/{campaignId}/executions")
    public UUID executeCampaign(@PathParam("campaignId") UUID campaignId,
                                CampaignExecution campaignExecution,
                                @HeaderParam("Authorization") String authorization) throws Exception {

        // Must be connected to execute campain
        UUID userId = tokenService.checkToken(authorization);

        SqlSession session = createSqlSession();
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);

        ProjectDao dao = session.getDao(ProjectDao.class);
        Project project = dao.getProject(campaign.getProjectId());

        if (tokenService.isUserAllowedOnProject(userId, project)) {
            CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
            TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);

            TestCaseResult[] testCaseResults = campaignExecution.getTestCaseResults();
            UUID[] testCaseResultIds = new UUID[testCaseResults.length];
            int testCaseResultIndex = 0;
            // Create each testCaseResult
            for (TestCaseResult testCaseResult : testCaseResults) {
                UUID testCaseResultId = UUID.randomUUID();
                testCaseResult.setId(testCaseResultId);
                testCaseResult.setLastUserId(userId);
                testCaseResultDao.create(testCaseResult);
                testCaseResultIds[testCaseResultIndex] = testCaseResultId;
                testCaseResultIndex++;
            }
            campaignExecution.setId(UUID.randomUUID());
            campaignExecution.setTestCampaignId(campaignId);
            campaignExecution.setTestCaseResultIds(testCaseResultIds);
            campaignExecution.setExecutionDate(OffsetDateTime.now(ZoneOffset.UTC));
            campaignExecutionDao.create(campaignExecution);
        } else {
            throw new ForbiddenException("Not allowed");
        }

        return campaignExecution.getId();
    }

    @POST
    @Path("/testCampaign/{campaignId}/executions/{executionId}")
    public CampaignExecution getCampaignExecution(@PathParam("campaignId") UUID campaignId,
                                                  @PathParam("executionId") UUID campaignExecutionId,
                                                  @HeaderParam("Authorization") String authorization) throws Exception {

        CampaignExecution campaignExecutionDto;

        SqlSession session = createSqlSession();
        CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            CampaignExecution campaignExecution = campaignExecutionDao.findById(campaignExecutionId);
            campaignExecutionDto = new CampaignExecution();
            for (UUID testCaseResultId : campaignExecution.getTestCaseResultIds()) {
                TestCaseResult testCaseResult = testCaseResultDao.findById(testCaseResultId);
                campaignExecutionDto.addTestCaseResult(testCaseResult);
            }
        } else {
            throw new ForbiddenException("Not allowed");
        }
        return campaignExecutionDto;
    }

    @GET
    @Path("/testCampaign/{campaignId}/executions")
    public CampaignExecution[] getCampaignExecutions(@PathParam("campaignId") UUID campaignId,
                                                     @HeaderParam("Authorization") String authorization) throws Exception {

        CampaignExecution[] campaignExecutions;

        SqlSession session = createSqlSession();
        CampaignExecutionDao campaignExecutionDao = session.getDao(CampaignExecutionDao.class);
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            campaignExecutions = campaignExecutionDao.findAllByTestCampaign(campaignId);
        } else {
            throw new ForbiddenException("Not allowed");
        }
        return campaignExecutions;
    }

    @GET
    @Path("/testCampaign/{campaignId}/executions/{executionId}/results")
    public TestCaseResult[] getTestCaseResults(@PathParam("campaignId") UUID campaignId,
                                               @QueryParam("testCaseResultId") List<UUID> resultIds,
                                               @HeaderParam("Authorization") String authorization) throws Exception {

        TestCaseResult[] testCaseResults;

        SqlSession session = createSqlSession();
        TestCaseResultDao testCaseResultDao = session.getDao(TestCaseResultDao.class);
        TestCampaignDao campaignDao = session.getDao(TestCampaignDao.class);
        TestCampaign campaign = campaignDao.findById(campaignId);
        if (tokenService.isUserAllowedToSeeProject(authorization, campaign.getProjectId(), session)) {
            testCaseResults = testCaseResultDao.findAllByIds(resultIds.toArray(new UUID[resultIds.size()]));
        } else {
            throw new ForbiddenException("Not allowed");
        }
        return testCaseResults;
    }

}
