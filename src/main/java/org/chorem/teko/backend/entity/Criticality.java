package org.chorem.teko.backend.entity;

/**
 * Created by martel on 13/07/17.
 */
public enum Criticality {
    LOW,
    MEDIUM,
    HIGH,
    CRITICAL,
    ;
}
