let campaignService = require("../../service/TestCampaignService");
let reportsService = require("../../service/ReportsService");
let eventBus = require("../../utils/TekoMagicEventBus");
let route = require("riot-route");
let translate = require('counterpart');

<CampaignExecution>

    <div class="board-content">
      <ul class="board-list board-sublist">
        <li each="{testCaseResult in currentExecution.testCaseResults}">
          <span if="{testCaseResult.testCase}">{testCaseResult.testCase.label}</span>
          <span if="{testCaseResult.comment}">({testCaseResult.comment})</span>
            <span class="test-execution-results-right test-execution-result--{testCaseResult.validity}"></span>
            <!--<span class="test-execution-results-left test-execution-result--WARNING" if="{testCaseResult.info.split('\n').length > 4;}"></span>-->
        </li>
      </ul>
    </div>
    <div class="board-results-and-actions">
      <div class="campaign-execution-result">
        <b>{successPercent}</b> {translate("campaignExecution.results")}
      </div>
      <div class="campaign-execution-export">
        <a href="" onclick="{generatePDFReport}" class="button">{ translate("common.download.pdf") }</a>
        <a href="" onclick="{generateODTReport}" class="button">{ translate("common.download.odt") }</a>
      </div>
    </div>

    <script>
      this.currentExecution = this.opts.execution;
      this.translate = translate;
      this.successPercent = Math.round(this.currentExecution.success / this.currentExecution.testCaseResults.length * 1000) / 10;

      this.translateExecution = (execution) => {
        return translate("campaignExecution.summary", { "year" : execution.executionDate.year, "month" : execution.executionDate.monthValue, "day" : execution.executionDate.dayOfMonth, "nbSuccess" : execution.success, "nbTestCases" : execution.testCaseResults.length });
      };

      this.generatePDFReport = () => {
        let url = reportsService.getCampaignExecutionPdfReportsUrl(this.currentExecution.id, this.translate.getLocale());
        window.open(url);
      };

      this.generateODTReport = () => {
        let url = reportsService.getCampaignExecutionOdtReportsUrl(this.currentExecution.id, this.translate.getLocale());
        window.open(url);
      };

      eventBus.on('campaignExecution-changed', (execution) => {
        this.currentExecution = execution;
        campaignService.getCampaignExecutionTestResults(execution).then((results) => {
          this.currentExecution.testCaseResults = results;
          this.update();
        });
      });

      eventBus.on('locale-changed', () => {
        this.update();
      });

    </script>

</CampaignExecution>