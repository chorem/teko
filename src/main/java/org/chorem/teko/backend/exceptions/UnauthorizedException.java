package org.chorem.teko.backend.exceptions;

import javax.ws.rs.core.Response;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class UnauthorizedException extends TekoException {

    private static final long serialVersionUID = -2632793319923989863L;

    public UnauthorizedException(String msg) {
        super(msg);
    }

    public UnauthorizedException(String msg, Exception e) {
        super(msg, e);
    }

    @Override
    public Response.Status getHttpStatus() {
        return Response.Status.UNAUTHORIZED;
    }

}
