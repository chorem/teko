#!/bin/bash

sudo rm -rf output

docker run --rm --name fc -v $(pwd):/work -ti -d thomaswelton/fontcustom
docker exec -it fc fontcustom compile /work -c /work/fontcustom.yml -o /work/output
docker stop fc

# Installe la font générée
cp output/teko-icons.ttf ../../src/main/resources/fonts/
cp output/teko-icons.svg ../../src/main/resources/fonts/
cp output/teko-icons.woff ../../src/main/resources/fonts/
cp output/teko-icons.eot ../../src/main/resources/fonts/

# Installe le CSS généré après l'avoir rectifié
cat output/teko-icons.css | sed 's|url("./teko-icons|url("/fonts/teko-icons|g' > ../../src/main/web/less/common/_icons.less
cat default-rules.css >> ../../src/main/web/less/common/_icons.less
