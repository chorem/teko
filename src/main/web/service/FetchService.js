class FetchService {

    fetch(url, method, headers, body) {
        headers = headers || {};
        if ( !headers["Content-Type"] && !(body instanceof FormData)) {
            headers["Content-Type"] = "application/json";
        }

        return fetch(
            window.BACKEND_URL + url, {
                headers,
                method,
                credentials: "include",
                body: body instanceof FormData || typeof body === "string" ? body : body && JSON.stringify(body)
            })
            .then((response) => {
                if (response.status === 204) {
                    return null;
                }

                if (response.status === 200) {
                    return response.json();
                }

                if (response.status === 503) {
                    return Promise.reject();
                }

                return response.text()
                .then((code) => {
                    return Promise.reject(code);
                }, () => {
                    return Promise.reject();
                });
            });
    }

    get(url) {
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        return this.fetch(url, "GET", headers);
    }

    post(url, body) {
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        return this.fetch(url, "POST", headers, body);
    }

    multipartForm(url, data) {
        let formData = null;
        if (data) {
            formData = new FormData();

            let keys = Object.keys(data);
            keys.forEach((key) => {
                let value = data[key];
                formData.set(key, value);
            });
        }
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        headers["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";

        return this.fetch(url, "POST", headers, data);
    }

    form(url, data) {
        var params = new URLSearchParams();
        if (data) {

            let keys = Object.keys(data);
            keys.forEach((key) => {
                let value = data[key];
                params.append(key, value);
            });
        }
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        headers["Content-Type"] = "application/x-www-form-urlencoded;charset=UTF-8";

        return this.fetch(url, "POST", headers, params.toString());
    }

    delete(url) {
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        return this.fetch(url, "DELETE", headers);
    }

    delete(url, body) {
        let headers = {};
        if (localStorage.accessToken) {
            headers["Authorization"] = "Bearer " + localStorage.accessToken;
        }
        return this.fetch(url, "DELETE", headers, body);
    }
}

module.exports = FetchService;
